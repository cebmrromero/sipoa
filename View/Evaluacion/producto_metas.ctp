<?php $this->Html->addCrumb(__('Evaluacion'), '/evaluacion/'); ?>
<?php $this->Html->addCrumb(__('Ejecucionfisica'), '/evaluacion/ejecucion_fisica/' . $this->request->data['Poa']['id'] . '/' . $trimestre['Trimestre']['id']); ?>
<?php $this->Html->addCrumb(__('Metaspendientes'), '/evaluacion/metas_pendientes/' . $this->request->data['Poa']['id'] . '/' . $trimestre['Trimestre']['id']); ?>
<?php $this->Html->addCrumb(__('Productometas'), '/evaluacion/producto_metas/' . $this->request->data['Poa']['id'] . '/' . $trimestre['Trimestre']['id']); ?>
<div class="row-fluid poas form body-top">
	<div class="title span10">
		<h2><?php echo __('Evaluacion del %s', array($trimestre['Trimestre']['denominacion'])); ?></h2>
		<h3><?php echo __('Relacion de la cantidad de Metas/Productos Ejecutados en el %s del Ejericio Economico Financiero %s', array($trimestre['Trimestre']['denominacion'], $this->request->data['Poa']['ano'])); ?></h3>
	</div>
	<div class="actions span2">
	</div>
</div>
<div class="row-fluid body-middle">
	<div class="span12">
		<?php echo $this->Form->create('Productometa', array('controller' => 'productometas', 'action' => 'add/' . $this->request->data['Poa']['id'] . '/' . $trimestre['Trimestre']['id'])); ?>
			<fieldset>
				<legend><?php echo __('Datos Basicos'); ?></legend>
				<div class="row-fluid">
					<div class="span7">
                        <dl>
                            <dt><?php echo __('DEPENDENCIA'); ?></dt>
                            <dd>
                            <?php echo h($this->request->data['Dependencia']['descripcion']); ?>
                            &nbsp;
                        </dl>
					</div>
					<div class="span1">
                        <dl>
                            <dt><?php echo __('ANO'); ?></dt>
                            <dd>
                            <?php echo h($this->request->data['Poa']['ano']); ?>
                            &nbsp;
                        </dl>
					</div>
					<div class="span4">
                        <dl>
                            <dt><?php echo __('ES REPROGRAMADO'); ?></dt>
                            <dd>
                            <?php echo ($this->request->data['Poa']['es_reprogramado']) ? __('Si') : __('No'); ?>
                            &nbsp;
                        </dl>
					</div>
				</div>
			</fieldset>
			<fieldset>
				<legend><?php echo __('Objetivos del POA'); ?></legend>
				<div class="row-fluid">
                    <div class="span12">
						<?php //if ($ejecucionfisicas): ?>
							<?php $i = 0; ?>
							<?php foreach($this->request->data['Objetivo'] as $objetivo): ?>
								<h4><?php echo h($objetivo['descripcion']); ?></h4>
								<div class="row-fluid">
									<div class="span11 offset1">
										<legend><?php echo __('Relacion de Metas/Productos'); ?></legend>
										<table cellpadding="0" cellspacing="0" class="table table-striped table-condensed">
											<thead>
												<tr>
													<th><?php echo __('Denominacion de la Meta Anual'); ?></th>
													<th><?php echo __('Unidadmedida'); ?></th>
													<th><?php echo __('Numero de Meta/Producto'); ?></th>
													<th class="span2"><?php echo __('Trimestre al que pertenece'); ?></th>
													<th class="span3"><?php echo __('Cantidad y Descripcion del Producto'); ?></th>
													<th class="span3"><?php echo __('Medio de Verificacion'); ?></th>
												</tr>
											</thead>
											<tbody>
												<?php if ($this->request->data['Meta']): ?>
													<?php $n = 0; ?>
													<?php foreach($ejecucionfisicas as $ejecucionfisica): ?>
														<?php if ($ejecucionfisica['Objetivo']['id'] == $objetivo['id']): ?>
															<tr>
																<td><strong><?php echo $ejecucionfisica['Ejecucionevaluacione']['cantidad_ejecutada']; ?></strong> <?php echo $ejecucionfisica['Meta']['descripcion']; ?></td>
																<td><?php echo $ejecucionfisica['Unidadmedida']['denominacion']; ?></td>
																	<?php if (!$ejecucionfisica['Productometa']): ?>
																		<td>
																			<?php $aux = $i; ?>
																			<?php for ($n = 0; $n < $ejecucionfisica['Ejecucionevaluacione']['cantidad_ejecutada']; $n++): ?>
																				<?php echo $this->Form->label($aux . '.trimestre_id', $n + 1, array("class" => 'label-table')); ?>
																				<?php $aux++; ?>
																			<?php endfor; ?>
																		</td>
																		<td>
																			<?php $aux = $i; ?>
																			<?php for ($n = 0; $n < $ejecucionfisica['Ejecucionevaluacione']['cantidad_ejecutada']; $n++): ?>
																				<?php echo $this->Form->input($aux . '.ejecucionevaluacione_id', array('type' => 'hidden', 'value' => $ejecucionfisica['Ejecucionevaluacione']['id'])); ?>
																				<?php echo $this->Form->label($aux . '.trimestre_id', $ejecucionfisica['Trimestre']['denominacion'], array("class" => 'label-table')); ?>
																				<?php echo $this->Form->input($aux . '.trimestre_id', array('type' => 'hidden', 'value' => (isset($ejecucionfisica['Ejecucionevaluacione']['trimestre_pertenece_id'])) ? $ejecucionfisica['Ejecucionevaluacione']['trimestre_pertenece_id'] : $trimestre['Trimestre']['id'])) ?>
																				<?php $aux++; ?>
																			<?php endfor; ?>
																		</td>
																		<td>
																			<?php $aux = $i; ?>
																			<?php for ($n = 0; $n < $ejecucionfisica['Ejecucionevaluacione']['cantidad_ejecutada']; $n++): ?>
																				<?php echo $this->Form->input($aux . '.descripcion', array('class' => 'span12', 'rows' => 1, 'label' => false, 'div' => false, 'value' => (isset($ejecucionfisica['Productometa']['descripcion'])) ? $ejecucionfisica['Productometa']['descripcion'] : '')) ?>
																				<?php $aux++; ?>
																			<?php endfor; ?>
																		</td>
																		<td>
																			<?php $aux = $i; ?>
																			<?php for ($n = 0; $n < $ejecucionfisica['Ejecucionevaluacione']['cantidad_ejecutada']; $n++): ?>
																				<?php echo $this->Form->input($aux . '.medio_verificacion', array('class' => 'span12', 'rows' => 1, 'label' => false, 'div' => false, 'value' => (isset($ejecucionfisica['Productometa']['medio_verificacion'])) ? $ejecucionfisica['Productometa']['medio_verificacion'] : '')) ?> 
																				<?php $aux++; ?>
																			<?php endfor; ?>
																		</td>
																		<?php $i = $aux; ?>
																	<?php else: ?>
																		<?php $n = 0; ?>
																		<?php //foreach ($ejecucionfisica['Productometa'] as $productometa): ?>
																			<td>
																				<?php $aux = $i; ?>
																				<?php foreach ($ejecucionfisica['Productometa'] as $productometa): ?>
																					<?php echo $this->Form->input($aux . '.id', array('type' => 'hidden', 'value' => $productometa['Productometa']['id'])) ?>
																					<?php echo $this->Form->label($aux . '.trimestre_id', ++$n, array("class" => 'label-table')); ?>
																					<?php $aux++; ?>
																				<?php endforeach; ?>
																			</td>
																			<td>
																				<?php $aux = $i; ?>
																				<?php foreach ($ejecucionfisica['Productometa'] as $productometa): ?>
																					<?php echo $this->Form->input($aux . '.ejecucionevaluacione_id', array('type' => 'hidden', 'value' => $productometa['Productometa']['ejecucionevaluacione_id'])); ?>
																					<?php echo $this->Form->label($aux . '.trimestre_id', $ejecucionfisica['Trimestre']['denominacion'], array("class" => 'label-table')); ?>
																					<?php echo $this->Form->input($aux . '.trimestre_id', array('type' => 'hidden', 'value' => $productometa['Productometa']['trimestre_id'])); ?>
																					<?php $aux++; ?>
																				<?php endforeach; ?>
																			</td>
																			<td>
																				<?php $aux = $i; ?>
																				<?php foreach ($ejecucionfisica['Productometa'] as $productometa): ?>
																					<?php echo $this->Form->input($aux . '.descripcion', array('class' => 'span12', 'rows' => 1, 'label' => false, 'div' => false, 'value' => $productometa['Productometa']['descripcion'])); ?>
																					<?php $aux++; ?>
																				<?php endforeach; ?>
																			</td>
																			<td>
																				<?php $aux = $i; ?>
																				<?php foreach ($ejecucionfisica['Productometa'] as $productometa): ?>
																					<?php echo $this->Form->input($aux . '.medio_verificacion', array('class' => 'span12', 'rows' => 1, 'label' => false, 'div' => false, 'value' => $productometa['Productometa']['medio_verificacion'])); ?>
																					<?php $n++; ?>
																					<?php $aux++; ?>
																				<?php endforeach; ?>
																				<?php $i = $aux; ?>
																			</td>
																		<?php //endforeach; ?>
																	<?php endif; ?>
																</td>
															</tr>
															<?php $n++; ?>
														<?php endif; ?>
													<?php endforeach; ?>
													<?php if ($n == 0): ?>
														<tr>
															<td colspan="6"><?php echo __('No hay Metas/Productos que mostrar'); ?></td>
														</tr>
													<?php endif; ?>
												<?php else: ?>
													<tr>
														<td colspan="6"><?php echo __('El Objetivo no tiene Metas asociadas, debe proceder a crearlas'); ?></td>
													</tr>
												<?php endif; ?>
											</tbody>
										</table>
									</div>
								</div>
							<?php endforeach; ?>
						<?php //endif; ?>
                    </div>
				</div>
			</fieldset>
			<div class="form-actions row-fluid">
                <div class="span6 control-group">
					<?php echo $this->Html->link(__("Volver a Metas Pendientes"), array('action' => 'metas_pendientes', $this->request->data['Poa']['id'], $trimestre['Trimestre']['id'])); ?>
				</div>
                <div class="span6 control-group text-right">
					<?php echo $this->Form->button(__("Guardar y Continuar con Indicadores"), array('type' => 'submit', 'class' => 'btn btn-primary')); ?>
				</div>
			</div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>
