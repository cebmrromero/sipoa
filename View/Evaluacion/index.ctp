<?php $this->Html->addCrumb(__('Evaluacion'), '/evaluacion/index'); ?>
<div class="row-fluid body-top">
	<div class="title span10">
		<h2><?php echo __("Evaluacion"); ?></h2>
	</div>
	<div class="actions span2">
		<ul class="inline-menu pull-right">
			<li><?php echo $this->Html->link(__('Agregar'), array('controller' => 'poas', 'action' => 'add')); ?></li>
		</ul>
	</div>
</div>
<div class="row-fluid body-middle">
	<div class="span12">
		<p><?php echo __("A continuacion se encuenta un listado con todos los POAs registrados en cada dependencia:"); ?></p>
		<fieldset>
			<?php foreach ($dependencias as $dependencia): ?>
				<legend><?php echo $dependencia['Dependencia']['descripcion']; ?></legend>
				<?php $poas = $dependencia['Poa']; ?>
				<table cellpadding="0" cellspacing="0" class="table table-striped table-condensed">
					<tbody>
						<?php if ($poas): ?>
							<?php foreach ($poas as $poa): ?>
								<tr<?php echo ($poa['status']) ? '' : ' class="highlight"'; ?>>
									<td><?php echo h($poa['ano']); ?>&nbsp;</td>
									<td><?php echo ($poa['es_reprogramado']) ? __('Reprogramacion') . ' #' . $poa['numero_reprogramacion'] : '-'; ?>&nbsp;</td>
									<td class="actions btn-group span1">
										<?php if ($poa['status']): ?>
											<?php echo $this->Html->link(__('<i class="icon-chevron-right icon-white"></i>'), array('action' => 'principal', $poa['id']), array("class" => "label label-important", "escape" => false, "title" => __('Evaluacion'))); ?>
										<?php else: ?>
											<?php echo $this->Html->link(__('<i class="icon-zoom-in icon-white"></i>'), array('action' => 'principal', $poa['id']), array("class" => "label", "escape" => false, "title" => __('Evaluacion'))); ?>
										<?php endif; ?>
									</td>
								</tr>
							<?php endforeach; ?>
						<?php else: ?>
							<tr>
								<td colspan="5"><?php echo __('No hay poas registrados para la dependencia'); ?></td>
							</tr>
						<?php endif; ?>
					</tbody>
				</table>
			<?php endforeach; ?>
		</fieldset>
	</div>
</div>
<div class="row-fluid body-bottom">
	<div class="span12 text-center">
		<p><?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de un total de {:count}, iniciando en el registro {:start}, y finalizando en {:end}'))); ?></p>
		<div class="paging">
			<?php
			echo $this->Paginator->prev('< ' . __('anterior'), array(), null, array('class' => 'prev disabled'));
			echo $this->Paginator->numbers(array('separator' => '|'));
			echo $this->Paginator->next(__('siguiente') . ' >', array(), null, array('class' => 'next disabled'));
			?>
		</div>
	</div>
</div>
