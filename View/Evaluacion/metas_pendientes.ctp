<?php $this->Html->addCrumb(__('Evaluacion'), '/evaluacion/'); ?>
<?php $this->Html->addCrumb(__('Ejecucionfisica'), '/evaluacion/ejecucion_fisica/' . $this->request->data['Poa']['id'] . '/' . $trimestre['Trimestre']['id']); ?>
<?php $this->Html->addCrumb(__('Metaspendientes'), '/evaluacion/metas_pendientes/' . $this->request->data['Poa']['id'] . '/' . $trimestre['Trimestre']['id']); ?>
<div class="row-fluid poas form body-top">
	<div class="title span10">
		<h2><?php echo __('Evaluacion del %s', array($trimestre['Trimestre']['denominacion'])); ?></h2>
		<h3><?php echo __('Relacion de Metas pendientes por alcanzar al cierre del %s Ejercicio Economico Financiero %s', array($trimestre['Trimestre']['denominacion'], $this->request->data['Poa']['ano'])); ?></h3>
	</div>
	<div class="actions span2">
	</div>
</div>
<div class="row-fluid body-middle">
	<div class="span12">
		<?php echo $this->Form->create('Metapendiente', array('controller' => 'metapendientes', 'action' => 'add/' . $this->request->data['Poa']['id'] . '/' . $trimestre['Trimestre']['id'])); ?>
			<fieldset>
				<legend><?php echo __('Datos Basicos'); ?></legend>
				<div class="row-fluid">
					<div class="span7">
                        <dl>
                            <dt><?php echo __('DEPENDENCIA'); ?></dt>
                            <dd>
                            <?php echo h($this->request->data['Dependencia']['descripcion']); ?>
                            &nbsp;
                        </dl>
					</div>
					<div class="span1">
                        <dl>
                            <dt><?php echo __('ANO'); ?></dt>
                            <dd>
                            <?php echo h($this->request->data['Poa']['ano']); ?>
                            &nbsp;
                        </dl>
					</div>
					<div class="span4">
                        <dl>
                            <dt><?php echo __('ES REPROGRAMADO'); ?></dt>
                            <dd>
                            <?php echo ($this->request->data['Poa']['es_reprogramado']) ? __('Si') : __('No'); ?>
                            &nbsp;
                        </dl>
					</div>
				</div>
			</fieldset>
			<fieldset>
				<legend><?php echo __('Objetivos del POA'); ?></legend>
				<div class="row-fluid">
                    <div class="span12">
						<?php $i = 0; ?>
						<?php //if ($ejecucionfisicas): ?>
							<?php foreach($this->request->data['Objetivo'] as $objetivo): ?>
								<h4><?php echo h($objetivo['descripcion']); ?></h4>
								<div class="row-fluid">
									<div class="span11 offset1">
										<?php if ($metapendientes): ?>
											<legend><?php echo __('Metas Pendientes de Trimestres Anteriores'); ?></legend>
											<table cellpadding="0" cellspacing="0" class="table table-striped table-condensed">
												<thead>
													<tr>
														<th><?php echo __('Trimestre al que pertenece'); ?></th>
														<th><?php echo __('Denominacion de la Meta Anual'); ?></th>
														<th><?php echo __('Cantidad de Meta/Producto a alcanzar'); ?></th>
														<th><?php echo __('Explicacion'); ?></th>
														<th><?php echo __('Trimestre estimado para el cumplimiento'); ?></th>
													</tr>
												</thead>
												<tbody>
													<?php $n = 0; ?>
													<?php foreach($metapendientes as $metapendiente): ?>
														<?php if ($metapendiente['Objetivo']['id'] == $objetivo['id']): ?>
															<tr>
																<td>
																	<?php if (isset($metapendiente['Metapendiente']['id']) && ($metapendiente['Metapendiente2']['trimestre_reporte_id'] == $trimestre['Trimestre']['id'])): ?>
																		<?php echo $this->Form->input($i . '.id', array('type' => 'hidden', 'value' => $metapendiente['Metapendiente2']['id'])) ?>
																	<?php endif; ?>
																	<?php echo $this->Form->input($i . '.unidadmedida_id', array('type' => 'hidden', 'value' => $metapendiente['Unidadmedida']['id'])) ?>
																	<?php echo $metapendiente['Trimestre']['denominacion']; ?>
																	<?php echo $this->Form->input($i . '.trimestre_id', array('type' => 'hidden', 'value' => ($metapendiente['Metapendiente']['trimestre_id']) ? $metapendiente['Metapendiente']['trimestre_id'] : $trimestre['Trimestre']['id'])); ?>
																	<?php echo $this->Form->input($i . '.trimestre_reporte_id', array('type' => 'hidden', 'value' => ($metapendiente['Metapendiente2']['trimestre_reporte_id']) ? $metapendiente['Metapendiente2']['trimestre_reporte_id'] : $trimestre['Trimestre']['id'])); ?>
																</td>
																<td><strong><?php echo $metapendiente['Ejecucionfisica']['cantidad_planificada']; ?></strong> <?php echo $metapendiente['Meta']['descripcion']; ?></td>
																<td>
																	<?php echo $metapendiente['Ejecucionfisica']['cantidad_por_alcanzar']; ?>
																	<?php echo $this->Form->input($i . '.cantidad_pendiente', array('type' => 'hidden', 'value' => $metapendiente['Ejecucionfisica']['cantidad_por_alcanzar'])) ?>
																</td>
																<td>
																	<?php if (isset($metapendiente['Metapendiente']['id']) && ($metapendiente['Metapendiente2']['trimestre_reporte_id'] == $trimestre['Trimestre']['id'])): ?>
																		<?php echo $this->Form->input($i . '.explicacion', array('type' => 'textarea', 'rows' => 3, 'class' => 'span12', 'label' => false, 'div' => false, 'value' => (isset($metapendiente['Metapendiente2']['explicacion']) ? $metapendiente['Metapendiente2']['explicacion'] : ''))) ?>
																	<?php else: ?>
																		<?php echo $this->Form->input($i . '.explicacion', array('type' => 'textarea', 'rows' => 3, 'class' => 'span12', 'label' => false, 'div' => false, 'value' => '')) ?>
																	<?php endif; ?>
																</td>
																<td><?php echo $this->Form->input($i . '.trimestre_ejecucion_id', array('class' => 'span12', 'label' => false, 'div' => false, 'value' => (isset($metapendiente['Metapendiente']['trimestre_id'])) ? $metapendiente['Metapendiente']['trimestre_id'] : '', 'options' => $trimestres)) ?></td>
															</tr>
															<? $n++; ?>
														<?php endif; ?>
														<?php $i++; ?>
													<?php endforeach; ?>
												</tbody>
											</table>
										<?php endif; ?>
										<legend><?php echo __('Metas Pendientes'); ?></legend>
										<table cellpadding="0" cellspacing="0" class="table table-striped table-condensed">
											<thead>
												<tr>
													<th><?php echo __('Trimestre al que pertenece'); ?></th>
													<th><?php echo __('Denominacion de la Meta Anual'); ?></th>
													<th><?php echo __('Cantidad de Meta/Producto a alcanzar'); ?></th>
													<th><?php echo __('Explicacion'); ?></th>
													<th><?php echo __('Trimestre estimado para el cumplimiento'); ?></th>
												</tr>
											</thead>
											<tbody>
												<?php if ($this->request->data['Meta']): ?>
													<?php $n = 0; ?>
													<?php foreach($ejecucionfisicas as $ejecucionfisica): ?>
														<?php if ($ejecucionfisica['Objetivo']['id'] == $objetivo['id']): ?>
															<tr>
																<td>
																	<?php if (isset($ejecucionfisica['Metapendiente']['id'])): ?>
																		<?php echo $this->Form->input($i . '.id', array('type' => 'hidden', 'value' => $ejecucionfisica['Metapendiente']['id'])) ?>
																	<?php endif; ?>
																	<?php echo $this->Form->input($i . '.unidadmedida_id', array('type' => 'hidden', 'value' => $ejecucionfisica['Unidadmedida']['id'])) ?>
																	<?php echo $ejecucionfisica['Trimestre']['denominacion']; ?>
																	<?php echo $this->Form->input($i . '.trimestre_id', array('type' => 'hidden', 'value' => ($ejecucionfisica['Metapendiente']['trimestre_id']) ? $ejecucionfisica['Metapendiente']['trimestre_id'] : $trimestre['Trimestre']['id'])); ?>
																	<?php echo $this->Form->input($i . '.trimestre_reporte_id', array('type' => 'hidden', 'value' => ($ejecucionfisica['Metapendiente']['trimestre_reporte_id']) ? $ejecucionfisica['Metapendiente']['trimestre_reporte_id'] : $trimestre['Trimestre']['id'])); ?>
																</td>
																<td><strong><?php echo $ejecucionfisica['Ejecucionfisica']['cantidad_planificada']; ?></strong> <?php echo $ejecucionfisica['Meta']['descripcion']; ?></td>
																<td>
																	<?php echo $ejecucionfisica['Ejecucionfisica']['cantidad_por_alcanzar']; ?>
																	<?php echo $this->Form->input($i . '.cantidad_pendiente', array('type' => 'hidden', 'value' => $ejecucionfisica['Ejecucionfisica']['cantidad_por_alcanzar'])) ?>
																</td>
																<td>
																	<?php echo $this->Form->input($i . '.explicacion', array('type' => 'textarea', 'rows' => 3, 'class' => 'span12', 'label' => false, 'div' => false, 'value' => (isset($ejecucionfisica['Metapendiente']['explicacion']) ? $ejecucionfisica['Metapendiente']['explicacion'] : ''))) ?>
																</td>
																<td><?php echo $this->Form->input($i . '.trimestre_ejecucion_id', array('class' => 'span12', 'label' => false, 'div' => false, 'value' => (isset($ejecucionfisica['Metapendiente']['trimestre_id'])) ? $ejecucionfisica['Metapendiente']['trimestre_id'] : '', 'options' => $trimestres)) ?></td>
															</tr>
															<? $n++; ?>
														<?php endif; ?>
														<?php $i++; ?>
													<?php endforeach; ?>
													<?php if ($n == 0): ?>
														<tr>
															<td colspan="5"><?php echo __('No hay Metas pendientes que mostrar'); ?></td>
														</tr>
													<?php endif; ?>
												<?php else: ?>
													<tr>
														<td colspan="5"><?php echo __('El Objetivo no tiene Metas asociadas, debe proceder a crearlas'); ?></td>
													</tr>
												<?php endif; ?>
											</tbody>
										</table>
									</div>
								</div>
							<?php endforeach; ?>
						<?php //endif; ?>
                    </div>
				</div>
			</fieldset>
			<div class="form-actions row-fluid">
                <div class="span6 control-group">
					<?php echo $this->Html->link(__("Volver a Ejecucion Fisica"), array('action' => 'ejecucion_fisica', $this->request->data['Poa']['id'], $trimestre['Trimestre']['id'])); ?>
				</div>
                <div class="span6 control-group text-right">
					<?php echo $this->Form->button(__("Guardar y Continuar con Metas Productos"), array('type' => 'submit', 'class' => 'btn btn-primary')); ?>
				</div>
			</div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>
