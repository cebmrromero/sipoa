<?php $this->Html->addCrumb(__('Evaluacion'), '/evaluacion/'); ?>
<?php $this->Html->addCrumb(__('Ejecucionfisica'), '/evaluacion/ejecucion_fisica/' . $this->request->data['Poa']['id'] . '/' . $trimestre['Trimestre']['id']); ?>
<div class="row-fluid poas form body-top">
	<div class="title span10">
		<h2><?php echo __('Evaluacion del %s', array($trimestre['Trimestre']['denominacion'])); ?></h2>
		<h3><?php echo __('Evaluacion del Plan Operativo Anual %s del Ejercicio Economico Financiero %s', array($trimestre['Trimestre']['denominacion'], $this->request->data['Poa']['ano'])); ?></h3>
	</div>
	<div class="actions span2">
	</div>
</div>
<div class="row-fluid body-middle">
	<div class="span12">
		<?php echo $this->Form->create('Ejecucionevaluacione', array('controller' => 'ejecucionevaluaciones', 'action' => 'add/' . $this->request->data['Poa']['id'] . '/' . $trimestre['Trimestre']['id'], 'class' => 'calculate')); ?>
			<fieldset>
				<legend><?php echo __('Datos Basicos'); ?></legend>
				<div class="row-fluid">
					<div class="span7">
                        <dl>
                            <dt><?php echo __('DEPENDENCIA'); ?></dt>
                            <dd>
                            <?php echo h($this->request->data['Dependencia']['descripcion']); ?>
                            &nbsp;
                        </dl>
					</div>
					<div class="span1">
                        <dl>
                            <dt><?php echo __('ANO'); ?></dt>
                            <dd>
                            <?php echo h($this->request->data['Poa']['ano']); ?>
                            &nbsp;
                        </dl>
					</div>
					<div class="span4">
                        <dl>
                            <dt><?php echo __('ES REPROGRAMADO'); ?></dt>
                            <dd>
                            <?php echo ($this->request->data['Poa']['es_reprogramado']) ? __('Si') : __('No'); ?>
                            &nbsp;
                        </dl>
					</div>
				</div>
			</fieldset>
			<fieldset>
				<legend><?php echo __('Objetivos del POA'); ?></legend>
				<div class="row-fluid">
                    <div class="span12">
						<?php $i = 0; ?>
						<?php foreach($this->request->data['Objetivo'] as $objetivo): ?>
							<h4><?php echo h($objetivo['descripcion']); ?></h4>
							<div class="row-fluid">
								<div class="span11 offset1">
									<?php if ($metapendientes): ?>
										<legend><?php echo __('Metas de Trimestres Anteriores'); ?></legend>
										<table cellpadding="0" cellspacing="0" class="table table-striped table-condensed">
											<thead>
												<tr>
													<th><?php echo __('Trimestre al que pertenece'); ?></th>
													<th><?php echo __('Denominacion de la Meta Anual'); ?></th>
													<th><?php echo __('Unidadmedida'); ?></th>
													<th class="span1"><?php echo __('Programado'); ?></th>
													<th class="span1"><?php echo __('Ejecutado'); ?></th>
													<th class="span1"><?php echo __('% Ejecucion'); ?></th>
													<th class="span1"><?php echo __('% Por Alcanzar'); ?></th>
												</tr>
											</thead>
											<tbody>
												<?php $n = 0; ?>
												<?php foreach($metapendientes as $metapendiente): ?>
													<?php if ($metapendiente['Objetivo']['id'] == $objetivo['id']): ?>
														<?php $n++; ?>
														<?php $empty = false; //print_r($ejecucionfisica); ?>
														<tr>
															<td>
																<?php //print_r($ejecucionfisica);
																if (isset($metapendiente['Ejecucionevaluacione']['id'])): ?>
																	<?php echo $this->Form->input($i . '.id', array('type' => 'hidden', 'value' => $metapendiente['Ejecucionevaluacione']['id'])) ?>
																<?php endif; ?>
																<?php echo $this->Form->input($i . '.unidadmedida_id', array('type' => 'hidden', 'value' => $metapendiente['Unidadmedida']['id'])) ?>
																<?php echo $this->Form->input($i . '.trimestre_id', array('type' => 'hidden', 'value' => isset($metapendiente['Metapendiente']['trimestre_ejecucion_id']) ? $metapendiente['Metapendiente']['trimestre_ejecucion_id'] : $trimestre['Trimestre']['id'])) ?>
																<?php echo $this->Form->input($i . '.trimestre_pertenece_id', array('type' => 'hidden', 'value' => isset($metapendiente['Ejecucionfisica']['trimestre_id']) ? $metapendiente['Ejecucionfisica']['trimestre_id'] : '')) ?>
																<?php echo $metapendiente['Trimestre']['denominacion']; ?>
															</td>
															<td><strong><?php echo $metapendiente['Ejecucionfisica']['cantidad_planificada']; ?></strong> <?php echo $metapendiente['Meta']['descripcion']; ?></td>
															<td><?php echo $metapendiente['Unidadmedida']['denominacion']; ?></td>
															<td class="text-center"><?php echo $metapendiente['Metapendiente']['cantidad_pendiente']; ?></td>
															<td><?php echo $this->Form->input($i . '.cantidad_ejecutada', array('type' => 'text', 'class' => 'span12', 'label' => false, 'div' => false, 'value' => (isset($metapendiente['Ejecucionfisica']['ejecutado']) ? $metapendiente['Ejecucionevaluacione']['cantidad_ejecutada'] : 0), 'max' => $metapendiente['Ejecucionevaluacione']['cantidad_ejecutada'])) ?></td>
															<td class="text-center">0</td>
															<td class="text-center">0</td>
														</tr>
													<?php endif; ?>
													<?php $i++; ?>
												<?php endforeach; ?>
												<?php if ($n == 0): ?>
													<tr>
														<td colspan="7"><?php echo __('No hay productos/metas de trimestres anteriores que mostrar'); ?></td>
													</tr>
												<?php endif; ?>
											</tbody>
										</table>
									<?php endif; ?>
									<legend><?php echo __('Metas'); ?></legend>
									<table cellpadding="0" cellspacing="0" class="table table-striped table-condensed">
										<thead>
											<tr>
												<th><?php echo __('Trimestre al que pertenece'); ?></th>
												<th><?php echo __('Denominacion de la Meta Anual'); ?></th>
												<th><?php echo __('Unidadmedida'); ?></th>
												<th class="span1"><?php echo __('Programado'); ?></th>
												<th class="span1"><?php echo __('Ejecutado'); ?></th>
												<th class="span1"><?php echo __('% Ejecucion'); ?></th>
												<th class="span1"><?php echo __('% Por Alcanzar'); ?></th>
											</tr>
										</thead>
										<tbody>
											<?php if ($this->request->data['Meta']): ?>
												<?php $empty = true; ?>
												<?php foreach($ejecucionfisicas as $ejecucionfisica): ?>
													<?php if ($ejecucionfisica['Objetivo']['id'] == $objetivo['id']): ?>
														<?php $empty = false; //print_r($ejecucionfisica); ?>
														<tr>
															<td>
																<?php //print_r($ejecucionfisica);
																if (isset($ejecucionfisica['Ejecucionevaluacione']['id'])): ?>
																	<?php echo $this->Form->input($i . '.id', array('type' => 'hidden', 'value' => $ejecucionfisica['Ejecucionevaluacione']['id'])) ?>
																<?php endif; ?>
																<?php echo $this->Form->input($i . '.unidadmedida_id', array('type' => 'hidden', 'value' => $ejecucionfisica['Unidadmedida']['id'])) ?>
																<?php echo $this->Form->input($i . '.trimestre_id', array('type' => 'hidden', 'value' => isset($ejecucionfisica['Ejecucionevaluacione']['trimestre_id']) ? $ejecucionfisica['Ejecucionevaluacione']['trimestre_id'] : $trimestre['Trimestre']['id'])) ?>
																<?php echo $this->Form->input($i . '.trimestre_pertenece_id', array('type' => 'hidden', 'value' => isset($ejecucionfisica['Ejecucionevaluacione']['trimestre_pertenece_id']) ? $ejecucionfisica['Ejecucionevaluacione']['trimestre_pertenece_id'] : $trimestre['Trimestre']['id'])) ?>
																<?php echo ($ejecucionfisica['Trimestre']['denominacion']) ? $ejecucionfisica['Trimestre']['denominacion'] : $trimestre['Trimestre']['denominacion']; ?>
															</td>
															<td><strong><?php echo $ejecucionfisica['Ejecucionfisica']['cantidad_planificada']; ?></strong> <?php echo $ejecucionfisica['Meta']['descripcion']; ?></td>
															<td><?php echo $ejecucionfisica['Unidadmedida']['denominacion']; ?></td>
															<td class="text-center"><?php echo $ejecucionfisica['Ejecucionfisica']['cantidad_planificada']; ?></td>
															<td><?php echo $this->Form->input($i . '.cantidad_ejecutada', array('type' => 'text', 'class' => 'span12', 'label' => false, 'div' => false, 'value' => (isset($ejecucionfisica['Ejecucionevaluacione']['cantidad_ejecutada']) ? $ejecucionfisica['Ejecucionevaluacione']['cantidad_ejecutada'] : 0), 'max' => $ejecucionfisica['Ejecucionfisica']['cantidad_planificada'])) ?></td>
															<td class="text-center">0</td>
															<td class="text-center">0</td>
														</tr>
													<?php endif; ?>
													<?php $i++; ?>
												<?php endforeach; ?>
												<?php if ($empty): ?>
													<tr>
														<td colspan="7"><?php echo __('El Objetivo no tiene Metas asociadas, debe proceder a crearlas'); ?></td>
													</tr>
												<?php endif; ?>
											<?php else: ?>
												<tr>
													<td colspan="7"><?php echo __('El Objetivo no tiene Metas asociadas, debe proceder a crearlas'); ?></td>
												</tr>
											<?php endif; ?>
										</tbody>
									</table>
								</div>
							</div>
						<?php endforeach; ?>
                    </div>
				</div>
			</fieldset>
			<div class="form-actions row-fluid">
                <div class="span6 control-group">
					<?php echo $this->Html->link(__("Volver a Resumen"), array('action' => 'principal', $this->request->data['Poa']['id'])); ?>
				</div>
                <div class="span6 control-group text-right">
					<?php echo $this->Form->button(__("Guardar y Continuar con Metas Pendientes"), array('type' => 'submit', 'class' => 'btn btn-primary')); ?>
				</div>
			</div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>
