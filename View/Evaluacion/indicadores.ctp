<?php $this->Html->addCrumb(__('Evaluacion'), '/evaluacion/'); ?>
<?php $this->Html->addCrumb(__('Ejecucionfisica'), '/evaluacion/ejecucion_fisica/' . $this->request->data['Poa']['id'] . '/' . $trimestre['Trimestre']['id']); ?>
<?php $this->Html->addCrumb(__('Metaspendientes'), '/evaluacion/metas_pendientes/' . $this->request->data['Poa']['id'] . '/' . $trimestre['Trimestre']['id']); ?>
<?php $this->Html->addCrumb(__('Productometas'), '/evaluacion/producto_metas/' . $this->request->data['Poa']['id'] . '/' . $trimestre['Trimestre']['id']); ?>
<?php $this->Html->addCrumb(__('Indicadores'), '/evaluacion/indicadores/' . $this->request->data['Poa']['id'] . '/' . $trimestre['Trimestre']['id']); ?>
<div class="row-fluid poas form body-top">
	<div class="title span10">
		<h2><?php echo __('Evaluacion del %s', array($trimestre['Trimestre']['denominacion'])); ?></h2>
		<h3><?php echo __('Relacion de los Indicadores de Gestion al %s del Ejericio Economico Financiero %s', array($trimestre['Trimestre']['denominacion'], $this->request->data['Poa']['ano'])); ?></h3>
	</div>
	<div class="actions span2">
	</div>
</div>
<div class="row-fluid body-middle">
	<div class="span12">
		<?php echo $this->Form->create('Ejecucionindicadore', array('controller' => 'ejecucionindicadores', 'action' => 'add/' . $this->request->data['Poa']['id'] . '/' . $trimestre['Trimestre']['id'])); ?>
			<fieldset>
				<legend><?php echo __('Datos Basicos'); ?></legend>
				<div class="row-fluid">
					<div class="span7">
                        <dl>
                            <dt><?php echo __('DEPENDENCIA'); ?></dt>
                            <dd>
                            <?php echo h($this->request->data['Dependencia']['descripcion']); ?>
                            &nbsp;
                        </dl>
					</div>
					<div class="span1">
                        <dl>
                            <dt><?php echo __('ANO'); ?></dt>
                            <dd>
                            <?php echo h($this->request->data['Poa']['ano']); ?>
                            &nbsp;
                        </dl>
					</div>
					<div class="span4">
                        <dl>
                            <dt><?php echo __('ES REPROGRAMADO'); ?></dt>
                            <dd>
                            <?php echo ($this->request->data['Poa']['es_reprogramado']) ? __('Si') : __('No'); ?>
                            &nbsp;
                        </dl>
					</div>
				</div>
			</fieldset>
			<fieldset>
				<legend><?php echo __('Objetivos del POA'); ?></legend>
				<div class="row-fluid">
                    <div class="span12">
							<?php $i = 0; ?>
							<?php foreach($this->request->data['Objetivo'] as $objetivo): ?>
								<h4><?php echo h($objetivo['descripcion']); ?></h4>
								<div class="row-fluid">
									<div class="span11 offset1">
										<legend><?php echo __('Relacion de Indicadores'); ?></legend>
										<table cellpadding="0" cellspacing="0" class="table table-striped table-condensed">
											<thead>
												<tr>
													<th><?php echo __('Denominacion de la Meta Anual'); ?></th>
													<th><?php echo __('Variables'); ?></th>
													<th><?php echo __('Numero'); ?></th>
													<th class="span1"><?php echo __('Valor'); ?></th>
													<th><?php echo __('Resultado'); ?></th>
													<th class="span3"><?php echo __('Interpretacion de los resultados'); ?></th>
													<th class="span3"><?php echo __('Acciones correctivas'); ?></th>
												</tr>
											</thead>
											<tbody>
												<?php if ($ejecucionfisicas): ?>
													<?php $n = 0; ?>
													<?php foreach($ejecucionfisicas as $ejecucionfisica): ?>
														<?php if ($ejecucionfisica['Objetivo']['id'] == $objetivo['id']): ?>
															<?php if (isset($ejecucionfisica['Ejecucionindicadore']['id'])): ?>
																<?php echo $this->Form->input($i . '.id', array('type' => 'hidden', 'value' => $ejecucionfisica['Ejecucionindicadore']['id'])) ?>
															<?php endif; ?>
															<?php echo $this->Form->input($i . '.ejecucionevaluacione_id', array('type' => 'hidden', 'value' => $ejecucionfisica['Ejecucionevaluacione']['id'])) ?>
															<tr>
																<td rowspan="2"><strong><?php echo $ejecucionfisica['Ejecucionfisica']['cantidad_planificada']; ?></strong> <?php echo $ejecucionfisica['Meta']['descripcion']; ?></td>
																<td><?php echo $ejecucionfisica['Indicadore']['numerador']; ?></td>
																<td><?php echo $ejecucionfisica['Ejecucionevaluacione']['cantidad_ejecutada']; ?></td>
																<td rowspan="2">*100=</td>
																<td rowspan="2"><?php echo number_format(($ejecucionfisica['Ejecucionevaluacione']['cantidad_ejecutada'] / $ejecucionfisica['Ejecucionfisica']['cantidad_planificada']) * 100, 2); ?>%</td>
																<td rowspan="2"><?php echo $this->Form->input($i . '.interpretacion', array('type' => 'textarea', 'rows' => 3, 'class' => 'span12', 'label' => false, 'div' => false, 'value' => (isset($ejecucionfisica['Ejecucionindicadore']['interpretacion']) ? $ejecucionfisica['Ejecucionindicadore']['interpretacion'] : ''))) ?></td>
																<td rowspan="2"><?php echo $this->Form->input($i . '.acciones_correctivas', array('type' => 'textarea', 'rows' => 3, 'class' => 'span12', 'label' => false, 'div' => false, 'value' => (isset($ejecucionfisica['Ejecucionindicadore']['acciones_correctivas']) ? $ejecucionfisica['Ejecucionindicadore']['acciones_correctivas'] : ''))) ?></td>
															</tr>
															<tr>
																<td><?php echo $ejecucionfisica['Indicadore']['denominador']; ?></td>
																<td><?php echo $ejecucionfisica['Ejecucionfisica']['cantidad_planificada']; ?></td>
															</tr>
															<?php $i++; ?>
															<?php $n++; ?>
														<?php endif; ?>
													<?php endforeach; ?>
													<?php if ($n == 0): ?>
														<tr>
															<td colspan="7"><?php echo __('No hay Indicadores que mostrar'); ?></td>
														</tr>
													<?php endif; ?>
												<?php else: ?>
													<tr>
														<td colspan="7"><?php echo __('No hay Indicadores que mostrar'); ?></td>
													</tr>
												<?php endif; ?>
											</tbody>
										</table>
									</div>
								</div>
							<?php endforeach; ?>
                    </div>
				</div>
			</fieldset>
			<div class="form-actions row-fluid">
                <div class="span6 control-group">
					<?php echo $this->Html->link(__("Volver a Metas Productos"), array('action' => 'producto_metas', $this->request->data['Poa']['id'], $trimestre['Trimestre']['id'])); ?>
				</div>
                <div class="span6 control-group text-right">
					<?php echo $this->Form->button(__("Finalizar"), array('type' => 'submit', 'class' => 'btn btn-success')); ?>
				</div>
			</div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>
