<?php $this->Html->addCrumb(__('Evaluacion'), '/evaluacion/'); ?>
<?php $this->Html->addCrumb(__('Resumen'), '/evaluacion/principal/' . $this->request->data['Poa']['id']); ?>
<div class="row-fluid poas form body-top">
	<div class="title span10">
		<h2><?php echo __('Evaluacion'); ?></h2>
		<h3><?php echo __('Resumen de la Evaluación'); ?></h3>
	</div>
	<div class="actions span2">
		
		
		
	</div>
</div>
<div class="row-fluid body-middle">
	<div class="span12">
		<?php echo $this->Form->create('Poa'); ?>
			<?php echo $this->Form->input('id'); ?>
			<fieldset>
				<legend><?php echo __('Datos Institucionales'); ?></legend>
				<div class="row-fluid">
                    <div class="span12">
                        <dl>
                            <?php foreach($poacabeceras as $poacabecera): ?>
                                <dt><?php echo h($poacabecera['Poacabecera']['denominacion']); ?></dt>
                                <dd>
                                <?php echo h($poacabecera['Poacabecera']['descripcion']); ?>
                                &nbsp;
                            </dd>
                            <?php endforeach; ?>
                            <dt><?php echo __('ORGANO / ENTE DESCENTRALIZADO'); ?></dt>
                            <dd>
                            <?php echo h($this->request->data['Organo']['descripcion']); ?>
                            &nbsp;
                        </dl>
					</div>
				</div>
			</fieldset>
			<fieldset>
				<legend><?php echo __('Datos Basicos'); ?></legend>
				<div class="row-fluid">
					<div class="span7">
                        <dl>
                            <dt><?php echo __('DEPENDENCIA'); ?></dt>
                            <dd>
                            <?php echo h($this->request->data['Dependencia']['descripcion']); ?>
                            &nbsp;
                        </dl>
					</div>
					<div class="span1">
                        <dl>
                            <dt><?php echo __('ANO'); ?></dt>
                            <dd>
                            <?php echo h($this->request->data['Poa']['ano']); ?>
                            &nbsp;
                        </dl>
					</div>
					<div class="span4">
                        <dl>
                            <dt><?php echo __('ES REPROGRAMADO'); ?></dt>
                            <dd>
                            <?php echo ($this->request->data['Poa']['es_reprogramado']) ? __('Si') : __('No'); ?>
                            &nbsp;
                        </dl>
					</div>
				</div>
			</fieldset>
			<fieldset>
				<legend><?php echo __('Objetivos del POA'); ?></legend>
				<div class="row-fluid">
                    <div class="span12 control-group">
						<?php foreach($this->request->data['Objetivo'] as $objetivo): ?>
							<h4><?php echo $objetivo['descripcion']; ?></h4>
							<div class="row-fluid">
								<div class="offset1 span11">
									<table cellpadding="0" cellspacing="0" class="table table-striped table-condensed">
										<thead>
											<tr>
												<th><?php echo __('Metas/Productos'); ?></th>
												<th class="span4"><?php echo __('Indicadores'); ?></th>
												<th>&nbsp;</th>
												<th><?php echo __('Medioverificaciones'); ?></th>
												<th><?php echo __('Unidadmedidas'); ?></th>
												<th><?php echo __('Trimestre I'); ?></th>
												<th><?php echo __('Trimestre II'); ?></th>
												<th><?php echo __('Trimestre III'); ?></th>
												<th><?php echo __('Trimestre IV'); ?></th>
											</tr>
										</thead>
										<tbody>
											<?php //foreach($this->request->data['Objetivo'] as $objetivo): ?>
												<?php $empty = true; ?>
												<?php foreach($ejecucion_trimestral as $ejecucion): ?>
													<?php if ($ejecucion['Objetivo']['id'] == $objetivo['id']): ?>
													<?php $empty = false; ?>
														<tr>
															<td><strong><?php echo $ejecucion['Meta']['cantidad']; ?></strong> <?php echo $ejecucion['Meta']['descripcion']; ?></td>
															<td><?php echo $ejecucion['Indicadore']['resultado']; ?> = <br /><?php echo $ejecucion['Indicadore']['numerador']; ?> <hr /> <?php echo $ejecucion['Indicadore']['denominador']; ?></td>
															<td>* 100</td>
															<td><?php echo $ejecucion['Medioverificacione']['descripcion']; ?></td>
															<td><?php echo $ejecucion['Unidadmedida']['denominacion']; ?></td>
															<td<?php echo isset($ejecucion['Ejecucionevaluacione1']['cantidad_ejecutada']) ? ' class="done"' : ' class="no-done"'; ?>><?php if ($ejecucion['Ejecucionfisica1']['cantidad_planificada'] > 0): ?><?php echo ($ejecucion['Ejecucionevaluacione1']['cantidad_ejecutada']) ? $ejecucion['Ejecucionevaluacione1']['cantidad_ejecutada'] : 0; ?>/<strong><?php echo $ejecucion['Ejecucionfisica1']['cantidad_planificada']; ?></strong><br /><small><?php echo ($ejecucion['Ejecucionfisica1']['cantidad_planificada'] > 0) ? number_format(($ejecucion['Ejecucionevaluacione1']['cantidad_ejecutada'] / $ejecucion['Ejecucionfisica1']['cantidad_planificada'] * 100), 2) : 0; ?>%</small><?php else: ?>-<?php endif; ?></td>
															<td<?php echo isset($ejecucion['Ejecucionevaluacione2']['cantidad_ejecutada']) ? ' class="done"' : ' class="no-done"'; ?>><?php if ($ejecucion['Ejecucionfisica2']['cantidad_planificada'] > 0): ?><?php echo ($ejecucion['Ejecucionevaluacione2']['cantidad_ejecutada']) ? $ejecucion['Ejecucionevaluacione2']['cantidad_ejecutada'] : 0; ?>/<strong><?php echo $ejecucion['Ejecucionfisica2']['cantidad_planificada']; ?></strong><br /><small><?php echo ($ejecucion['Ejecucionfisica2']['cantidad_planificada'] > 0) ? number_format(($ejecucion['Ejecucionevaluacione2']['cantidad_ejecutada'] / $ejecucion['Ejecucionfisica2']['cantidad_planificada'] * 100), 2) : 0; ?>%</small><?php else: ?>-<?php endif; ?></td>
															<td<?php echo isset($ejecucion['Ejecucionevaluacione3']['cantidad_ejecutada']) ? ' class="done"' : ' class="no-done"'; ?>><?php if ($ejecucion['Ejecucionfisica3']['cantidad_planificada'] > 0): ?><?php echo ($ejecucion['Ejecucionevaluacione3']['cantidad_ejecutada']) ? $ejecucion['Ejecucionevaluacione3']['cantidad_ejecutada'] : 0; ?>/<strong><?php echo $ejecucion['Ejecucionfisica3']['cantidad_planificada']; ?></strong><br /><small><?php echo ($ejecucion['Ejecucionfisica3']['cantidad_planificada'] > 0) ? number_format(($ejecucion['Ejecucionevaluacione3']['cantidad_ejecutada'] / $ejecucion['Ejecucionfisica3']['cantidad_planificada'] * 100), 2) : 0; ?>%</small><?php else: ?>-<?php endif; ?></td>
															<td<?php echo isset($ejecucion['Ejecucionevaluacione4']['cantidad_ejecutada']) ? ' class="done"' : ' class="no-done"'; ?>><?php if ($ejecucion['Ejecucionfisica4']['cantidad_planificada'] > 0): ?><?php echo ($ejecucion['Ejecucionevaluacione4']['cantidad_ejecutada']) ? $ejecucion['Ejecucionevaluacione4']['cantidad_ejecutada'] : 0; ?>/<strong><?php echo $ejecucion['Ejecucionfisica4']['cantidad_planificada']; ?></strong><br /><small><?php echo ($ejecucion['Ejecucionfisica4']['cantidad_planificada'] > 0) ? number_format(($ejecucion['Ejecucionevaluacione4']['cantidad_ejecutada'] / $ejecucion['Ejecucionfisica4']['cantidad_planificada'] * 100), 2) : 0; ?>%</small><?php else: ?>-<?php endif; ?></td>
														</tr>
													<?php endif; ?>
												<?php endforeach; ?>
											<?php //endforeach; ?>
											<?php if ($empty): ?>
												<tr><td colspan="8"><?php echo __('No hay evaluacion registrada para el presente objetivo'); ?></td></tr>
											<?php endif; ?>
										</tbody>
									</table>
								</div>
							</div>
						<?php endforeach; ?>
                    </div>
				</div>
			</fieldset>
			<div class="form-actions row-fluid">
				<?php foreach ($trimestres as $trimestre): ?>
					<div class="span3 text-center">
						<?php echo $this->Html->link(__("Evaluacion %s", $trimestre['Trimestre']['denominacion']), array('action' => 'ejecucion_fisica', $this->request->data['Poa']['id'], $trimestre['Trimestre']['id']), array('class' => 'btn btn-primary')); ?>
					</div>
				<?php endforeach; ?>
			</div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>
