<div class="alert alert-block">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <strong>Atención!</strong> <?php echo $message; ?>
</div>