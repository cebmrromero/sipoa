<?php $i = 0; ?>
<div class="row-fluid">
    <div class="span11 offset1">
        <legend><?php echo __('Metas/Productos'); ?> <?php echo $this->Html->link(__('<i class="icon-plus-sign icon-white"></i> Agregar'), array('controller' => 'metas', 'action' => 'add', $this->request->data['Poa']['id'], $objetivo['id']), array("title" => __('Agregar registro'), 'class' => 'label label-important', 'escape' => false)); ?></legend>
        <div class="row-fluid">
            <div class="span1 text-right">
                <?php echo $this->Form->label('Meta.' . $i . '.cantidad', __('Meta')); ?>
            </div>
            <div class="span7">
                <?php echo $this->Form->label('Meta.' . $i . '.descripcion', __('Producto')); ?>
            </div>
            <div class="span2 text-right">
                <?php echo $this->Form->label('Meta.' . $i . '.costo'); ?>
            </div>
        </div>
        <?php if ($this->request->data['Meta']): ?>
            <?php foreach($this->request->data['Meta'] as $meta): ?>
                <?php if ($meta['objetivo_id'] == $objetivo['id']): ?>
                    <div class="row-fluid">
                        <div class="span1 text-right">
                            <strong><?php echo $meta['cantidad']; ?></strong>
                        </div>
                        <div class="span7">
                            <?php echo $meta['descripcion']; ?>
                        </div>
                        <div class="span2 text-right">
                            <?php echo $meta['costo']; ?>
                        </div>
                        <div class="span2 text-right">
                            <?php echo $this->Html->link(__('<i class="icon-trash icon-white"></i>'), array('controller' => 'metas', 'action' => 'delete', $meta['id']), array("class" => "label label-important", "escape" => false, "title" => __('Eliminar registro')), __('¿Está de acuerdo con eliminar este registro?')); ?>
                            <?php echo $this->Html->link(__('<i class="icon-edit icon-white"></i>'), array('controller' => 'metas', 'action' => 'edit', $meta['id'], $this->request->data['Poa']['id']), array("class" => "label label-info", "escape" => false, "title" => __('Editar'))); ?>
                            <?php echo $this->Html->link(__('<i class="icon-chevron-right icon-white"></i>'), array('action' => 'indicadores', $this->request->data['Poa']['id'], $meta['id'], ), array("class" => "label label-success", "escape" => false, "title" => __('Continuar'))); ?>
                        </div>
                    </div>
                <?php endif; ?>
                <?php $i++; ?>
            <?php endforeach; ?>
        <?php else: ?>
            <div class="row-fluid">
                <p><?php echo __('El Objetivo no tiene Metas asociadas, debe proceder a crearlas'); ?></p>
            </div>
        <?php endif; ?>
    </div>
</div>