<?php $i = 0; ?>
<div class="row-fluid">
    <div class="span11 offset1">
        <legend><?php echo $this->Html->link(__('Meta/Producto'), array('action' => 'metas', $this->request->data['Poa']['id'])); ?></legend>
        <?php //foreach($metas as $meta): ?>
            <?php //if ($meta['Meta']['objetivo_id'] == $objetivo['id']): ?>
                <div class="row-fluid">
                    <div class="span12">
                        <h5><strong><?php echo $indicadore['Meta']['cantidad']; ?></strong> <?php echo $indicadore['Meta']['descripcion']; ?></h5>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span11 offset1">
                        <legend><?php echo $this->Html->link(__('Indicadore'), array('action' => 'indicadores', $this->request->data['Poa']['id'], $indicadore['Meta']['id'])); ?></legend>
                        <?php //foreach ($meta['Indicadore'] as $indicadore): ?>
                            <div class="row-fluid">
                                <div class="span12">
                                    <h5>
                                        <div class="span2">
                                            <?php echo $indicadore['Indicadore']['resultado']; ?>
                                        </div>
                                        <div class="span1">
                                            <label>&nbsp;</label>
                                            =
                                        </div>
                                        <div class="span2">
                                            <?php echo $indicadore['Indicadore']['numerador']; ?>
                                        </div>
                                        <div class="span1">
                                            <label>&nbsp;</label>
                                            /
                                        </div>
                                        <div class="span2">
                                            <?php echo $indicadore['Indicadore']['denominador']; ?>
                                        </div>
                                        <div class="span1">
                                            <label>&nbsp;</label>
                                            x
                                        </div>
                                        <div class="span1">
                                            <label>&nbsp;</label>
                                            100
                                        </div>
                                    </h5>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span11 offset1">
                                    <legend><?php echo __('Medioverificacione'); ?> <?php echo $this->Html->link(__('<i class="icon-plus-sign icon-white"></i> Agregar'), array('controller' => 'medioverificaciones', 'action' => 'add', $indicadore['Indicadore']['id']), array("title" => __('Agregar registro'), 'class' => 'label label-important', 'escape' => false)); ?></legend>
                                    <?php if ($indicadore['Medioverificacione']): ?>
                                        <?php foreach ($indicadore['Medioverificacione'] as $medioverificacione): ?>
                                            <div class="row-fluid">
                                                <div class="span9">
                                                    <h5><?php echo $medioverificacione['descripcion']; ?></h5>
                                                </div>
                                                <div class="span3 text-right">
                                                    <?php echo $this->Html->link(__('<i class="icon-trash icon-white"></i>'), array('controller' => 'medioverificaciones', 'action' => 'delete', $medioverificacione['id']), array("class" => "label label-important", "escape" => false, "title" => __('Eliminar registro')), __('¿Está de acuerdo con eliminar este registro?')); ?>
                                                    <?php echo $this->Html->link(__('<i class="icon-edit icon-white"></i>'), array('controller' => 'medioverificaciones', 'action' => 'edit', $medioverificacione['id'], $this->request->data['Poa']['id']), array("class" => "label label-info", "escape" => false, "title" => __('Editar'))); ?>
                                                    <?php echo $this->Html->link(__('<i class="icon-chevron-right icon-white"></i>'), array('action' => 'unidadm_supuestos', $this->request->data['Poa']['id'], $medioverificacione['id']), array("class" => "label label-success", "escape" => false, "title" => __('Continuar'))); ?>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    <?php else: ?>
                                        <div class="row-fluid">
                                            <p><?php echo __('No hay Medios de verificacion asociados, debe proceder a registrarlos'); ?></p>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php //endforeach; ?>
                    </div>
                </div>
            <?php //endif; ?>
            <?php $i++; ?>
        <?php //endforeach; ?>
    </div>
</div>