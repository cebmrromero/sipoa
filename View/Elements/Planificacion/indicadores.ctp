<?php $i = 0; ?>
<div class="row-fluid">
    <div class="span11 offset1">
        <legend><?php echo $this->Html->link(__('Meta/Producto'), array('action' => 'metas', $this->request->data['Poa']['id'])); ?></legend>
        <?php //foreach($metas as $meta): ?>
            <?php //if ($meta['Meta']['objetivo_id'] == $objetivo['id']): ?>
                <div class="row-fluid">
                    <div class="span12">
                        <h5><strong><?php echo $meta['Meta']['cantidad']; ?></strong> <?php echo $meta['Meta']['descripcion']; ?></h5>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span11 offset1">
                        <legend><?php echo __('Indicadores'); ?> <?php echo $this->Html->link(__('<i class="icon-plus-sign icon-white"></i> Agregar'), array('controller' => 'indicadores', 'action' => 'add', $meta['Meta']['id']), array("title" => __('Agregar registro'), 'class' => 'label label-important', 'escape' => false)); ?></legend>
                        <div class="row-fluid">
                            <div class="span2">
                                <?php echo $this->Form->label('Indicadore.' . $i . '.resultado'); ?>
                            </div>
                            <div class="span1"></div>
                            <div class="span2">
                                <?php echo $this->Form->label('Indicadore.' . $i . '.numerador'); ?>
                            </div>
                            <div class="span1"></div>
                            <div class="span2">
                                <?php echo $this->Form->label('Indicadore.' . $i . '.denominador'); ?>
                            </div>
                        </div>
                        <?php if ($meta['Indicadore']): ?>
                            <?php foreach ($meta['Indicadore'] as $indicadore): ?>
                                <div class="row-fluid">
                                    <div class="span2">
                                        <?php echo $indicadore['resultado']; ?>
                                    </div>
                                    <div class="span1">
                                        =
                                    </div>
                                    <div class="span2">
                                        <?php echo $indicadore['numerador']; ?>
                                    </div>
                                    <div class="span1">
                                        /
                                    </div>
                                    <div class="span2">
                                        <?php echo $indicadore['denominador']; ?>
                                    </div>
                                    <div class="span1">
                                        x
                                    </div>
                                    <div class="span1">
                                        100
                                    </div>
                                    <div class="span2 text-right">
                                        <?php echo $this->Html->link(__('<i class="icon-trash icon-white"></i>'), array('controller' => 'indicadores', 'action' => 'delete', $indicadore['id']), array("class" => "label label-important", "escape" => false, "title" => __('Eliminar registro')), __('¿Está de acuerdo con eliminar este registro?')); ?>
                                        <?php echo $this->Html->link(__('<i class="icon-edit icon-white"></i>'), array('controller' => 'indicadores', 'action' => 'edit', $indicadore['id'], $this->request->data['Poa']['id']), array("class" => "label label-info", "escape" => false, "title" => __('Editar'))); ?>
                                        <?php echo $this->Html->link(__('<i class="icon-chevron-right icon-white"></i>'), array('action' => 'medioverificaciones', $this->request->data['Poa']['id'], $indicadore['id']), array("class" => "label label-success", "escape" => false, "title" => __('Continuar'))); ?>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <div class="row-fluid">
                                <p><?php echo __('No hay Indicadores asociados, debe proceder a registrarlos'); ?></p>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            <?php //endif; ?>
            <?php $i++; ?>
        <?php //endforeach; ?>
    </div>
</div>