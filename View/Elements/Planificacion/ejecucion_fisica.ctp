<?php $i = 0; ?>
<div class="row-fluid">
    <div class="span11 offset1">
        <legend><?php echo $this->Html->link(__('Meta/Producto'), array('action' => 'metas', $this->request->data['Poa']['id'])); ?></legend>
        <?php //foreach($metas as $meta): ?>
            <?php //if ($meta['Meta']['objetivo_id'] == $objetivo['id']): ?>
                <div class="row-fluid">
                    <div class="span12">
                        <h5><strong><?php echo $unidadmedida['Medioverificacione']['Indicadore']['Meta']['cantidad']; ?></strong> <?php echo $unidadmedida['Medioverificacione']['Indicadore']['Meta']['descripcion']; ?></h5>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span11 offset1">
                        <legend><?php echo $this->Html->link(__('Indicadore'), array('action' => 'indicadores', $this->request->data['Poa']['id'], $unidadmedida['Medioverificacione']['Indicadore']['Meta']['id'])); ?></legend>
                        <?php //foreach ($meta['Indicadore'] as $indicadore): ?>
                            <div class="row-fluid">
                                <div class="span12">
                                    <h5>
                                        <div class="span2">
                                            <?php echo $unidadmedida['Medioverificacione']['Indicadore']['resultado']; ?>
                                        </div>
                                        <div class="span1">
                                            <label>&nbsp;</label>
                                            =
                                        </div>
                                        <div class="span2">
                                            <?php echo $unidadmedida['Medioverificacione']['Indicadore']['numerador']; ?>
                                        </div>
                                        <div class="span1">
                                            <label>&nbsp;</label>
                                            /
                                        </div>
                                        <div class="span2">
                                            <?php echo $unidadmedida['Medioverificacione']['Indicadore']['denominador']; ?>
                                        </div>
                                        <div class="span1">
                                            <label>&nbsp;</label>
                                            x
                                        </div>
                                        <div class="span1">
                                            <label>&nbsp;</label>
                                            100
                                        </div>
                                    </h5>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span11 offset1">
                                    <legend><?php echo $this->Html->link(__('Medioverificacione'), array('action' => 'medioverificaciones', $this->request->data['Poa']['id'], $unidadmedida['Medioverificacione']['Indicadore']['id'])); ?></legend>
                                    <?php //foreach ($indicadore['Medioverificacione'] as $medioverificacione): ?>
                                        <div class="row-fluid">
                                            <div class="span11">
                                                <h5><?php echo $unidadmedida['Medioverificacione']['descripcion']; ?></h5>
                                            </div>
                                        </div>
                                        <div class="row-fluid">
                                            <div class="span11 offset1">
                                                <legend><?php echo $this->Html->link(__('Unidadmedida'), array('action' => 'unidadm_supuestos', $this->request->data['Poa']['id'], $unidadmedida['Medioverificacione']['id'])); ?></legend>
                                                <?php //foreach ($medioverificacione['Unidadmedida'] as $unidadmedida): ?>
                                                    <div class="row-fluid">
                                                        <div class="span11">
                                                            <h5><?php echo $unidadmedida['Unidadmedida']['denominacion']; ?></h5>
                                                        </div>
                                                        <div class="row-fluid">
                                                            <div class="span11 offset1">
                                                                <?php if ($unidadmedida['Ejecucionfisica']): ?>
                                                                    <legend><?php echo __('Ejecucionfisica'); ?></legend>
                                                                <?php else: ?>
                                                                    <legend><?php echo __('Ejecucionfisica'); ?> <?php echo $this->Html->link(__('<i class="icon-plus-sign icon-white"></i> Agregar'), array('controller' => 'ejecucionfisicas', 'action' => 'add', $unidadmedida['Unidadmedida']['id']), array("title" => __('Agregar registro'), 'class' => 'label label-important', 'escape' => false)); ?></legend>
                                                                <?php endif; ?>
                                                                <div class="row-fluid">
                                                                    <div class="span2">
                                                                        <h5><?php echo __('Trimestre I'); ?></h5>
                                                                    </div>
                                                                    <div class="span2">
                                                                        <h5><?php echo __('Trimestre II'); ?></h5>
                                                                    </div>
                                                                    <div class="span2">
                                                                        <h5><?php echo __('Trimestre III'); ?></h5>
                                                                    </div>
                                                                    <div class="span2">
                                                                        <h5><?php echo __('Trimestre IV'); ?></h5>
                                                                    </div>
                                                                </div>
                                                                <div class="row-fluid">
                                                                    <?php if ($unidadmedida['Ejecucionfisica']): ?>
                                                                        <?php foreach ($unidadmedida['Ejecucionfisica'] as $ejecucionfisica): ?>
                                                                            <div class="span2">
                                                                                <h5><?php echo $ejecucionfisica['cantidad_planificada']; ?></h5>
                                                                            </div>
                                                                        <?php endforeach; ?>
                                                                        <div class="span4 text-right">
                                                                            <?php echo $this->Html->link(__('<i class="icon-trash icon-white"></i>'), array('controller' => 'ejecucionfisicas', 'action' => 'delete', $ejecucionfisica['id']), array("class" => "label label-important", "escape" => false, "title" => __('Eliminar registro')), __('¿Está de acuerdo con eliminar este registro?')); ?>
                                                                        </div>
                                                                    <?php else: ?>
                                                                        <div class="row-fluid">
                                                                            <p><?php echo __('No hay Ejecucionfisica asociada, debe proceder a registrarla'); ?></p>
                                                                        </div>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php //endforeach; ?>
                                            </div>
                                        </div>
                                    <?php //endforeach; ?>
                                </div>
                            </div>
                        <?php //endforeach; ?>
                    </div>
                </div>
            <?php //endif; ?>
            <?php $i++; ?>
        <?php //endforeach; ?>
    </div>
</div>