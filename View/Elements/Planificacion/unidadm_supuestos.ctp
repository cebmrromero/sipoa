<?php $i = 0; ?>
<div class="row-fluid">
    <div class="span11 offset1">
        <legend><?php echo $this->Html->link(__('Meta/Producto'), array('action' => 'metas', $this->request->data['Poa']['id'])); ?></legend>
        <?php //foreach($metas as $meta): ?>
            <?php //if ($meta['Meta']['objetivo_id'] == $objetivo['id']): ?>
                <div class="row-fluid">
                    <div class="span12">
                        <h5><strong><?php echo $medioverificacione['Indicadore']['Meta']['cantidad']; ?></strong> <?php echo $medioverificacione['Indicadore']['Meta']['descripcion']; ?></h5>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span11 offset1">
                        <legend><?php echo $this->Html->link(__('Indicadore'), array('action' => 'indicadores', $this->request->data['Poa']['id'], $medioverificacione['Indicadore']['Meta']['id'])); ?></legend>
                        <?php //foreach ($meta['Indicadore'] as $indicadore): ?>
                            <div class="row-fluid">
                                <div class="span12">
                                    <h5>
                                        <div class="span2">
                                            <?php echo $medioverificacione['Indicadore']['resultado']; ?>
                                        </div>
                                        <div class="span1">
                                            <label>&nbsp;</label>
                                            =
                                        </div>
                                        <div class="span2">
                                            <?php echo $medioverificacione['Indicadore']['numerador']; ?>
                                        </div>
                                        <div class="span1">
                                            <label>&nbsp;</label>
                                            /
                                        </div>
                                        <div class="span2">
                                            <?php echo $medioverificacione['Indicadore']['denominador']; ?>
                                        </div>
                                        <div class="span1">
                                            <label>&nbsp;</label>
                                            x
                                        </div>
                                        <div class="span1">
                                            <label>&nbsp;</label>
                                            100
                                        </div>
                                    </h5>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span11 offset1">
                                    <legend><?php echo $this->Html->link(__('Medioverificacione'), array('action' => 'medioverificaciones', $this->request->data['Poa']['id'], $medioverificacione['Indicadore']['id'])); ?></legend>
                                    <?php //foreach ($indicadore['Medioverificacione'] as $medioverificacione): ?>
                                        <div class="row-fluid">
                                            <div class="span11">
                                                <h5><?php echo $medioverificacione['Medioverificacione']['descripcion']; ?></h5>
                                            </div>
                                        </div>
                                        <div class="row-fluid">
                                            <div class="span11 offset1">
                                                <legend><?php echo __('Supuestos'); ?> <?php echo $this->Html->link(__('<i class="icon-plus-sign icon-white"></i> Agregar'), array('controller' => 'supuestos', 'action' => 'add', $medioverificacione['Medioverificacione']['id']), array("title" => __('Agregar registro'), 'class' => 'label label-important', 'escape' => false)); ?></legend>
                                                <?php if ($medioverificacione['Supuesto']): ?>
                                                    <?php foreach ($medioverificacione['Supuesto'] as $supuesto): ?>
                                                        <div class="row-fluid">
                                                            <div class="span10">
                                                                <h5><?php echo $supuesto['descripcion']; ?></h5>
                                                            </div>
                                                            <div class="span2 text-right">
                                                                <?php echo $this->Html->link(__('<i class="icon-trash icon-white"></i>'), array('controller' => 'supuestos', 'action' => 'delete', $supuesto['id']), array("class" => "label label-important", "escape" => false, "title" => __('Eliminar registro')), __('¿Está de acuerdo con eliminar este registro?')); ?>
                                                                <?php echo $this->Html->link(__('<i class="icon-edit icon-white"></i>'), array('controller' => 'supuestos', 'action' => 'edit', $supuesto['id'], $this->request->data['Poa']['id']), array("class" => "label label-info", "escape" => false, "title" => __('Editar'))); ?>
                                                            </div>
                                                        </div>
                                                    <?php endforeach; ?>
                                                <?php else: ?>
                                                    <div class="row-fluid">
                                                        <p><?php echo __('No hay Supuestos asociados, debe proceder a registrarlos'); ?></p>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="row-fluid">
                                            <div class="span11 offset1">
                                                <?php if ($medioverificacione['Unidadmedida']): ?>
                                                    <legend><?php echo __('Unidadmedida'); ?></legend>
                                                    <?php foreach ($medioverificacione['Unidadmedida'] as $unidadmedida): ?>
                                                        <div class="row-fluid">
                                                            <div class="span9">
                                                                <h5><?php echo $unidadmedida['denominacion']; ?></h5>
                                                            </div>
                                                            <div class="span3 text-right">
                                                                <?php echo $this->Html->link(__('<i class="icon-trash icon-white"></i>'), array('controller' => 'unidadmedidas', 'action' => 'delete', $unidadmedida['id']), array("class" => "label label-important", "escape" => false, "title" => __('Eliminar registro')), __('¿Está de acuerdo con eliminar este registro?')); ?>
                                                                <?php echo $this->Html->link(__('<i class="icon-edit icon-white"></i>'), array('controller' => 'unidadmedidas', 'action' => 'edit', $unidadmedida['id'], $this->request->data['Poa']['id']), array("class" => "label label-info", "escape" => false, "title" => __('Editar'))); ?>
                                                                <?php echo $this->Html->link(__('<i class="icon-chevron-right icon-white"></i>'), array('action' => 'ejecucion_fisica', $this->request->data['Poa']['id'], $unidadmedida['id']), array("class" => "label label-success", "escape" => false, "title" => __('Continuar'))); ?>
                                                            </div>
                                                        </div>
                                                    <?php endforeach; ?>
                                                <?php else: ?>
                                                    <legend><?php echo __('Unidadmedida'); ?> <?php echo $this->Html->link(__('<i class="icon-plus-sign icon-white"></i> Agregar'), array('controller' => 'unidadmedidas', 'action' => 'add', $medioverificacione['Medioverificacione']['id']), array("title" => __('Agregar registro'), 'class' => 'label label-important', 'escape' => false)); ?></legend>
                                                    <div class="row-fluid">
                                                        <p><?php echo __('No hay Unidadmedida asociadas, debe proceder a registrarlos'); ?></p>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    <?php //endforeach; ?>
                                </div>
                            </div>
                        <?php //endforeach; ?>
                    </div>
                </div>
            <?php //endif; ?>
            <?php $i++; ?>
        <?php //endforeach; ?>
    </div>
</div>