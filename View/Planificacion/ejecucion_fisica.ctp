<?php $this->Html->addCrumb(__('Planificacion'), '/planificacion/'); ?>
<?php $this->Html->addCrumb(__('Metas/Productos'), '/planificacion/metas/' . $this->request->data['Poa']['id']); ?>
<?php $this->Html->addCrumb(__('Indicadores'), '/planificacion/indicadores/' . $this->request->data['Poa']['id']. '/' . $unidadmedida['Medioverificacione']['Indicadore']['Meta']['id']); ?>
<?php $this->Html->addCrumb(__('Medioverificaciones'), '/planificacion/medioverificaciones/' . $this->request->data['Poa']['id']. '/' . $unidadmedida['Medioverificacione']['Indicadore']['id']); ?>
<?php $this->Html->addCrumb(__('UnidadmSupuestos'), '/planificacion/unidadm_supuestos/' . $this->request->data['Poa']['id']. '/' . $unidadmedida['Medioverificacione']['id']); ?>
<?php $this->Html->addCrumb(__('Ejecucionficia'), '/planificacion/ejecucion_fisica/' . $this->request->data['Poa']['id']. '/' . $unidadmedida['Unidadmedida']['id']); ?>
<div class="row-fluid poas form body-top">
	<div class="title span10">
		<h2><?php echo __('Planificacion'); ?></h2>
		<h3><?php echo __('Asignacion de Ejecucion Fisica'); ?></h3>
	</div>
	<div class="actions span2">
		<ul class="inline-menu pull-right">
			<li><?php echo $this->Html->link(__('Editar'), array('controller' => 'poas', 'action' => 'edit',  $this->request->data['Poa']['id'])); ?></li>
		</ul>
	</div>
</div>
<div class="row-fluid body-middle">
	<div class="span12">
		<?php echo $this->Form->create('Poa'); ?>
			<?php echo $this->Form->input('id'); ?>
			<fieldset>
				<legend><?php echo __('Datos Basicos'); ?></legend>
				<div class="row-fluid">
					<div class="span7">
                        <dl>
                            <dt><?php echo __('DEPENDENCIA'); ?></dt>
                            <dd>
                            <?php echo h($this->request->data['Dependencia']['descripcion']); ?>
                            &nbsp;
                        </dl>
					</div>
					<div class="span1">
                        <dl>
                            <dt><?php echo __('ANO'); ?></dt>
                            <dd>
                            <?php echo h($this->request->data['Poa']['ano']); ?>
                            &nbsp;
                        </dl>
					</div>
					<div class="span4">
                        <dl>
                            <dt><?php echo __('ES REPROGRAMADO'); ?></dt>
                            <dd>
                            <?php echo ($this->request->data['Poa']['es_reprogramado']) ? __('Si') : __('No'); ?>
                            &nbsp;
                        </dl>
					</div>
				</div>
			</fieldset>
			<fieldset>
				<legend><?php echo __('Objetivo del POA'); ?></legend>
				<div class="row-fluid">
					<?php //print_r($this->request->data); ?>
                    <div class="span12">
						<?php //foreach($this->request->data['Objetivo'] as $objetivo): ?>
							<h4><?php echo h($unidadmedida['Medioverificacione']['Indicadore']['Meta']['Objetivo']['descripcion']); ?></h4>
							<?php echo $this->element('Planificacion/ejecucion_fisica'); ?>
						<?php //endforeach; ?>
                    </div>
				</div>
			</fieldset>
			<div class="form-actions row-fluid">
                <div class="span6 control-group">
					<?php echo $this->Html->link(__("Volver a Unidades de Medida y Supuestos"), array('action' => 'unidadm_supuestos', $this->request->data['Poa']['id'], $unidadmedida['Medioverificacione']['id'])); ?>
				</div>
                <div class="span6 control-group text-right">
					<?php echo $this->Form->button(__("Finalizar"), array('type' => 'submit', 'class' => 'btn btn-success')); ?>
				</div>
			</div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>
