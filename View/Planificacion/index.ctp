<?php $this->Html->addCrumb(__('Planificacion'), '/planificacion/index'); ?>
<div class="row-fluid body-top">
	<div class="title span7">
		<h2><?php echo __("Planificacion"); ?></h2>
	</div>
	<div class="actions span5">
		<ul class="inline-menu pull-right">
			<li><?php echo $this->Html->link(__('Agregar'), array('controller' => 'poas', 'action' => 'add')); ?></li>
		</ul>
	</div>
</div>
<div class="row-fluid body-middle">
	<div class="span12">
		<p><?php echo __("A continuacion se encuenta un listado con todos los POAs registrados en cada dependencia:"); ?></p>
		<fieldset>
			<?php foreach ($dependencias as $dependencia): ?>
				<legend><?php echo $dependencia['Dependencia']['descripcion']; ?></legend>
				<?php $poas = $dependencia['Poa']; ?>
				<table cellpadding="0" cellspacing="0" class="table table-striped table-condensed">
					<tbody>
						<?php if ($poas): ?>
							<?php foreach ($poas as $poa): ?>
								<tr<?php echo ($poa['status']) ? '' : ' class="highlight"'; ?>>
									<td><?php echo h($poa['ano']); ?>&nbsp;</td>
									<td><?php echo ($poa['es_reprogramado']) ? __('Reprogramacion') . ' #' . $poa['numero_reprogramacion'] : '-'; ?>&nbsp;</td>
									<td class="actions btn-group span1">
										<?php if ($poa['status']): ?>
											<?php echo $this->Html->link(__('<i class="icon-chevron-right icon-white"></i>'), array('action' => 'principal', $poa['id']), array("class" => "label label-important", "escape" => false, "title" => __('Planificacion'))); ?>
										<?php else: ?>
											<?php echo $this->Html->link(__('<i class="icon-zoom-in icon-white"></i>'), array('action' => 'principal', $poa['id']), array("class" => "label", "escape" => false, "title" => __('Planificacion'))); ?>
										<?php endif; ?>
									</td>
								</tr>
							<?php endforeach; ?>
						<?php else: ?>
							<tr>
								<td colspan="5"><?php echo __('No hay poas registrados para la dependencia'); ?></td>
							</tr>
						<?php endif; ?>
					</tbody>
				</table>
			<?php endforeach; ?>
		</fieldset>
	</div>
</div>
