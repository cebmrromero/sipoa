<?php $this->Html->addCrumb(__('Planificacion'), '/planificacion/'); ?>
<?php $this->Html->addCrumb(__('Resumen'), '/planificacion/principal/' . $this->request->data['Poa']['id']); ?>
<div class="row-fluid poas form body-top">
	<div class="title span10">
		<h2><?php echo __('Planificacion'); ?></h2>
		<h3><?php echo __('Resumen de la Planificación'); ?></h3>
	</div>
	<div class="actions span2">
		<ul class="inline-menu pull-right">
			<li><?php echo $this->Html->link(__('Editar'), array('controller' => 'poas', 'action' => 'edit',  $this->request->data['Poa']['id'])); ?></li>
		</ul>
	</div>
</div>
<div class="row-fluid body-middle">
	<div class="span12">
		<?php echo $this->Form->create('Poa'); ?>
			<?php echo $this->Form->input('id'); ?>
			<fieldset>
				<legend><?php echo __('Datos Institucionales'); ?></legend>
				<div class="row-fluid">
                    <div class="span12">
                        <dl>
                            <?php foreach($poacabeceras as $poacabecera): ?>
                                <dt><?php echo h($poacabecera['Poacabecera']['denominacion']); ?></dt>
                                <dd>
                                <?php echo h($poacabecera['Poacabecera']['descripcion']); ?>
                                &nbsp;
                            </dd>
                            <?php endforeach; ?>
                            <dt><?php echo __('ORGANO / ENTE DESCENTRALIZADO'); ?></dt>
                            <dd>
                            <?php echo h($this->request->data['Organo']['descripcion']); ?>
                            &nbsp;
                        </dl>
					</div>
				</div>
			</fieldset>
			<fieldset>
				<legend><?php echo __('Datos Basicos'); ?></legend>
				<div class="row-fluid">
					<div class="span7">
                        <dl>
                            <dt><?php echo __('DEPENDENCIA'); ?></dt>
                            <dd>
                            <?php echo h($this->request->data['Dependencia']['descripcion']); ?>
                            &nbsp;
                        </dl>
					</div>
					<div class="span1">
                        <dl>
                            <dt><?php echo __('ANO'); ?></dt>
                            <dd>
                            <?php echo h($this->request->data['Poa']['ano']); ?>
                            &nbsp;
                        </dl>
					</div>
					<div class="span4">
                        <dl>
                            <dt><?php echo __('ES REPROGRAMADO'); ?></dt>
                            <dd>
                            <?php echo ($this->request->data['Poa']['es_reprogramado']) ? __('Si') : __('No'); ?>
                            &nbsp;
                        </dl>
					</div>
				</div>
			</fieldset>
			<fieldset>
				<legend><?php echo __('Objetivos del POA'); ?></legend>
				<div class="row-fluid">
                    <div class="span12 control-group">
						<?php foreach($this->request->data['Objetivo'] as $objetivo): ?>
							<h4><?php echo $objetivo['descripcion']; ?></h4>
							<div class="row-fluid">
								<div class="offset1 span11">
									<table cellpadding="0" cellspacing="0" class="table table-striped table-condensed">
										<thead>
											<tr>
												<th rowspan="2"><?php echo __('Metas/Productos'); ?></th>
												<th rowspan="2"><?php echo __('Indicadores'); ?></th>
												<th rowspan="2">&nbsp;</th>
												<th rowspan="2"><?php echo __('Medioverificaciones'); ?></th>
												<th rowspan="2"><?php echo __('Unidadmedidas'); ?></th>
												<th colspan="4"><?php echo __('Trimestres'); ?></th>
											</tr>
											<tr>
												<th><?php echo __('I'); ?></th>
												<th><?php echo __('II'); ?></th>
												<th><?php echo __('III'); ?></th>
												<th><?php echo __('IV'); ?></th>
											</tr>
										</thead>
										<tbody>
											<?php //foreach($this->request->data['Objetivo'] as $objetivo): ?>
												<?php $empty = true; ?>
												<?php foreach($planificacion_trimestral as $planificacion): ?>
													<?php if ($planificacion['Objetivo']['id'] == $objetivo['id']): ?>
														<?php $empty = false; ?>
														<tr>
															<td><strong><?php echo $planificacion['Meta']['cantidad']; ?></strong> <?php echo $planificacion['Meta']['descripcion']; ?></td>
															<td><?php echo $planificacion['Indicadore']['resultado']; ?> = <br /><?php echo $planificacion['Indicadore']['numerador']; ?> <hr /> <?php echo $planificacion['Indicadore']['denominador']; ?></td>
															<td>* 100</td>
															<td><?php echo $planificacion['Medioverificacione']['descripcion']; ?></td>
															<td><?php echo $planificacion['Unidadmedida']['denominacion']; ?></td>
															<td><?php echo ($planificacion['Ejecucionfisica1']['cantidad_planificada']) ? $planificacion['Ejecucionfisica1']['cantidad_planificada'] : '0'; ?></td>
															<td><?php echo ($planificacion['Ejecucionfisica2']['cantidad_planificada']) ? $planificacion['Ejecucionfisica2']['cantidad_planificada'] : '0'; ?></td>
															<td><?php echo ($planificacion['Ejecucionfisica3']['cantidad_planificada']) ? $planificacion['Ejecucionfisica3']['cantidad_planificada'] : '0'; ?></td>
															<td><?php echo ($planificacion['Ejecucionfisica4']['cantidad_planificada']) ? $planificacion['Ejecucionfisica4']['cantidad_planificada'] : '0'; ?></td>
														</tr>
													<?php endif; ?>
												<?php endforeach; ?>
											<?php //endforeach; ?>
											<?php if ($empty): ?>
												<tr><td colspan="8"><?php echo __('No hay planificacion registrada para el presente objetivo'); ?></td></tr>
											<?php endif; ?>
										</tbody>
									</table>
								</div>
							</div>
						<?php endforeach; ?>
                    </div>
				</div>
			</fieldset>
			<div class="form-actions row-fluid">
                <div class="span6 control-group"></div>
                <div class="span6 control-group text-right">
					<?php echo $this->Form->button(__("Continuar con Asignacion de Metas"), array('type' => 'submit', 'class' => 'btn btn-primary')); ?>
				</div>
			</div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>
