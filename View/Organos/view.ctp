<?php $this->Html->addCrumb(__('Organos'), '/organos/index'); ?>
<?php $this->Html->addCrumb(__('Visualizar'), '/organos/view/' . $organo['Organo']['id']); ?>
<div class="row-fluid organos view body-top">
	<div class="title span7">
		<h2><?php echo __('Visualizar Organo'); ?></h2>
	</div>
	<div class="actions span5">
		<ul class="inline-menu pull-right">
			<li class="active"><?php echo $this->Html->link(__('Visualizar'), array('action' => 'view', $organo['Organo']['id'])); ?></li>
			<li><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $organo['Organo']['id'])); ?></li>
			<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
			<li><?php echo $this->Html->link(__('Agregar'), array('action' => 'add')); ?></li>
		</ul>
	</div>
</div>

<div class="row-fluid body-middle">
	<div class="span12">
		<dl>
			<dt><?php echo __('Descripcion'); ?></dt>
				<dd>
				<?php echo h($organo['Organo']['descripcion']); ?>
				&nbsp;
			</dd>
		</dl>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related %s', 'Poas'); ?></h3>
	<?php if (!empty($organo['Poa'])): ?>
	<table cellpadding="0" cellspacing="0" class="table table-striped table-condensed">
	<tr>
		<th><?php echo __('Dependencia'); ?></th>
		<th><?php echo __('Ano'); ?></th>
		<th><?php echo __('Es Reprogramado'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($organo['Poa'] as $poa): ?>
		<tr>
			<td><?php echo $poa['Dependencia']['descripcion']; ?></td>
			<td><?php echo $poa['ano']; ?></td>
			<td><?php echo ($poa['es_reprogramado']) ? __('Si') : __('No'); ?></td>
			<td class="actions btn-group span1">
				<?php echo $this->Html->link(__('<i class="icon-zoom-in icon-white"></i>'), array('controller' => 'poas', 'action' => 'view', $poa['id']), array("class" => "btn btn-small btn-primary", "escape" => false, "title" => __('Visualizar registro'))); ?>
				<?php echo $this->Html->link(__('<i class="icon-edit icon-white"></i>'), array('controller' => 'poas', 'action' => 'edit', $poa['id']), array("class" => "btn btn-small btn-primary", "escape" => false, "title" => __('Editar registro'))); ?>
				<?php echo $this->Form->postLink(__('<i class="icon-trash icon-white"></i>'), array('controller' => 'poas', 'action' => 'delete', $poa['id']), array("class" => "btn btn-small btn-primary", "escape" => false, "title" => __('Eliminar registro')), __('¿Está de acuerdo con eliminar este registro?')); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

</div>
