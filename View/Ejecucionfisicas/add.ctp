<?php $this->Html->addCrumb(__('Planificacion'), '/planificacion/'); ?>
<?php $this->Html->addCrumb(__('Metas'), '/planificacion/metas/' . $unidadmedida['Medioverificacione']['Indicadore']['Meta']['poa_id']); ?>
<?php $this->Html->addCrumb(__('Indicadores'), '/planificacion/indicadores/' . $unidadmedida['Medioverificacione']['Indicadore']['Meta']['poa_id'] . '/' . $unidadmedida['Medioverificacione']['Indicadore']['Meta']['id']); ?>
<?php $this->Html->addCrumb(__('Medioverificaciones'), '/planificacion/medioverificaciones/' . $unidadmedida['Medioverificacione']['Indicadore']['Meta']['poa_id'] . '/' . $unidadmedida['Medioverificacione']['Indicadore']['id']); ?>
<?php $this->Html->addCrumb(__('UnidadmSupuestos'), '/planificacion/unidadm_supuestos/' . $unidadmedida['Medioverificacione']['Indicadore']['Meta']['poa_id'] . '/' . $unidadmedida['Medioverificacione']['id']); ?>
<?php $this->Html->addCrumb(__('Ejecucionfisica'), '/planificacion/ejecucion_fisica/' . $unidadmedida['Medioverificacione']['Indicadore']['Meta']['poa_id'] . '/' . $unidadmedida['Unidadmedida']['id']); ?>
<?php $this->Html->addCrumb(__('Agregar'), '/ejecucionfisicas/add/' . $unidadmedida['Unidadmedida']['id']); ?>
<div class="row-fluid ejecucionfisicas form body-top">
	<div class="title span7">
		<h2><?php echo __('Agregar Ejecucionfisica'); ?></h2>
	</div>
	<div class="actions span5">
		<ul class="inline-menu pull-right">
			<li class="active"><?php echo $this->Html->link(__('Agregar'), array('action' => 'add')); ?></li>
			<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		</ul>
	</div>
</div>
<div class="row-fluid body-middle">
	<div class="span12">
		<?php echo $this->Form->create('Ejecucionfisica'); ?>
			<fieldset>
				<legend><?php echo __('Datos del Objetivo'); ?></legend>
				<div class="row-fluid">
					<div class="span12">
						<?php echo $unidadmedida['Medioverificacione']['Indicadore']['Meta']['Objetivo']['descripcion']; ?>
					</div>
				</div>
				<legend><?php echo __('Datos de la Meta/Producto'); ?></legend>
				<div class="row-fluid">
					<div class="span12">
						<strong><?php echo $unidadmedida['Medioverificacione']['Indicadore']['Meta']['cantidad']; ?></strong> <?php echo $unidadmedida['Medioverificacione']['Indicadore']['Meta']['descripcion']; ?><span class="hidden" id="meta_cantidad"><?php echo $unidadmedida['Medioverificacione']['Indicadore']['Meta']['cantidad']; ?></span>
					</div>
				</div>
				<legend><?php echo __('Datos del Indicador'); ?></legend>
				<div class="row-fluid">
					<div class="span12">
						<?php echo $unidadmedida['Medioverificacione']['Indicadore']['resultado']; ?> = <?php echo $unidadmedida['Medioverificacione']['Indicadore']['numerador']; ?> / <?php echo $unidadmedida['Medioverificacione']['Indicadore']['denominador']; ?> * 100
					</div>
				</div>
				<legend><?php echo __('Datos del Medio de Verificacion'); ?></legend>
				<div class="row-fluid">
					<div class="span12">
						<?php echo $unidadmedida['Medioverificacione']['descripcion']; ?>
					</div>
				</div>
				<legend><?php echo __('Datos de la Unidad de Medida'); ?></legend>
				<div class="row-fluid">
					<div class="span12">
						<?php echo $unidadmedida['Unidadmedida']['denominacion']; ?>
					</div>
				</div>
				<legend><?php echo __('Datos de la Ejecucionfisica'); ?></legend>
				<span id="cantidad_feedback" class="green"><?php echo __('Cantidad de Metas por Distribuir')?>: <strong><?php echo $unidadmedida['Medioverificacione']['Indicadore']['Meta']['cantidad']; ?></strong></span>
				<div class="row-fluid">
					<?php foreach($trimestres as $trimestre): ?>
						<div class="span3">
							<label><strong><?php echo $trimestre; ?></strong></label>
						</div>
					<?php endforeach; ?>
				</div>
				<div class="row-fluid">
					<?php $i = 0; ?>
					<?php foreach($trimestres as $trimestre): ?>
						<div class="span3 control-group">
							<?php echo $this->Form->input("$i.unidadmedida_id", array('type' => 'hidden', 'value' => $unidadmedida['Unidadmedida']['id'])); ?>
							<?php echo $this->Form->input("$i.trimestre_id", array('type' => 'hidden', 'value' => $i + 1)); ?>
							<?php if ($i == 0): ?>
								<?php echo $this->Form->input("$i.cantidad_planificada", array('class' => 'span12', 'label' => __('Cantidad Planificada'), 'autofocus' => true, 'class' => 'cantidades', 'value' => 0)); ?>
							<?php else: ?>
								<?php echo $this->Form->input("$i.cantidad_planificada", array('class' => 'span12', 'label' => __('Cantidad Planificada'), 'class' => 'cantidades', 'value' => 0)); ?>
							<?php endif; ?>
							<span class="help-block"><?php echo __("helpbox ejecucionfisicas add cantidad_planificada"); ?></span>
						</div>
						<?php $i++; ?>
					<?php endforeach; ?>
				</div>
			</fieldset>
			<div class="form-actions">
				<button type="submit" class="btn btn-primary"><?php echo __("Guardar"); ?></button>
				<button type="button" class="btn btn-small cancel"><?php echo __("Cancelar"); ?></button>
			</div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>
<?php
	echo $this->Html->script(array('efvalidate'));
?>