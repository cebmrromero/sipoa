<?php $this->Html->addCrumb(__('Poacabeceras'), '/poacabeceras/index'); ?>
<?php $this->Html->addCrumb(__('Visualizar'), '/poacabeceras/view/' . $poacabecera['Poacabecera']['id']); ?>
<div class="row-fluid poacabeceras view body-top">
	<div class="title span7">
		<h2><?php echo __('Visualizar Poacabecera'); ?></h2>
	</div>
	<div class="actions span5">
		<ul class="inline-menu pull-right">
			<li class="active"><?php echo $this->Html->link(__('Visualizar'), array('action' => 'view', $poacabecera['Poacabecera']['id'])); ?></li>
			<li><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $poacabecera['Poacabecera']['id'])); ?></li>
			<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
			<li><?php echo $this->Html->link(__('Agregar'), array('action' => 'add')); ?></li>
		</ul>
	</div>
</div>

<div class="row-fluid body-middle">
	<div class="span12">
		<dl>
			<dt><?php echo __('Denominacion'); ?></dt>
			<dd>
			<?php echo h($poacabecera['Poacabecera']['denominacion']); ?>
			&nbsp;
		</dd>
			<dt><?php echo __('Descripcion'); ?></dt>
			<dd>
			<?php echo h($poacabecera['Poacabecera']['descripcion']); ?>
			&nbsp;
		</dd>
			<dt><?php echo __('Parent Poacabecera'); ?></dt>
			<dd>
			<?php echo $this->Html->link($poacabecera['ParentPoacabecera']['descripcion'], array('controller' => 'poacabeceras', 'action' => 'view', $poacabecera['ParentPoacabecera']['id'])); ?>
			&nbsp;
		</dd>
			<dt><?php echo __('Lft'); ?></dt>
			<dd>
			<?php echo h($poacabecera['Poacabecera']['lft']); ?>
			&nbsp;
		</dd>
			<dt><?php echo __('Rght'); ?></dt>
			<dd>
			<?php echo h($poacabecera['Poacabecera']['rght']); ?>
			&nbsp;
		</dd>
			<dt><?php echo __('Seleccionable'); ?></dt>
			<dd>
			<?php echo h($poacabecera['Poacabecera']['seleccionable']); ?>
			&nbsp;
		</dd>
			<dt><?php echo __('Activo'); ?></dt>
			<dd>
			<?php echo h($poacabecera['Poacabecera']['activo']); ?>
			&nbsp;
		</dd>
		</dl>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related %s', 'Poacabeceras'); ?></h3>
	<?php if (!empty($poacabecera['ChildPoacabecera'])): ?>
	<table cellpadding="0" cellspacing="0" class="table table-striped table-condensed">
	<tr>
		<th><?php echo __('Denominacion'); ?></th>
		<th><?php echo __('Descripcion'); ?></th>
		<th><?php echo __('Parent Id'); ?></th>
		<th><?php echo __('Lft'); ?></th>
		<th><?php echo __('Rght'); ?></th>
		<th><?php echo __('Seleccionable'); ?></th>
		<th><?php echo __('Activo'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($poacabecera['ChildPoacabecera'] as $childPoacabecera): ?>
		<tr>
			<td><?php echo $childPoacabecera['denominacion']; ?></td>
			<td><?php echo $childPoacabecera['descripcion']; ?></td>
			<td><?php echo $childPoacabecera['parent_id']; ?></td>
			<td><?php echo $childPoacabecera['lft']; ?></td>
			<td><?php echo $childPoacabecera['rght']; ?></td>
			<td><?php echo $childPoacabecera['seleccionable']; ?></td>
			<td><?php echo $childPoacabecera['activo']; ?></td>
			<td class="actions btn-group span1">
				<?php echo $this->Html->link(__('<i class="icon-zoom-in icon-white"></i>'), array('controller' => 'poacabeceras', 'action' => 'view', $childPoacabecera['id']), array("class" => "label label-important", "escape" => false, "title" => __('Visualizar registro'))); ?>
				<?php echo $this->Html->link(__('<i class="icon-edit icon-white"></i>'), array('controller' => 'poacabeceras', 'action' => 'edit', $childPoacabecera['id']), array("class" => "label label-important", "escape" => false, "title" => __('Editar registro'))); ?>
				<?php echo $this->Form->postLink(__('<i class="icon-trash icon-white"></i>'), array('controller' => 'poacabeceras', 'action' => 'delete', $childPoacabecera['id']), array("class" => "label label-important", "escape" => false, "title" => __('Eliminar registro')), __('¿Está de acuerdo con eliminar este registro?')); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

</div>
<div class="related">
	<h3><?php echo __('Related %s', 'Poas'); ?></h3>
	<?php if (!empty($poacabecera['Poa'])): ?>
	<table cellpadding="0" cellspacing="0" class="table table-striped table-condensed">
	<tr>
		<th><?php echo __('Dependencia Id'); ?></th>
		<th><?php echo __('Ano'); ?></th>
		<th><?php echo __('Es Reprogramado'); ?></th>
		<th><?php echo __('Poacabecera Id'); ?></th>
		<th><?php echo __('Organo Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($poacabecera['Poa'] as $poa): ?>
		<tr>
			<td><?php echo $poa['dependencia_id']; ?></td>
			<td><?php echo $poa['ano']; ?></td>
			<td><?php echo $poa['es_reprogramado']; ?></td>
			<td><?php echo $poa['poacabecera_id']; ?></td>
			<td><?php echo $poa['organo_id']; ?></td>
			<td class="actions btn-group span1">
				<?php echo $this->Html->link(__('<i class="icon-zoom-in icon-white"></i>'), array('controller' => 'poas', 'action' => 'view', $poa['id']), array("class" => "label label-important", "escape" => false, "title" => __('Visualizar registro'))); ?>
				<?php echo $this->Html->link(__('<i class="icon-edit icon-white"></i>'), array('controller' => 'poas', 'action' => 'edit', $poa['id']), array("class" => "label label-important", "escape" => false, "title" => __('Editar registro'))); ?>
				<?php echo $this->Form->postLink(__('<i class="icon-trash icon-white"></i>'), array('controller' => 'poas', 'action' => 'delete', $poa['id']), array("class" => "label label-important", "escape" => false, "title" => __('Eliminar registro')), __('¿Está de acuerdo con eliminar este registro?')); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

</div>
