<?php $this->Html->addCrumb(__('Poacabeceras'), '/poacabeceras/index'); ?>
	<?php $this->Html->addCrumb(__('Agregar'), '/poacabeceras/add'); ?>
<div class="row-fluid poacabeceras form body-top">
	<div class="title span7">
		<h2><?php echo __('Agregar Poacabecera'); ?></h2>
	</div>
	<div class="actions span5">
		<ul class="inline-menu pull-right">
			<li class="active"><?php echo $this->Html->link(__('Agregar'), array('action' => 'add')); ?></li>
			<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		</ul>
	</div>
</div>
<div class="row-fluid body-middle">
	<div class="span12">
		<?php echo $this->Form->create('Poacabecera'); ?>
			<fieldset>
				<legend><?php echo __('Datos del Poacabecera'); ?></legend>
				<div class="row-fluid">
					<div class="span6 control-group">
						<?php echo $this->Form->input('parent_id', array('class' => 'span12', 'label' => __('Parent Id'), 'empty' => 'Ninguna')); ?>
						<span class="help-block"><?php echo __("helpbox poacabeceras add parent_id"); ?></span>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span6 control-group">
						<?php echo $this->Form->input('denominacion', array('class' => 'span12', 'label' => __('Denominacion'))); ?>
						<span class="help-block"><?php echo __("helpbox poacabeceras add denominacion"); ?></span>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span6 control-group">
						<?php echo $this->Form->input('descripcion', array('rows' => 2, 'class' => 'span12', 'label' => __('Descripcion'))); ?>
						<span class="help-block"><?php echo __("helpbox poacabeceras add descripcion"); ?></span>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span3 control-group">
						<?php echo $this->Form->input('seleccionable', array('label' => __('Seleccionable'))); ?>
						<span class="help-block"><?php echo __("helpbox poacabeceras add seleccionable"); ?></span>
					</div>
					<div class="span3 control-group">
						<?php echo $this->Form->input('activo', array('label' => __('Activo'))); ?>
						<span class="help-block"><?php echo __("helpbox poacabeceras add activo"); ?></span>
					</div>
				</div>
			</fieldset>
			<div class="form-actions">
				<button type="submit" class="btn btn-primary"><?php echo __("Guardar"); ?></button>
				<button type="button" class="btn btn-small cancel"><?php echo __("Cancelar"); ?></button>
			</div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>
