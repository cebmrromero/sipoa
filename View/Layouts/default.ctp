<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

$cakeDescription = __d('cake_dev', 'Sistema de Información para el Plan Operativo Anual');
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo 'SIPOA: ' . $cakeDescription ?>:
		<?php echo $title_for_layout; ?>
	</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href='http://fonts.googleapis.com/css?family=Dosis:200,400,600' rel='stylesheet' type='text/css'>
	<?php
		echo $this->Html->meta('icon');
		
		echo $this->Html->css(array(
			'bootstrap.min',
			'bootstrap-responsive.min',
			'style4',
			'main',
		));

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
		
		$auth_user = $this->Session->read('Auth.User');
	?>
</head>
<body>
	<div id="header-container">
		<div id="header" class="container">
			<div class="row-fluid">
				<div id="logo" class="logo alpha span2">
					<h1><?php echo $this->Html->image('logo.png', array('alt' => $cakeDescription, 'url' => '/')); ?></h1>
				</div>
				<div id="switcher" class="span4">
					<?php if ($auth_user): ?>
						<select id="operation-switcher" name="cd-dropdown" class="cd-select">
							<option value="-1" selected="selected">Seleccione una Operación</option>
							<option value="planificacion"<?php echo ($this->Session->read('operacion') == 'planificacion') ? ' selected="selected"': ''; ?>><?php echo __("Planificación"); ?></option>
							<option value="evaluacion"<?php echo ($this->Session->read('operacion') == 'evaluacion') ? ' selected="selected"': ''; ?>><?php echo __("Evaluación"); ?></option>
							<option value="poas"<?php echo ($this->Session->read('operacion') == 'configuracion') ? ' selected="selected"': ''; ?>><?php echo __("Configurar POAs"); ?></option>
							<option value="reportes"<?php echo ($this->Session->read('operacion') == 'reportes') ? ' selected="selected"': ''; ?>><?php echo __("Reportes"); ?></option>
							<option value="administracion"<?php echo ($this->Session->read('operacion') == 'administracion') ? ' selected="selected"': ''; ?>><?php echo __("Administración"); ?></option>
						</select>
					<?php endif; ?>
				</div>
				<div id="user-menu" class="omega span6">
					<ul class="pull-right inline-menu">
						<li<?php echo ($this->request->params['controller'] == 'pages' && $this->request->params['pass'][0] == 'home') ? ' class="active"' : ''; ?>>
							<?php echo $this->Html->link(__("Inicio"), '/'); ?></a>
						</li>
						<li<?php echo ($this->request->params['controller'] == 'pages' && $this->request->params['pass'][0] == 'ayudas') ? ' class="active"' : ''; ?>>
							<?php echo $this->Html->link(__("Ayudas (?)"), array('controller' => 'pages', 'action' => 'display', 'ayudas')); ?></a>
						</li>
						<?php if ($auth_user): ?>
							<li<?php echo ($this->request->params['controller'] == 'users' && $this->request->params['action'] == 'view') ? ' class="active"' : ''; ?>>
								<?php echo $this->Html->link(sprintf("%s %s", $auth_user['first_name'], $auth_user['last_name']), array('controller' => 'users', 'action' => 'view', $auth_user['id'])); ?></a>
							</li>
							<li>
								<?php echo $this->Html->link(__("Salir"), array('controller' => 'users', 'action' => 'logout')); ?></a>
							</li>
						<?php else: ?>
							<li<?php echo ($this->request->params['controller'] == 'users' && $this->request->params['action'] == 'login') ? ' class="active"' : ''; ?>>
								<?php echo $this->Html->link(__("Ingresar"), array('controller' => 'users', 'action' => 'login')); ?></a>
							</li>
						<?php endif; ?>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div id="container" class="container">
		<div id="content">
			<div id="body" class="row-fluid">
				<div id="left-side" class="span10">
					<div id="flash">
						<?php echo $this->Session->flash(); ?>
						<?php echo $this->Session->flash('auth'); ?>
					</div>
					<div id="breadcrumb">
						<?php echo $this->Html->getCrumbs(' > ', __('Home')); ?>
					</div>
					<?php echo $this->fetch('content'); ?>
				</div>
				<div id="right-side" class="span2">
					<?php if (isset($right_side_element)): ?>
						<?php echo $this->element($right_side_element); ?>
					<?php else: ?>
						<?php echo $this->element('Help/principal'); ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<div id="footer">
			
		</div>
	</div>
	<?php
		echo $this->Html->script(array(
			'jquery.min',
			'bootstrap.min',
			'modernizr.custom.63321',
			'jquery.dropdown',
			'main',
		));
	?>
</body>
<?php echo $this->element('sql_dump'); ?>
</html>
