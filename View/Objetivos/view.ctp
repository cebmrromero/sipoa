<?php $this->Html->addCrumb(__('Objetivos'), '/objetivos/index'); ?>
<?php $this->Html->addCrumb(__('Visualizar'), '/objetivos/view/' . $objetivo['Objetivo']['id']); ?>
<div class="row-fluid objetivos view body-top">
	<div class="title span7">
		<h2><?php echo __('Visualizar Objetivo'); ?></h2>
	</div>
	<div class="actions span5">
		<ul class="inline-menu pull-right">
			<li class="active"><?php echo $this->Html->link(__('Visualizar'), array('action' => 'view', $objetivo['Objetivo']['id'])); ?></li>
			<li><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $objetivo['Objetivo']['id'])); ?></li>
			<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
			<li><?php echo $this->Html->link(__('Agregar'), array('action' => 'add')); ?></li>
		</ul>
	</div>
</div>

<div class="row-fluid body-middle">
	<div class="span12">
		<dl>
			<dt><?php echo __('Descripcion'); ?></dt>
			<dd>
				<?php echo h($objetivo['Objetivo']['descripcion']); ?>
				&nbsp;
			</dd>
		</dl>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related %s', 'Metas'); ?></h3>
	<?php if (!empty($objetivo['Meta'])): ?>
	<table cellpadding="0" cellspacing="0" class="table table-striped table-condensed">
	<tr>
		<th><?php echo __('Poa Id'); ?></th>
		<th><?php echo __('Objetivo Id'); ?></th>
		<th><?php echo __('Cantidad'); ?></th>
		<th><?php echo __('Descripcion'); ?></th>
		<th><?php echo __('Costo'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($objetivo['Meta'] as $meta): ?>
		<tr>
			<td><?php echo $meta['poa_id']; ?></td>
			<td><?php echo $meta['objetivo_id']; ?></td>
			<td><?php echo $meta['cantidad']; ?></td>
			<td><?php echo $meta['descripcion']; ?></td>
			<td><?php echo $meta['costo']; ?></td>
			<td class="actions btn-group span1">
				<?php echo $this->Html->link(__('<i class="icon-zoom-in icon-white"></i>'), array('controller' => 'metas', 'action' => 'view', $meta['id']), array("class" => "label label-important", "escape" => false, "title" => __('Visualizar registro'))); ?>
				<?php echo $this->Html->link(__('<i class="icon-edit icon-white"></i>'), array('controller' => 'metas', 'action' => 'edit', $meta['id']), array("class" => "label label-important", "escape" => false, "title" => __('Editar registro'))); ?>
				<?php echo $this->Form->postLink(__('<i class="icon-trash icon-white"></i>'), array('controller' => 'metas', 'action' => 'delete', $meta['id']), array("class" => "label label-important", "escape" => false, "title" => __('Eliminar registro')), __('¿Está de acuerdo con eliminar este registro?')); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

</div>
<div class="related">
	<h3><?php echo __('Related %s', 'Poas'); ?></h3>
	<?php if (!empty($objetivo['Poa'])): ?>
	<table cellpadding="0" cellspacing="0" class="table table-striped table-condensed">
	<tr>
		<th><?php echo __('Dependencia Id'); ?></th>
		<th><?php echo __('Ano'); ?></th>
		<th><?php echo __('Es Reprogramado'); ?></th>
		<th><?php echo __('Poacabecera Id'); ?></th>
		<th><?php echo __('Organo Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($objetivo['Poa'] as $poa): ?>
		<tr>
			<td><?php echo $poa['dependencia_id']; ?></td>
			<td><?php echo $poa['ano']; ?></td>
			<td><?php echo $poa['es_reprogramado']; ?></td>
			<td><?php echo $poa['poacabecera_id']; ?></td>
			<td><?php echo $poa['organo_id']; ?></td>
			<td class="actions btn-group span1">
				<?php echo $this->Html->link(__('<i class="icon-zoom-in icon-white"></i>'), array('controller' => 'poas', 'action' => 'view', $poa['id']), array("class" => "label label-important", "escape" => false, "title" => __('Visualizar registro'))); ?>
				<?php echo $this->Html->link(__('<i class="icon-edit icon-white"></i>'), array('controller' => 'poas', 'action' => 'edit', $poa['id']), array("class" => "label label-important", "escape" => false, "title" => __('Editar registro'))); ?>
				<?php echo $this->Form->postLink(__('<i class="icon-trash icon-white"></i>'), array('controller' => 'poas', 'action' => 'delete', $poa['id']), array("class" => "label label-important", "escape" => false, "title" => __('Eliminar registro')), __('¿Está de acuerdo con eliminar este registro?')); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

</div>
