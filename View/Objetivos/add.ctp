<?php $this->Html->addCrumb(__('Objetivos'), '/objetivos/index'); ?>
	<?php $this->Html->addCrumb(__('Agregar'), '/objetivos/add'); ?>
<div class="row-fluid objetivos form body-top">
	<div class="title span7">
		<h2><?php echo __('Agregar Objetivo'); ?></h2>
	</div>
	<div class="actions span5">
		<ul class="inline-menu pull-right">
			<li class="active"><?php echo $this->Html->link(__('Agregar'), array('action' => 'add')); ?></li>
			<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		</ul>
	</div>
</div>
<div class="row-fluid body-middle">
	<div class="span12">
		<?php echo $this->Form->create('Objetivo'); ?>
			<fieldset>
				<legend><?php echo __('Datos del Objetivo'); ?></legend>
				<div class="row-fluid">
					<div class="span12 control-group">
						<?php echo $this->Form->input('descripcion', array('class' => 'span12', 'rows' => 2, 'label' => __('Descripcion'))); ?>
						<?php echo $this->Form->input('Poa', array('class' => 'span12', 'label' => false, 'type' => 'hidden', 'value' => $poa_id)); ?>
						<span class="help-block"><?php echo __("helpbox objetivos add descripcion"); ?></span>
					</div>
				</div>
			</fieldset>
			<div class="form-actions">
				<button type="submit" class="btn btn-primary"><?php echo __("Guardar"); ?></button>
				<button type="button" class="btn btn-small cancel"><?php echo __("Cancelar"); ?></button>
			</div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>
