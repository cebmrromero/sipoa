<?php $this->Html->addCrumb(__('Objetivos'), '/objetivos/index'); ?>
<?php $this->Html->addCrumb(__('Editar'), '/objetivos/edit/' . $this->request->data['Objetivo']['id']); ?>
<div class="row-fluid objetivos form body-top">
	<div class="title span7">
		<h2><?php echo __('Editar Objetivo'); ?></h2>
	</div>
	<div class="actions span5">
		<ul class="inline-menu pull-right">
			<li class="active"><?php echo $this->Html->link(__('Editar'), array('action' => 'edit',  $this->request->data['Objetivo']['id'])); ?></li>
			<li><?php echo $this->Html->link(__('Agregar'), array('action' => 'add')); ?></li>
			<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		</ul>
	</div>
</div>
<div class="row-fluid body-middle">
	<div class="span12">
		<?php echo $this->Form->create('Objetivo'); ?>
			<?php echo $this->Form->input('id'); ?>
			<fieldset>
				<legend><?php echo __('Datos del Objetivo'); ?></legend>
				<div class="row-fluid">
					<div class="span6 control-group">
						<?php echo $this->Form->input('descripcion', array('class' => 'span12', 'label' => __('Descripcion'))); ?>
						<span class="help-block"><?php echo __("helpbox objetivos edit descripcion"); ?></span>
					</div>
					<div class="span6 control-group">
						<?php echo $this->Form->input('Poa', array('class' => 'span12', 'label' => __('Poa'))); ?>
						<span class="help-block"><?php echo __("helpbox objetivos edit Poa"); ?></span>
					</div>
				</div>
			</fieldset>
			<div class="form-actions">
				<button type="submit" class="btn btn-primary"><?php echo __("Guardar"); ?></button>
				<button type="button" class="btn btn-small cancel"><?php echo __("Cancelar"); ?></button>
			</div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>
