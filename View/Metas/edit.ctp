<?php $this->Html->addCrumb(__('Planificacion'), '/planificacion/'); ?>
<?php $this->Html->addCrumb(__('Metas'), '/planificacion/metas/' . $this->request->data['Meta']['poa_id']); ?>
<?php $this->Html->addCrumb(__('Editar'), '/metas/edit/' . '/' . $this->request->data['Objetivo']['id']); ?>
<div class="row-fluid metas form body-top">
	<div class="title span7">
		<h2><?php echo __('Editar Meta'); ?></h2>
	</div>
	<div class="actions span5">
		<ul class="inline-menu pull-right">
			<li class="active"><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $this->request->data['Objetivo']['id'])); ?></li>
		</ul>
	</div>
</div>
<div class="row-fluid body-middle">
	<div class="span12">
		<?php echo $this->Form->create('Meta'); ?>
			<?php echo $this->Form->input('id'); ?>
			<?php echo $this->Form->input('poa_id', array('type' => 'hidden', 'value' => $this->request->data['Meta']['poa_id'])); ?>
			<?php echo $this->Form->input('objetivo_id', array('type' => 'hidden', 'value' => $this->request->data['Objetivo']['id'])); ?>
			<fieldset>
				<legend><?php echo __('Datos del Objetivo'); ?></legend>
				<div class="row-fluid">
					<div class="span12">
						<?php echo $this->request->data['Objetivo']['descripcion']; ?>
					</div>
				</div>
				<legend><?php echo __('Datos de la Meta/Producto'); ?></legend>
				<div class="row-fluid">
					<div class="span1 control-group">
						<?php echo $this->Form->input('cantidad', array('class' => 'span12', 'label' => __('Cantidad'))); ?>
						<span class="help-block"><?php echo __("helpbox metas add cantidad"); ?></span>
					</div>
					<div class="span9 control-group">
						<?php echo $this->Form->input('descripcion', array('class' => 'span12', 'label' => __('Descripcion'), 'rows' => '2')); ?>
						<span class="help-block"><?php echo __("helpbox metas add descripcion"); ?></span>
					</div>
					<div class="span2 control-group">
						<?php echo $this->Form->input('costo', array('type' => 'text', 'class' => 'span12 text-right', 'label' => __('Costo'))); ?>
						<span class="help-block"><?php echo __("helpbox metas add costo"); ?></span>
					</div>
				</div>
			</fieldset>
			<div class="form-actions">
				<button type="submit" class="btn btn-primary"><?php echo __("Guardar"); ?></button>
				<button type="button" class="btn btn-small cancel"><?php echo __("Cancelar"); ?></button>
			</div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>
