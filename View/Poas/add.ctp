<?php $this->Html->addCrumb(__('Poas'), '/poas/index'); ?>
	<?php $this->Html->addCrumb(__('Agregar'), '/poas/add'); ?>
<div class="row-fluid poas form body-top">
	<div class="title span7">
		<h2><?php echo __('Agregar Poa'); ?></h2>
	</div>
	<div class="actions span5">
		<ul class="inline-menu pull-right">
			<li class="active"><?php echo $this->Html->link(__('Agregar'), array('action' => 'add')); ?></li>
			<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		</ul>
	</div>
</div>
<div class="row-fluid body-middle">
	<div class="span12">
		<?php echo $this->Form->create('Poa'); ?>
			<fieldset>
				<legend><?php echo __('Datos Institucionales'); ?></legend>
				<div class="row-fluid">
                    <div class="span12">
                        <dl>
                            <?php foreach($poacabeceras as $poacabecera): ?>
                                <dt><?php echo h($poacabecera['Poacabecera']['denominacion']); ?></dt>
                                <dd>
                                <?php echo h($poacabecera['Poacabecera']['descripcion']); ?>
								<?php echo $this->Form->input('poacabecera_id', array('type' => 'hidden', 'value' => $poacabecera['Poacabecera']['id'])); ?>
                                &nbsp;
                            </dd>
                            <?php endforeach; ?>
                            <dt><?php echo __('ORGANO / ENTE DESCENTRALIZADO'); ?></dt>
                            <dd>
                            <?php echo h($organos['Organo']['descripcion']); ?>
							<?php echo $this->Form->input('organo_id', array('type' => 'hidden', 'value' => $organos['Organo']['id'])); ?>
                            &nbsp;
                        </dl>
					</div>
				</div>
			</fieldset>
			<fieldset>
				<legend><?php echo __('Datos Basicos'); ?></legend>
				<div class="row-fluid">
					<div class="span8 control-group">
						<?php echo $this->Form->input('dependencia_id', array('class' => 'span12', 'label' => __('Dependencia Id'))); ?>
						<span class="help-block"><?php echo __("helpbox poas edit dependencia_id"); ?></span>
					</div>
					<div class="span1 control-group">
						<?php echo $this->Form->input('ano', array('class' => 'span12', 'type' => 'text', 'label' => __('Ano'), 'value' => date('Y'))); ?>
						<span class="help-block"><?php echo __("helpbox poas edit ano"); ?></span>
					</div>
					<div class="span3 control-group">
						<?php echo $this->Form->label('es_reprogramado', __('Es Reprogramado')); ?>
						<?php echo $this->Form->input('es_reprogramado', array('label' => __('Si'))); ?>
						<span class="help-block"><?php echo __("helpbox poas edit es_reprogramado"); ?></span>
					</div>
				</div>
			</fieldset>
			<div class="form-actions">
				<button type="submit" class="btn btn-primary"><?php echo __("Crear"); ?></button>
				<button type="button" class="btn btn-small cancel"><?php echo __("Cancelar"); ?></button>
			</div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>
