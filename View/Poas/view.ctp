<?php $this->Html->addCrumb(__('Poas'), '/poas/index'); ?>
<?php $this->Html->addCrumb(__('Visualizar'), '/poas/view/' . $poa['Poa']['id']); ?>
<div class="row-fluid poas view body-top">
	<div class="title span7">
		<h2><?php echo __('Visualizar Poa'); ?></h2>
	</div>
	<div class="actions span5">
		<ul class="inline-menu pull-right">
			<li class="active"><?php echo $this->Html->link(__('Visualizar'), array('action' => 'view', $poa['Poa']['id'])); ?></li>
			<li><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $poa['Poa']['id'])); ?></li>
			<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
			<li><?php echo $this->Html->link(__('Agregar'), array('action' => 'add')); ?></li>
		</ul>
	</div>
</div>

<div class="row-fluid body-middle">
	<div class="span12">
		<dl>
			<dt><?php echo __('Dependencia'); ?></dt>
			<dd>
			<?php echo $this->Html->link($poa['Dependencia']['descripcion'], array('controller' => 'dependencias', 'action' => 'view', $poa['Dependencia']['id'])); ?>
			&nbsp;
		</dd>
			<dt><?php echo __('Ano'); ?></dt>
			<dd>
			<?php echo h($poa['Poa']['ano']); ?>
			&nbsp;
		</dd>
			<dt><?php echo __('Es Reprogramado'); ?></dt>
			<dd>
			<?php echo h($poa['Poa']['es_reprogramado']); ?>
			&nbsp;
		</dd>
			<dt><?php echo __('Poacabecera'); ?></dt>
			<dd>
			<?php echo $this->Html->link($poa['Poacabecera']['descripcion'], array('controller' => 'poacabeceras', 'action' => 'view', $poa['Poacabecera']['id'])); ?>
			&nbsp;
		</dd>
			<dt><?php echo __('Organo'); ?></dt>
			<dd>
			<?php echo $this->Html->link($poa['Organo']['descripcion'], array('controller' => 'organos', 'action' => 'view', $poa['Organo']['id'])); ?>
			&nbsp;
		</dd>
		</dl>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related %s', 'Metas'); ?></h3>
	<?php if (!empty($poa['Meta'])): ?>
	<table cellpadding="0" cellspacing="0" class="table table-striped table-condensed">
	<tr>
		<th><?php echo __('Poa Id'); ?></th>
		<th><?php echo __('Objetivo Id'); ?></th>
		<th><?php echo __('Cantidad'); ?></th>
		<th><?php echo __('Descripcion'); ?></th>
		<th><?php echo __('Costo'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($poa['Meta'] as $meta): ?>
		<tr>
			<td><?php echo $meta['poa_id']; ?></td>
			<td><?php echo $meta['objetivo_id']; ?></td>
			<td><?php echo $meta['cantidad']; ?></td>
			<td><?php echo $meta['descripcion']; ?></td>
			<td><?php echo $meta['costo']; ?></td>
			<td class="actions btn-group span1">
				<?php echo $this->Html->link(__('<i class="icon-zoom-in icon-white"></i>'), array('controller' => 'metas', 'action' => 'view', $meta['id']), array("class" => "label label-important", "escape" => false, "title" => __('Visualizar registro'))); ?>
				<?php echo $this->Html->link(__('<i class="icon-edit icon-white"></i>'), array('controller' => 'metas', 'action' => 'edit', $meta['id']), array("class" => "label label-important", "escape" => false, "title" => __('Editar registro'))); ?>
				<?php echo $this->Form->postLink(__('<i class="icon-trash icon-white"></i>'), array('controller' => 'metas', 'action' => 'delete', $meta['id']), array("class" => "label label-important", "escape" => false, "title" => __('Eliminar registro')), __('¿Está de acuerdo con eliminar este registro?')); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

</div>
<div class="related">
	<h3><?php echo __('Related %s', 'Objetivos'); ?></h3>
	<?php if (!empty($poa['Objetivo'])): ?>
	<table cellpadding="0" cellspacing="0" class="table table-striped table-condensed">
	<tr>
		<th><?php echo __('Descripcion'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($poa['Objetivo'] as $objetivo): ?>
		<tr>
			<td><?php echo $objetivo['descripcion']; ?></td>
			<td class="actions btn-group span1">
				<?php echo $this->Html->link(__('<i class="icon-zoom-in icon-white"></i>'), array('controller' => 'objetivos', 'action' => 'view', $objetivo['id']), array("class" => "label label-important", "escape" => false, "title" => __('Visualizar registro'))); ?>
				<?php echo $this->Html->link(__('<i class="icon-edit icon-white"></i>'), array('controller' => 'objetivos', 'action' => 'edit', $objetivo['id']), array("class" => "label label-important", "escape" => false, "title" => __('Editar registro'))); ?>
				<?php echo $this->Form->postLink(__('<i class="icon-trash icon-white"></i>'), array('controller' => 'objetivos', 'action' => 'delete', $objetivo['id']), array("class" => "label label-important", "escape" => false, "title" => __('Eliminar registro')), __('¿Está de acuerdo con eliminar este registro?')); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

</div>
