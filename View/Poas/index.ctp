<?php $this->Html->addCrumb(__('Poas'), '/poas/index'); ?>
<div class="row-fluid body-top">
	<div class="title span7">
		<h2><?php echo __('Listado de Poas'); ?></h2>
	</div>
	<div class="actions span5">
		<ul class="inline-menu pull-right">
			<li><?php echo $this->Html->link(__('Agregar'), array('action' => 'add')); ?></li>
			<li class="active"><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		</ul>
	</div>
</div>
<div class="row-fluid body-middle">
	<div class="span12">
		<p><?php echo __("A continuacion se encuentan los registros correspondientes a %s:", "Poas"); ?></p>
		<fieldset>
			<?php foreach ($dependencias as $dependencia): ?>
				<legend><?php echo $dependencia['Dependencia']['descripcion']; ?></legend>
				<?php $poas = $dependencia['Poa']; ?>
				<table cellpadding="0" cellspacing="0" class="table table-striped table-condensed">
					<tbody>
						<?php if ($poas): ?>
							<?php foreach ($poas as $poa): ?>
								<tr<?php echo ($poa['status']) ? '' : ' class="highlight"'; ?>>
									<td><?php echo h($poa['ano']); ?>&nbsp;</td>
									<td><?php echo ($poa['es_reprogramado']) ? __('Reprogramacion') . ' #' . $poa['numero_reprogramacion'] : '-'; ?>&nbsp;</td>
									<td class="actions btn-group span1">
										<?php echo $this->Html->link(__('<i class="icon-tasks icon-white"></i>'), array('controller' => 'planificacion', 'action' => 'principal', $poa['id']), array("class" => "label label-important", "escape" => false, "title" => __('Planificacion'))); ?>
										<?php echo $this->Html->link(__('<i class="icon-eye-open icon-white"></i>'), array('controller' => 'evaluacion', 'action' => 'principal', $poa['id']), array("class" => "label label-important", "escape" => false, "title" => __('Evaluacion'))); ?>
										<?php echo $this->Html->link(__('<i class="icon-edit icon-white"></i>'), array('action' => 'edit', $poa['id']), array("class" => "label label-important", "escape" => false, "title" => __('Editar registro'))); ?>
										<?php echo $this->Form->postLink(__('<i class="icon-trash icon-white"></i>'), array('action' => 'delete', $poa['id']), array("class" => "label label-important", "escape" => false, "title" => __('Eliminar registro')), __('¿Está de acuerdo con eliminar este registro?')); ?>
									</td>
								</tr>
							<?php endforeach; ?>
						<?php else: ?>
							<tr>
								<td colspan="5"><?php echo __('No hay poas registrados para la dependencia'); ?></td>
							</tr>
						<?php endif; ?>
					</tbody>
				</table>
			<?php endforeach; ?>
		</fieldset>
	</div>
</div>