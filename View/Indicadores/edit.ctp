<?php $this->Html->addCrumb(__('Planificacion'), '/planificacion/'); ?>
<?php $this->Html->addCrumb(__('Metas'), '/planificacion/metas/' . $this->request->data['Meta']['poa_id']); ?>
<?php $this->Html->addCrumb(__('Indicadores'), '/planificacion/indicadores/' . $this->request->data['Meta']['poa_id'] . '/' . $this->request->data['Meta']['id']); ?>
<?php $this->Html->addCrumb(__('Editar'), '/indicadores/edit/' . $this->request->data['Indicadore']['id']); ?>
<div class="row-fluid indicadores form body-top">
	<div class="title span7">
		<h2><?php echo __('Editar Indicadore'); ?></h2>
	</div>
	<div class="actions span5">
		<ul class="inline-menu pull-right">
			<li class="active"><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $this->request->data['Indicadore']['id'])); ?></li>
		</ul>
	</div>
</div>
<div class="row-fluid body-middle">
	<div class="span12">
		<?php echo $this->Form->create('Indicadore'); ?>
			<?php echo $this->Form->input('id'); ?>
			<?php echo $this->Form->input('meta_id', array('type' => 'hidden')); ?>
			<fieldset>
				<legend><?php echo __('Datos del Objetivo'); ?></legend>
				<div class="row-fluid">
					<div class="span12">
						<?php echo $this->request->data['Meta']['Objetivo']['descripcion']; ?>
					</div>
				</div>
				<legend><?php echo __('Datos de la Meta/Producto'); ?></legend>
				<div class="row-fluid">
					<div class="span12">
						<strong><?php echo $this->request->data['Meta']['cantidad']; ?></strong> <?php echo $this->request->data['Meta']['descripcion']; ?>
					</div>
				</div>
				<legend><?php echo __('Datos del Indicadore'); ?></legend>
				<div class="row-fluid">
					<div class="span3 control-group">
						<?php echo $this->Form->input('resultado', array('class' => 'span12', 'label' => __('Resultado'), 'rows' => 2, 'autofocus' => true)); ?>
						<span class="help-block"><?php echo __("helpbox indicadores add resultado"); ?></span>
					</div>
					<div class="span1 text-center">
						<label>&nbsp;</label>
						=
					</div>
					<div class="span3 control-group">
						<?php echo $this->Form->input('numerador', array('class' => 'span12', 'label' => __('Numerador'), 'rows' => 2)); ?>
						<span class="help-block"><?php echo __("helpbox indicadores add numerador"); ?></span>
					</div>
					<div class="span1 text-center">
						<label>&nbsp;</label>
						/
					</div>
					<div class="span3 control-group">
						<?php echo $this->Form->input('denominador', array('class' => 'span12', 'label' => __('Denominador'), 'rows' => 2)); ?>
						<span class="help-block"><?php echo __("helpbox indicadores add denominador"); ?></span>
					</div>
					<div class="span1 text-center">
						<label>&nbsp;</label>
						x&nbsp;&nbsp;&nbsp;&nbsp;100
					</div>
				</div>
			</fieldset>
			<div class="form-actions">
				<button type="submit" class="btn btn-primary"><?php echo __("Guardar"); ?></button>
				<button type="button" class="btn btn-small cancel"><?php echo __("Cancelar"); ?></button>
			</div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>
