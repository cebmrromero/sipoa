<?php $this->Html->addCrumb(__('Trimestres'), '/trimestres/index'); ?>
<?php $this->Html->addCrumb(__('Visualizar'), '/trimestres/view/' . $trimestre['Trimestre']['id']); ?>
<div class="row-fluid trimestres view body-top">
	<div class="title span7">
		<h2><?php echo __('Visualizar Trimestre'); ?></h2>
	</div>
	<div class="actions span5">
		<ul class="inline-menu pull-right">
			<li class="active"><?php echo $this->Html->link(__('Visualizar'), array('action' => 'view', $trimestre['Trimestre']['id'])); ?></li>
			<li><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $trimestre['Trimestre']['id'])); ?></li>
			<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
			<li><?php echo $this->Html->link(__('Agregar'), array('action' => 'add')); ?></li>
		</ul>
	</div>
</div>

<div class="row-fluid body-middle">
	<div class="span12">
		<dl>
			<dt><?php echo __('Denominacion'); ?></dt>
				<dd>
				<?php echo h($trimestre['Trimestre']['denominacion']); ?>
				&nbsp;
			</dd>
		</dl>
	</div>
</div>