<?php $this->Html->addCrumb(__('Trimestres'), '/trimestres/index'); ?>
<div class="row-fluid trimestres index body-top">
	<div class="title span7">
		<h2><?php echo __('Listado de Trimestres'); ?></h2>
	</div>
	<div class="actions span5">
		<ul class="inline-menu pull-right">
			<li><?php echo $this->Html->link(__('Agregar'), array('action' => 'add')); ?></li>
			<li class="active"><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		</ul>
	</div>
</div>
<div class="row-fluid body-middle">
	<div class="span12">
		<p><?php echo __("A continuacion se encuentan los registros correspondientes a %s:", "Trimestres"); ?></p>
		<table cellpadding="0" cellspacing="0" class="table table-striped table-condensed">
			<thead>
				<tr>
					<th><?php echo $this->Paginator->sort('denominacion'); ?></th>
					<th class="actions"><?php echo __('Actions'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($trimestres as $trimestre): ?>
					<tr>
						<td><?php echo h($trimestre['Trimestre']['denominacion']); ?>&nbsp;</td>
						<td class="actions btn-group span1">
							<?php echo $this->Html->link(__('<i class="icon-zoom-in icon-white"></i>'), array('action' => 'view', $trimestre['Trimestre']['id']), array("class" => "btn btn-small btn-primary", "escape" => false, "title" => __('Visualizar registro'))); ?>
							<?php echo $this->Html->link(__('<i class="icon-edit icon-white"></i>'), array('action' => 'edit', $trimestre['Trimestre']['id']), array("class" => "btn btn-small btn-primary", "escape" => false, "title" => __('Editar registro'))); ?>
							<?php echo $this->Form->postLink(__('<i class="icon-trash icon-white"></i>'), array('action' => 'delete', $trimestre['Trimestre']['id']), array("class" => "btn btn-small btn-primary", "escape" => false, "title" => __('Eliminar registro')), __('¿Está de acuerdo con eliminar este registro?')); ?>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
<div class="row-fluid body-bottom">
	<div class="span12 text-center">
		<p><?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de un total de {:count}, iniciando en el registro {:start}, y finalizando en {:end}'))); ?></p>
		<div class="paging">
			<?php
			echo $this->Paginator->prev('< ' . __('anterior'), array(), null, array('class' => 'prev disabled'));
			echo $this->Paginator->numbers(array('separator' => '|'));
			echo $this->Paginator->next(__('siguiente') . ' >', array(), null, array('class' => 'next disabled'));
			?>
		</div>
	</div>
</div>

