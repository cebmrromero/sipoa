<?php $this->Html->addCrumb(__('Administración'), '/administracion/index'); ?>
<div class="row-fluid reportes index body-top">
	<div class="title span7">
		<h2><?php echo __('Administración'); ?></h2>
	</div>
	<div class="actions span5">
		<ul class="inline-menu pull-right">
			<li class="active"><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		</ul>
	</div>
</div>
<div class="row-fluid body-middle">
	<div class="span12">
		<p><?php echo __("A continuacion se encuentan todos los modulos a administrar"); ?></p>
		<table cellpadding="0" cellspacing="0" class="table table-striped table-condensed">
			<thead>
				<tr>
					<th><?php echo __('Nombre del Módulo'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php if ($modulos): ?>
					<?php foreach ($modulos as $modulo): ?>
						<tr>
							<td>
								<?php echo $this->Html->link($modulo['nombre'], array('controller' => $modulo['src'])); ?>
							</td>
						</tr>
					<?php endforeach; ?>
				<?php endif; ?>
			</tbody>
		</table>
	</div>
</div>