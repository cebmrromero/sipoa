<?php $this->Html->addCrumb(__('Users'), '/users/index'); ?>
<div class="row-fluid users index body-top">
	<div class="title span7">
		<h2><?php echo __('Listado de Users'); ?></h2>
	</div>
	<div class="actions span5">
		<ul class="inline-menu pull-right">
			<li><?php echo $this->Html->link(__('Agregar'), array('action' => 'add')); ?></li>
			<li class="active"><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		</ul>
	</div>
</div>
<div class="row-fluid body-middle">
	<div class="span12">
		<p><?php echo __("A continuacion se encuentan los registros correspondientes a %s:", "Users"); ?></p>
		<table cellpadding="0" cellspacing="0" class="table table-striped table-condensed">
			<thead>
				<tr>
					<th><?php echo $this->Paginator->sort('first_name'); ?></th>
					<th><?php echo $this->Paginator->sort('last_name'); ?></th>
					<th><?php echo $this->Paginator->sort('username'); ?></th>
					<th><?php echo $this->Paginator->sort('group_id'); ?></th>
					<th class="actions"><?php echo __('Actions'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($users as $user): ?>
					<tr>
						<td><?php echo h($user['User']['first_name']); ?>&nbsp;</td>
						<td><?php echo h($user['User']['last_name']); ?>&nbsp;</td>
						<td><?php echo h($user['User']['username']); ?>&nbsp;</td>
						<td>
							<?php echo $this->Html->link($user['Group']['name'], array('controller' => 'groups', 'action' => 'view', $user['Group']['id'])); ?>
						</td>
						<td class="actions btn-group span1">
							<?php echo $this->Html->link(__('<i class="icon-zoom-in icon-white"></i>'), array('action' => 'view', $user['User']['id']), array("class" => "btn btn-small btn-primary", "escape" => false, "title" => __('Visualizar registro'))); ?>
							<?php echo $this->Html->link(__('<i class="icon-edit icon-white"></i>'), array('action' => 'edit', $user['User']['id']), array("class" => "btn btn-small btn-primary", "escape" => false, "title" => __('Editar registro'))); ?>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
<div class="row-fluid body-bottom">
	<div class="span12 text-center">
		<p><?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de un total de {:count}, iniciando en el registro {:start}, y finalizando en {:end}'))); ?></p>
		<div class="paging">
			<?php
			echo $this->Paginator->prev('< ' . __('anterior'), array(), null, array('class' => 'prev disabled'));
			echo $this->Paginator->numbers(array('separator' => '|'));
			echo $this->Paginator->next(__('siguiente') . ' >', array(), null, array('class' => 'next disabled'));
			?>
		</div>
	</div>
</div>

