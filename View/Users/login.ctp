	<?php $this->Html->addCrumb(__('Ingreso'), '/users/login'); ?>
<div class="row-fluid body-middle login">
	<div class="span4 offset4">
		<?php echo $this->Form->create('User', array('action' => 'login', 'autocomplete' => 'off')); ?>
			<fieldset>
				<legend><?php echo __('Datos para el Ingreso al Sistema'); ?></legend>
                &nbsp;
				<div class="row-fluid">
					<div class="span12 control-group">
						<?php echo $this->Form->input('username', array('class' => 'span12', 'label' => false, 'autofocus' => 'true', 'placeholder' => __('Nombre de Usuario'))); ?>
					</div>
                </div>
				<div class="row-fluid">
					<div class="span12 control-group">
						<?php echo $this->Form->input('password', array('class' => 'span12', 'label' => false, 'placeholder' => __('Contraseña'))); ?>
					</div>
				</div>
			</fieldset>
			<div class="form-actions">
				<button type="submit" class="btn btn-primary"><?php echo __("Ingresar"); ?></button>
			</div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>
