<?php $this->Html->addCrumb(__('Users'), '/users/index'); ?>
	<?php $this->Html->addCrumb(__('Editar'), '/users/edit/' . $this->request->data['User']['id']); ?>
<div class="row-fluid users form body-top">
	<div class="title span7">
		<h2><?php echo __('Editar User'); ?></h2>
	</div>
	<div class="actions span5">
		<ul class="inline-menu pull-right">
			
			<li><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $this->request->data['User']['id'])); ?></li>
			<li class="active"><?php echo $this->Html->link(__('Contraseña'), array('action' => 'password', $this->request->data['User']['id'])); ?></li>
			<li><?php echo $this->Html->link(__('Agregar'), array('action' => 'add')); ?></li>
			<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		</ul>
	</div>
</div>
<div class="row-fluid body-middle">
	<div class="span12">
		<?php echo $this->Form->create('User', array('autocomplete' => 'off')); ?>
			<?php echo $this->Form->input('id'); ?>
			<fieldset>
				
				<legend><?php echo __('Cambiar Contraseña'); ?></legend>
				<div class="row-fluid">
					<div class="span4 control-group">
						<?php echo $this->Form->input('password_old', array('class' => 'span12', 'label' => __('Password Old'))); ?>
						<span class="help-block"><?php echo __("helpbox users add password old"); ?></span>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span4 control-group">
						<?php echo $this->Form->input('password', array('class' => 'span12', 'label' => __('Password'), 'value' => '')); ?>
						<span class="help-block"><?php echo __("helpbox users add password"); ?></span>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span4 control-group">
						<?php echo $this->Form->input('password_confirm', array('class' => 'span12', 'label' => __('Password Confirm'))); ?>
						<span class="help-block"><?php echo __("helpbox users add password confirm"); ?></span>
					</div>
				</div>
			</fieldset>
			<div class="form-actions">
				<button type="submit" class="btn btn-primary"><?php echo __("Guardar"); ?></button>
				<button type="button" class="btn btn-small cancel"><?php echo __("Cancelar"); ?></button>
			</div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>
