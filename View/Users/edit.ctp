<?php $this->Html->addCrumb(__('Users'), '/users/index'); ?>
	<?php $this->Html->addCrumb(__('Editar'), '/users/edit/' . $this->request->data['User']['id']); ?>
<div class="row-fluid users form body-top">
	<div class="title span7">
		<h2><?php echo __('Editar User'); ?></h2>
	</div>
	<div class="actions span5">
		<ul class="inline-menu pull-right">
			<li class="active"><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $this->request->data['User']['id'])); ?></li>
			<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		</ul>
	</div>
</div>
<div class="row-fluid body-middle">
	<div class="span12">
		<?php echo $this->Form->create('User', array('autocomplete' => 'off')); ?>
			<?php echo $this->Form->input('id'); ?>
			<fieldset>
				<legend><?php echo __('Nombre de Usuario'); ?></legend>
				<div class="row-fluid">
					<div class="span4 control-group">
						<?php echo $this->Form->input('username', array('class' => 'span12', 'readonly' => 'readonly', 'label' => __('Username'))); ?>
						<span class="help-block"><?php echo __("helpbox users add username"); ?></span>
					</div>
				</div>
				<legend><?php echo __('Datos del User'); ?></legend>
				<div class="row-fluid">
					<div class="span4 control-group">
						<?php echo $this->Form->input('first_name', array('class' => 'span12', 'readonly' => 'readonly', 'label' => __('Nombres'))); ?>
						<span class="help-block"><?php echo __("helpbox users add nombres"); ?></span>
					</div>
					<div class="span4 control-group">
						<?php echo $this->Form->input('last_name', array('class' => 'span12', 'readonly' => 'readonly', 'label' => __('Apellidos'))); ?>
						<span class="help-block"><?php echo __("helpbox users add apellidos"); ?></span>
					</div>
				</div>
				<legend><?php echo __('Grupo al que pertenece'); ?></legend>
				<div class="row-fluid">
					<div class="span3 control-group">
						<?php echo $this->Form->input('group_id', array('class' => 'span12', 'label' => __('Group Id'))); ?>
						<span class="help-block"><?php echo __("helpbox users add group_id"); ?></span>
					</div>
					<div class="span3 control-group">
						<?php echo $this->Form->input('cargo', array('class' => 'span12', 'label' => __('Cargo'))); ?>
						<span class="help-block"><?php echo __("helpbox users add cargo"); ?></span>
					</div>
					<?php if ($auth_user['group_id'] == 1): ?>
						<div class="span3 control-group">
							<?php echo $this->Form->input('Sistema', array('class' => 'span12', 'label' => __('Sistemas'))); ?>
							<span class="help-block"><?php echo __("helpbox users add Sistema"); ?></span>
						</div>
						<div class="span3 control-group">
							<?php echo $this->Form->input('Dependencia', array('class' => 'span12', 'label' => __('Dependencias'))); ?>
							<span class="help-block"><?php echo __("helpbox users add Dependencia"); ?></span>
						</div>
					<?php endif; ?>
				</div>
			</fieldset>
			<div class="form-actions">
				<button type="submit" class="btn btn-primary"><?php echo __("Guardar"); ?></button>
				<button type="button" class="btn btn-small cancel"><?php echo __("Cancelar"); ?></button>
			</div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>
