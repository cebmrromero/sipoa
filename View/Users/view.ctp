<?php $this->Html->addCrumb(__('Users'), '/users/index'); ?>
<?php $this->Html->addCrumb(__('Visualizar'), '/users/view/' . $user['User']['id']); ?>
<div class="row-fluid users view body-top">
	<div class="title span7">
		<h2><?php echo __('Visualizar User'); ?></h2>
	</div>
	<div class="actions span5">
		<ul class="inline-menu pull-right">
			<li class="active"><?php echo $this->Html->link(__('Visualizar'), array('action' => 'view', $user['User']['id'])); ?></li>
			<li><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $user['User']['id'])); ?></li>
			<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		</ul>
	</div>
</div>

<div class="row-fluid body-middle">
	<div class="span12">
		<dl>
			<dt><?php echo __('Nombres'); ?></dt>
			<dd>
				<?php echo h($user['User']['first_name']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Apellidos'); ?></dt>
			<dd>
				<?php echo h($user['User']['last_name']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Username'); ?></dt>
			<dd>
				<?php echo h($user['User']['username']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Group'); ?></dt>
				<dd>
				<?php echo $this->Html->link($user['Group']['name'], array('controller' => 'groups', 'action' => 'view', $user['Group']['id'])); ?>
				&nbsp;
			</dd>
		</dl>
	</div>
</div>
