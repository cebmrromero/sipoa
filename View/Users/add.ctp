<?php $this->Html->addCrumb(__('Users'), '/users/index'); ?>
	<?php $this->Html->addCrumb(__('Agregar'), '/users/add'); ?>
<div class="row-fluid users form body-top">
	<div class="title span7">
		<h2><?php echo __('Agregar User'); ?></h2>
	</div>
	<div class="actions span5">
		<ul class="inline-menu pull-right">
			<li class="active"><?php echo $this->Html->link(__('Agregar'), array('action' => 'add')); ?></li>
			<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		</ul>
	</div>
</div>
<div class="row-fluid body-middle">
	<div class="span12">
		<?php echo $this->Form->create('User', array('autocomplete' => 'off')); ?>
			<fieldset>
				<legend><?php echo __('Nombre de Usuario, Contraseña y Confirmación'); ?></legend>
				<div class="row-fluid">
					<div class="span4 control-group">
						<?php echo $this->Form->input('username', array('class' => 'span12', 'label' => __('Username'))); ?>
						<span class="help-block"><?php echo __("helpbox users add username"); ?></span>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span4 control-group">
						<?php echo $this->Form->input('password', array('class' => 'span12', 'label' => __('Password'))); ?>
						<span class="help-block"><?php echo __("helpbox users add password"); ?></span>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span4 control-group">
						<?php echo $this->Form->input('password_confirm', array('class' => 'span12', 'label' => __('Password Confirm'))); ?>
						<span class="help-block"><?php echo __("helpbox users add password confirm"); ?></span>
					</div>
				</div>
				
				<legend><?php echo __('Datos del User'); ?></legend>
				<div class="row-fluid">
					<div class="span4 control-group">
						<?php echo $this->Form->input('first_name', array('class' => 'span12', 'label' => __('Nombres'))); ?>
						<span class="help-block"><?php echo __("helpbox users add nombres"); ?></span>
					</div>
					<div class="span4 control-group">
						<?php echo $this->Form->input('last_name', array('class' => 'span12', 'label' => __('Apellidos'))); ?>
						<span class="help-block"><?php echo __("helpbox users add apellidos"); ?></span>
					</div>
				</div>
				<legend><?php echo __('Grupo al que pertenece'); ?></legend>
				<div class="row-fluid">
					<div class="span4 control-group">
						<?php echo $this->Form->input('group_id', array('class' => 'span12', 'label' => __('Group Id'))); ?>
						<span class="help-block"><?php echo __("helpbox users add group_id"); ?></span>
					</div>
				</div>
			</fieldset>
			<div class="form-actions">
				<button type="submit" class="btn btn-primary"><?php echo __("Guardar"); ?></button>
				<button type="button" class="btn btn-small cancel"><?php echo __("Cancelar"); ?></button>
			</div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>
