<?php $this->Html->addCrumb(__('Dependencias'), '/dependencias/index'); ?>
<?php $this->Html->addCrumb(__('Visualizar'), '/dependencias/view/' . $dependencia['Dependencia']['id']); ?>
<div class="row-fluid dependencias view body-top">
	<div class="title span7">
		<h2><?php echo __('Visualizar Dependencia'); ?></h2>
	</div>
	<div class="actions span5">
		<ul class="inline-menu pull-right">
			<li class="active"><?php echo $this->Html->link(__('Visualizar'), array('action' => 'view', $dependencia['Dependencia']['id'])); ?></li>
			<li><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $dependencia['Dependencia']['id'])); ?></li>
			<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
			<li><?php echo $this->Html->link(__('Agregar'), array('action' => 'add')); ?></li>
		</ul>
	</div>
</div>

<div class="row-fluid body-middle">
	<div class="span12">
		<dl>
			<dt><?php echo __('Denominacion'); ?></dt>
				<dd>
				<?php echo h($dependencia['Dependencia']['descripcion)']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Dependenciacategoria'); ?></dt>
			<dd>
				<?php echo $this->Html->link($dependencia['Dependenciacategoria']['denominacion'], array('controller' => 'dependenciacategorias', 'action' => 'view', $dependencia['Dependenciacategoria']['id'])); ?>
				&nbsp;
			</dd>
		</dl>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related %s', 'Poas'); ?></h3>
	<?php if (!empty($dependencia['Poa'])): ?>
		<table cellpadding="0" cellspacing="0" class="table table-striped table-condensed">
		<tr>
			<th><?php echo __('Ano'); ?></th>
			<th><?php echo __('Es Reprogramado'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
		</tr>
		<?php
			$i = 0;
			foreach ($dependencia['Poa'] as $poa): ?>
			<tr>
				<td><?php echo $poa['ano']; ?></td>
				<td><?php echo ($poa['es_reprogramado']) ? __('Si') : __('No'); ?></td>
				<td class="actions btn-group span1">
					<?php echo $this->Html->link(__('<i class="icon-zoom-in icon-white"></i>'), array('controller' => 'poas', 'action' => 'view', $poa['id']), array("class" => "btn btn-small btn-primary", "escape" => false, "title" => __('Visualizar registro'))); ?>
					<?php echo $this->Html->link(__('<i class="icon-edit icon-white"></i>'), array('controller' => 'poas', 'action' => 'edit', $poa['id']), array("class" => "btn btn-small btn-primary", "escape" => false, "title" => __('Editar registro'))); ?>
					<?php echo $this->Form->postLink(__('<i class="icon-trash icon-white"></i>'), array('controller' => 'poas', 'action' => 'delete', $poa['id']), array("class" => "btn btn-small btn-primary", "escape" => false, "title" => __('Eliminar registro')), __('¿Está de acuerdo con eliminar este registro?')); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</table>
	<?php endif; ?>
</div>
