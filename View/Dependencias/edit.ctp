<?php $this->Html->addCrumb(__('Dependencias'), '/dependencias/index'); ?>
	<?php $this->Html->addCrumb(__('Editar'), '/dependencias/edit/' . $this->request->data['Dependencia']['id']); ?>
<div class="row-fluid dependencias form body-top">
	<div class="title span7">
		<h2><?php echo __('Editar Dependencia'); ?></h2>
	</div>
	<div class="actions span5">
		<ul class="inline-menu pull-right">
			<li class="active"><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $this->request->data['Dependencia']['id'])); ?></li>
			<li><?php echo $this->Html->link(__('Agregar'), array('action' => 'add')); ?></li>
			<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		</ul>
	</div>
</div>
<div class="row-fluid body-middle">
	<div class="span12">
		<?php echo $this->Form->create('Dependencia'); ?>
			<?php echo $this->Form->input('id'); ?>
			<fieldset>
				<legend><?php echo __('Datos del Dependencia'); ?></legend>
				<div class="row-fluid">
					<div class="span8 control-group">
						<?php echo $this->Form->input('descripcion', array('class' => 'span12', 'label' => __('Descripcion'))); ?>
						<span class="help-block"><?php echo __("helpbox dependencias add descripcion"); ?></span>
					</div>
					<div class="span2 control-group">
						<?php echo $this->Form->input('iniciales', array('class' => 'span12', 'label' => __('Iniciales'))); ?>
						<span class="help-block"><?php echo __("helpbox dependencias add iniciales"); ?></span>
					</div>
					<div class="span2 control-group">
						<?php echo $this->Form->input('dependenciacategoria_id', array('class' => 'span12', 'label' => __('Dependenciacategoria Id'))); ?>
						<span class="help-block"><?php echo __("helpbox dependencias add dependenciacategoria_id"); ?></span>
					</div>
				</div>
			</fieldset>
			<div class="form-actions">
				<button type="submit" class="btn btn-primary"><?php echo __("Guardar"); ?></button>
				<button type="button" class="btn btn-small cancel"><?php echo __("Cancelar"); ?></button>
			</div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>
