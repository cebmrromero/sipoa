<?php $this->Html->addCrumb(__('Reportes'), '/reportes/index'); ?>
<div class="row-fluid reportes index body-top">
	<div class="title span7">
		<h2><?php echo __('Listado de Reportes'); ?></h2>
	</div>
	<div class="actions span5">
		<ul class="inline-menu pull-right">
			<li class="active"><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		</ul>
	</div>
</div>
<div class="row-fluid body-middle">
	<div class="span12">
		<p><?php echo __("A continuacion se encuentan todos los reportes generados"); ?></p>
		<table cellpadding="0" cellspacing="0" class="table table-striped table-condensed">
			<thead>
				<tr>
					<th><?php echo __('Nombre del Reporte'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php if ($reportes): ?>
					<?php foreach ($reportes as $reporte): ?>
						<tr>
							<td>
								<?php echo $this->Html->link($reporte['nombre'], array('action' => $reporte['src'])); ?>
							</td>
						</tr>
					<?php endforeach; ?>
				<?php else: ?>
					<tr>
						<td colspan="5"><?php echo __('No hay reportes registrados'); ?></td>
					</tr>
				<?php endif; ?>
			</tbody>
		</table>
	</div>
</div>