<?php $this->Html->addCrumb(__('Reportes'), '/reportes/index'); ?>
<div class="row-fluid poas index body-top">
	<div class="title span7">
		<h2><?php echo __('Reporte de Poas'); ?></h2>
	</div>
</div>
<div class="row-fluid search">
    <div id="content-inner" class="etiquetas form is-post span12">
        <?php echo $this->Form->create('Poa', array( 'url' => array_merge(array('action' => '../reportes/reporte1'), $this->params['pass']) )); ?>
            <fieldset>
				<legend><?php echo __('Filtros'); ?></legend>
                <div class="row-fluid">
                    <?php echo $this->Form->input('dependenciacategoria_id', array('label' => false, 'empty' => __('Todos los Tipos'), 'required' => false, 'class' => 'span12', 'div' => array('class' => 'span2') )); ?>
                    <?php echo $this->Form->input('dependencia_id', array('label' => false, 'empty' => __('Todas las Dependencias'), 'required' => false, 'class' => 'span12', 'div' => array('class' => 'span3') )); ?>
                    <?php echo $this->Form->input('ano', array('type' => 'select', 'label' => false, 'empty' => __('Todos los Años'), 'required' => false, 'class' => 'span12', 'div' => array('class' => 'span1') )); ?>
                    <?php echo $this->Form->input('es_reprogramado', array('options' => array('1' => __('Reprogramado'), '0' => __('No Reprogramado')), 'empty' => __('Todos'), 'label' => false, 'required' => false, 'class' => 'span12', 'div' => array('class' => 'span1') )); ?>
                    <?php //echo $this->Form->input('organo_id', array('label' => false, 'required' => false, 'class' => 'span12', 'div' => array('class' => 'span2') )); ?>
                    <?php echo $this->Form->input('trimestredesde_id', array('label' => false, 'empty' => __('Todos'), 'required' => false, 'class' => 'span12', 'div' => array('class' => 'span2'), 'value' => (isset($this->request->params['named']['trimestredesde_id'])) ? $this->request->params['named']['trimestredesde_id'] : '')); ?>
                    <?php echo $this->Form->input('trimestrehasta_id', array('label' => false, 'empty' => __('Todos'), 'required' => false, 'class' => 'span12', 'div' => array('class' => 'span2'), 'value' => (isset($this->request->params['named']['trimestrehasta_id'])) ? $this->request->params['named']['trimestrehasta_id'] : '')); ?>
                    
                    <?php echo $this->Form->input('>', array('type' => 'submit', 'label' => false, 'class' => 'btn btn-primary span12', 'div' => array('class' => 'span1 submit'))); ?>
                </div>
            </fieldset>
        <?php echo $this->Form->end(); ?>
    </div>
</div>
<div class="row-fluid body-middle">
	<div class="span12">
		<?php if (isset($poas)): ?>
			<p>
				Exportar a:
				<?php // echo $this->Html->link('PDF', str_replace($this->action, $this->action . '_pdf', $this->Html->url( null, true ))); ?> <span title="Proximamente">PDF</span>
				|
				<?php echo $this->Html->link('Excel', str_replace($this->action, $this->action . '_excel', $this->Html->url( null, true ))); ?></p>
			<?php //echo $this->Paginator->sort('dependencia_id'); ?>
			<?php //echo $this->Paginator->sort('ano'); ?>
			<?php //echo $this->Paginator->sort('es_reprogramado'); ?>
			
			<?php if (!empty($poas)): ?>
				<?php foreach ($poas as $poa): ?>
					<div class="row-fluid report">
						<div class="span12">
							<div class="row-fluid">
								<div class="span6 alpha">
									<h4><?php echo $this->Html->link($poa['Dependencia']['descripcion'], array('controller' => 'dependencias', 'action' => 'view', $poa['Dependencia']['id'])); ?><br /><?php echo __('POA'); ?> <?php echo h($poa['Poa']['ano']); ?><?php echo ($poa['Poa']['es_reprogramado']) ? __(' (Reprogramado)') : '';  ?></h4>
								</div>
								<div class="span6 omega">
									<div class="row-fluid">
										<div class="span3">
											<label class="text-center"><?php echo __('Cantidad de Productos/Meta'); ?>:</label>
											<p class="text-center"><?php echo $poa['Nivelejecucion']['Ejecucionfisica']['meta_productos']; ?></p>
										</div>
										<div class="span3">
											<label class="text-center"><?php echo __('Productos/Metas Planificados'); ?>:</label>
											<p class="text-center"><?php echo $poa['Nivelejecucion']['Ejecucionfisica']['total_planificado']; ?></p>
										</div>
										<div class="span3">
											<label class="text-center"><?php echo __('Productos/Metas Ejecutados'); ?>:</label>
											<p class="text-center"><?php echo $poa['Nivelejecucion']['Ejecucionfisica']['total_ejecutado']; ?></p>
										</div>
										<div class="span3">
											<label class="text-center"><?php echo __('Porcentaje de Ejecución'); ?>:</label>
											<h3 class="text-center"><?php echo number_format($poa['Nivelejecucion']['Ejecucionfisica']['porcentaje_ejecucion'], 2, ',', '.'); ?>%</h3>
										</div>
									</div>
								</div>
							</div>
							<div class="row-fluid">
								<div class="span12">
									<table cellpadding="0" cellspacing="0" class="table table-striped table-condensed">
										<thead>
											<tr>
												<th><?php echo __('Objetivo'); ?></th>
												<th><?php echo __('Meta'); ?></th>
												<th><?php echo __('Unidadmedida'); ?></th>
												<th><?php echo __('Ejecución Trimestral'); ?></th>
											</tr>
										</thead>
										<tbody>
										<?php $current_obj = null; ?>
										<?php $current_meta = null; ?>
										<?php foreach ($poa['Ejecucionfisica'] as $ejecucionfisica): ?>
											<tr>
												<?php if ($current_obj != $ejecucionfisica['Objetivo']['id']): ?>
													<?php
													$rowspan = 0;
													foreach ($poa['Ejecucionfisica'] as $aux) {
														if ($aux['Objetivo']['id'] == $ejecucionfisica['Objetivo']['id']) {
															$rowspan++;
														}
													}
													$current_obj = $ejecucionfisica['Objetivo']['id'];
													?>
													<td class="span3" rowspan="<?php echo $rowspan; ?>"><?php echo $ejecucionfisica['Objetivo']['descripcion']; ?>&nbsp;</td>
												<?php endif; ?>
												
												<?php if ($current_meta != $ejecucionfisica['Meta']['id']): ?>
													<?php
													$rowspan2 = 0;
													foreach ($poa['Ejecucionfisica'] as $aux) {
														if ($aux['Meta']['id'] == $ejecucionfisica['Meta']['id']) {
															$rowspan2++;
														}
													}
													$current_meta = $ejecucionfisica['Meta']['id'];
													?>
													<td class="span3" rowspan="<?php echo $rowspan2; ?>"><strong><?php echo $ejecucionfisica['Meta']['cantidad']; ?></strong> <?php echo $ejecucionfisica['Meta']['descripcion']; ?>&nbsp;</td>
												<?php endif; ?>
												<td class="span2"><?php echo $ejecucionfisica['Unidadmedida']['denominacion']; ?>&nbsp;</td>
												<td class="span4">
													<?php $trimestre_range = range(($this->request->params['named']['trimestredesde_id']) ? $this->request->params['named']['trimestredesde_id'] : 1, ($this->request->params['named']['trimestrehasta_id']) ? $this->request->params['named']['trimestrehasta_id'] : 4); ?>
													<div class="row-fluid">
														<?php $span_len = ceil(12 / sizeof($trimestre_range)); ?>
														<?php foreach ($trimestre_range as $t): ?>
															<div class="text-center span<?php echo $span_len; ?>">
																<small><?php echo __('Trimestre');?> <?php echo $t; ?></small><br />
																<?php if ($ejecucionfisica['Ejecucionfisica' . $t]['cantidad_planificada']): ?>
																	<small><?php echo ($ejecucionfisica['Ejecucionevaluacione' . $t]['cantidad_ejecutada']) ? $ejecucionfisica['Ejecucionevaluacione' . $t]['cantidad_ejecutada'] : 0; ?> / <?php echo $ejecucionfisica['Ejecucionfisica' . $t]['cantidad_planificada']; ?></small><br />
																	<small><strong><?php echo ($ejecucionfisica['Ejecucionfisica' . $t]['cantidad_planificada'] > 0) ? number_format($ejecucionfisica['Ejecucionevaluacione' . $t]['cantidad_ejecutada']/$ejecucionfisica['Ejecucionfisica' . $t]['cantidad_planificada']*100,2, ',', '.'): '-'; ?>%</strong></small>
																<?php else: ?>
																	<small>-</small>
																<?php endif; ?>
															</div>
														<?php endforeach; ?>
													</div>
												</td>
											</tr>
										<?php endforeach; ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			<?php else: ?>
				<p><?php echo __('No hay poas registrados que cumplan los filtros indicados'); ?></p>
			<?php endif; ?>
			
			<div class="row-fluid body-bottom">
				<div class="span12 text-center">
					<p><?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de un total de {:count}, iniciando en el registro {:start}, y finalizando en {:end}'))); ?></p>
					<div class="paging">
						<?php
						echo $this->Paginator->prev('< ' . __('anterior'), array(), null, array('class' => 'prev disabled'));
						echo $this->Paginator->numbers(array('separator' => '|'));
						echo $this->Paginator->next(__('siguiente') . ' >', array(), null, array('class' => 'next disabled'));
						?>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</div>