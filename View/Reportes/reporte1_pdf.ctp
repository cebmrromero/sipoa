<?php
/**
 * HTML2PDF Librairy - example
 *
 * HTML => PDF convertor
 * distributed under the LGPL License
 *
 * @author      Laurent MINGUET <webmaster@html2pdf.fr>
 *
 * isset($_GET['vuehtml']) is not mandatory
 * it allow to display the result in the HTML format
 */

ob_start();
?>
<style type="text/css">
    table td, table th  {font-size: 85%; }
<!--
    table.page_header {width: 100%; border: none; background-color: #DDDDFF; border-bottom: solid 1mm #AAAADD; padding: 2mm }
    table.page_footer {width: 100%; border: none; background-color: #DDDDFF; border-top: solid 1mm #AAAADD; padding: 2mm}
    h1 {color: #000033}
    h2 {color: #000055}
    h3 {color: #000077}

    div.niveau
    {
        padding-left: 5mm;
    }
-->
table td, table th { padding: 1mm }
</style>
<page backtop="14mm" backbottom="14mm" backleft="10mm" backright="10mm" style="font-size: 12pt">
    <page_header>
        <table class="page_header">
            <tr>
                <td style="width: 100%; text-align: left">
                    Reporte de POA
                </td>
            </tr>
        </table>
    </page_header>
    <page_footer>
        <table class="page_footer">
            <tr>
                <td style="width: 100%; text-align: right">
                    Página [[page_cu]]/[[page_nb]]
                </td>
            </tr>
        </table>
    </page_footer>
    <bookmark title="Reporte De POAS" level="0" ></bookmark>
    <div class="niveau">
        <?php if (!empty($poas)): ?>
            <?php $i = 1; ?>
            <?php foreach ($poas as $poa): ?>
                <table style="width: 100%; vertical-align: middle" border="1" align="center" cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                            <th colspan="5"><?php echo $i++; ?>) <?php echo $poa['Dependencia']['descripcion']; ?> <?php echo __('POA'); ?> <?php echo h($poa['Poa']['ano']); ?><?php echo ($poa['Poa']['es_reprogramado']) ? __(' (Reprogramado)') : '';  ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td style="width: 20%; vertical-align: middle">
                                <strong><?php echo __('Cantidad de Productos/Meta'); ?>:</strong> <?php echo $poa['Nivelejecucion']['Ejecucionfisica']['meta_productos']; ?>
                            </td>
                            <td style="width: 20%; vertical-align: middle">
                                <strong><?php echo __('Productos/Metas Planificados'); ?>:</strong> <?php echo $poa['Nivelejecucion']['Ejecucionfisica']['total_planificado']; ?>
                            </td>
                            <td style="width: 20%; vertical-align: middle">
                                <strong><?php echo __('Productos/Metas Ejecutados'); ?>:</strong> <?php echo $poa['Nivelejecucion']['Ejecucionfisica']['total_ejecutado']; ?>
                            </td>
                            <td style="width: 20%; vertical-align: middle">
                                <strong><?php echo __('Porcentaje de Ejecución Eficaz'); ?>:</strong><strong style="color:red; font-weight: bold;"><?php echo number_format($poa['Nivelejecucion']['Ejecucionfisica']['porcentaje_ejecucion'], 2,',', '.'); ?>%</strong>
                            </td>
                            <td style="width: 20%; vertical-align: middle">
                                <strong><?php echo __('Porcentaje de Ejecución Eficiente'); ?>:</strong><strong style="color:red; font-weight: bold;"><?php echo number_format($poa['Nivelejecucion']['Ejecucionfisica']['porcentaje_ejecucion'], 2,',', '.'); ?>%</strong>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table style="width: 100%; vertical-align: middle" border="1" align="center" cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                            <th><?php echo __('Objetivo'); ?></th>
                            <th><?php echo __('Metas/Productos'); ?></th>
                            <th><?php echo __('Unidadmedida'); ?></th>
                            <th><?php echo __('Ejecucion Trimestral'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $current_obj = null; ?>
                    <?php foreach ($poa['Ejecucionfisica'] as $ejecucionfisica): ?>
                        <tr>
                            <?php if ($current_obj != $ejecucionfisica['Objetivo']['id']): ?>
                                <?php
                                $rowspan = 0;
                                foreach ($poa['Ejecucionfisica'] as $aux) {
                                    if ($aux['Objetivo']['id'] == $ejecucionfisica['Objetivo']['id']) {
                                        $rowspan++;
                                    }
                                }
                                $current_obj = $ejecucionfisica['Objetivo']['id'];
                                ?>
                                <td style="width: 20%; vertical-align: middle" rowspan="<?php echo $rowspan; ?>"><?php echo $ejecucionfisica['Objetivo']['descripcion']; ?></td>
                            <?php endif; ?>
                            <td style="width: 25%; vertical-align: middle"><strong><?php echo $ejecucionfisica['Meta']['cantidad']; ?></strong> <?php echo $ejecucionfisica['Meta']['descripcion']; ?></td>
                            <td style="width: 15%; vertical-align: middle"><?php echo $ejecucionfisica['Unidadmedida']['denominacion']; ?></td>
                            <td style="width: 30%; vertical-align: middle">
                                <?php $trimestre_range = range(($this->request->params['named']['trimestredesde_id']) ? $this->request->params['named']['trimestredesde_id'] : 1, ($this->request->params['named']['trimestrehasta_id']) ? $this->request->params['named']['trimestrehasta_id'] : 4); ?>
                                <?php $span_len = ceil(100 / sizeof($trimestre_range)); ?>
                                <table style="width: 100%" align="center" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <?php $ts = array(1 => 'I', 2 => 'II', 3 => 'III', 4 => 'IV') ?>
                                        <?php foreach ($trimestre_range as $t): ?>
                                            <td style="width: <?php echo $span_len; ?>%; text-align: center" >
                                                <small><?php echo $ts[$t]; ?></small><br />
                                                <small><?php echo ($ejecucionfisica['Ejecucionevaluacione' . $t]['cantidad_ejecutada']) ? $ejecucionfisica['Ejecucionevaluacione' . $t]['cantidad_ejecutada'] : 0; ?> / <?php echo $ejecucionfisica['Ejecucionfisica' . $t]['cantidad_planificada']; ?></small><br />
                                                <small><strong><?php echo ($ejecucionfisica['Ejecucionfisica' . $t]['cantidad_planificada'] > 0) ? number_format($ejecucionfisica['Ejecucionevaluacione' . $t]['cantidad_ejecutada']/$ejecucionfisica['Ejecucionfisica' . $t]['cantidad_planificada']*100,2,',', '.'): '-'; ?>%</strong></small>
                                            </td>
                                        <?php endforeach; ?>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <p>&nbsp;</p>
            <?php endforeach; ?>
        <?php else: ?>
            <p><?php echo __('No hay poas registrados que cumplan los filtros indicados'); ?></p>
        <?php endif; ?>
    </div>
</page>