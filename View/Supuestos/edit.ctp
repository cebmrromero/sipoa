<?php $this->Html->addCrumb(__('Planificacion'), '/planificacion/'); ?>
<?php $this->Html->addCrumb(__('Metas'), '/planificacion/metas/' . $this->request->data['Medioverificacione']['Indicadore']['Meta']['poa_id']); ?>
<?php $this->Html->addCrumb(__('Indicadores'), '/planificacion/indicadores/' . $this->request->data['Medioverificacione']['Indicadore']['Meta']['poa_id'] . '/' . $this->request->data['Medioverificacione']['Indicadore']['Meta']['id']); ?>
<?php $this->Html->addCrumb(__('Medioverificaciones'), '/planificacion/medioverificaciones/' . $this->request->data['Medioverificacione']['Indicadore']['Meta']['poa_id'] . '/' . $this->request->data['Medioverificacione']['Indicadore']['id']); ?>
<?php $this->Html->addCrumb(__('UnidadmSupuestos'), '/planificacion/unidadm_supuestos/' . $this->request->data['Medioverificacione']['Indicadore']['Meta']['poa_id'] . '/' . $this->request->data['Medioverificacione']['id']); ?>
<?php $this->Html->addCrumb(__('Editar'), '/supuestos/edit/' . $this->request->data['Supuesto']['id'] . '/' . $this->request->data['Medioverificacione']['Indicadore']['Meta']['poa_id']); ?>
<div class="row-fluid supuestos form body-top">
	<div class="title span7">
		<h2><?php echo __('Editar Supuesto'); ?></h2>
	</div>
	<div class="actions span5">
		<ul class="inline-menu pull-right">
			<li class="active"><?php echo $this->Html->link(__('Agregar'), array('action' => 'edit', $this->request->data['Supuesto']['id'], $this->request->data['Medioverificacione']['Indicadore']['Meta']['poa_id'])); ?></li>
		</ul>
	</div>
</div>
<div class="row-fluid body-middle">
	<div class="span12">
		<?php echo $this->Form->create('Supuesto'); ?>
			<?php echo $this->Form->input('id'); ?>
			<?php echo $this->Form->input('medioverificacione_id', array('type' => 'hidden')); ?>
			<fieldset>
				<legend><?php echo __('Datos del Objetivo'); ?></legend>
				<div class="row-fluid">
					<div class="span12">
						<?php echo $this->request->data['Medioverificacione']['Indicadore']['Meta']['Objetivo']['descripcion']; ?>
					</div>
				</div>
				<legend><?php echo __('Datos de la Meta/Producto'); ?></legend>
				<div class="row-fluid">
					<div class="span12">
						<strong><?php echo $this->request->data['Medioverificacione']['Indicadore']['Meta']['cantidad']; ?></strong> <?php echo $this->request->data['Medioverificacione']['Indicadore']['Meta']['descripcion']; ?>
					</div>
				</div>
				<legend><?php echo __('Datos del Indicador'); ?></legend>
				<div class="row-fluid">
					<div class="span12">
						<?php echo $this->request->data['Medioverificacione']['Indicadore']['resultado']; ?> = <?php echo $this->request->data['Medioverificacione']['Indicadore']['numerador']; ?> / <?php echo $this->request->data['Medioverificacione']['Indicadore']['denominador']; ?> * 100
					</div>
				</div>
				<legend><?php echo __('Datos del Medio de Verificacion'); ?></legend>
				<div class="row-fluid">
					<div class="span12">
						<?php echo $this->request->data['Medioverificacione']['descripcion']; ?>
					</div>
				</div>
				<legend><?php echo __('Datos del Supuesto'); ?></legend>
				<div class="row-fluid">
					<div class="span6 control-group">
						<?php echo $this->Form->input('descripcion', array('class' => 'span12', 'label' => __('Descripcion'), 'rows' => '2', 'autofocus' => true)); ?>
						<span class="help-block"><?php echo __("helpbox supuestos add descripcion"); ?></span>
					</div>
				</div>
			</fieldset>
			<div class="form-actions">
				<button type="submit" class="btn btn-primary"><?php echo __("Guardar"); ?></button>
				<button type="button" class="btn btn-small cancel"><?php echo __("Cancelar"); ?></button>
			</div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>
