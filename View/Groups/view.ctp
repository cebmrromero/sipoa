<?php $this->Html->addCrumb(__('Groups'), '/groups/index'); ?>
<?php $this->Html->addCrumb(__('Visualizar'), '/groups/view/' . $group['Group']['id']); ?>
<div class="row-fluid groups view body-top">
	<div class="title span7">
		<h2><?php echo __('Visualizar Group'); ?></h2>
	</div>
	<div class="actions span5">
		<ul class="inline-menu pull-right">
			<li class="active"><?php echo $this->Html->link(__('Visualizar'), array('action' => 'view', $group['Group']['id'])); ?></li>
			<li><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $group['Group']['id'])); ?></li>
			<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
			<li><?php echo $this->Html->link(__('Agregar'), array('action' => 'add')); ?></li>
		</ul>
	</div>
</div>

<div class="row-fluid body-middle">
	<div class="span12">
		<dl>
			<dt><?php echo __('Name'); ?></dt>
			<dd>
				<?php echo h($group['Group']['name']); ?>
				&nbsp;
			</dd>
		</dl>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related %s', 'Usuarios'); ?></h3>
	<?php if (!empty($group['User'])): ?>
	<table cellpadding="0" cellspacing="0" class="table table-striped table-condensed">
	<tr>
		<th><?php echo __('Nombres'); ?></th>
		<th><?php echo __('Apellidos'); ?></th>
		<th><?php echo __('Username'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($group['User'] as $user): ?>
		<tr>
			<td><?php echo $user['first_name']; ?></td>
			<td><?php echo $user['last_name']; ?></td>
			<td><?php echo $user['username']; ?></td>
			<td class="actions btn-group span1">
				<?php echo $this->Html->link(__('<i class="icon-zoom-in icon-white"></i>'), array('controller' => 'users', 'action' => 'view', $user['id']), array("class" => "btn btn-small btn-primary", "escape" => false, "title" => __('Visualizar registro'))); ?>
				<?php echo $this->Html->link(__('<i class="icon-edit icon-white"></i>'), array('controller' => 'users', 'action' => 'edit', $user['id']), array("class" => "btn btn-small btn-primary", "escape" => false, "title" => __('Editar registro'))); ?>
				<?php echo $this->Form->postLink(__('<i class="icon-trash icon-white"></i>'), array('controller' => 'users', 'action' => 'delete', $user['id']), array("class" => "btn btn-small btn-primary", "escape" => false, "title" => __('Eliminar registro')), __('¿Está de acuerdo con eliminar este registro?')); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

</div>
