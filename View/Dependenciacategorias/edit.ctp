<?php $this->Html->addCrumb(__('Dependenciacategorias'), '/dependenciacategorias/index'); ?>
	<?php $this->Html->addCrumb(__('Editar'), '/dependenciacategorias/edit/' . $this->request->data['Dependenciacategoria']['id']); ?>
<div class="row-fluid dependenciacategorias form body-top">
	<div class="title span7">
		<h2><?php echo __('Editar Dependenciacategoria'); ?></h2>
	</div>
	<div class="actions span5">
		<ul class="inline-menu pull-right">
			<li class="active"><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $this->request->data['Dependenciacategoria']['id'])); ?></li>
			<li><?php echo $this->Html->link(__('Agregar'), array('action' => 'add')); ?></li>
			<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		</ul>
	</div>
</div>
<div class="row-fluid body-middle">
	<div class="span12">
		<?php echo $this->Form->create('Dependenciacategoria'); ?>
			<?php echo $this->Form->input('id'); ?>
			<fieldset>
				<legend><?php echo __('Datos del Dependenciacategoria'); ?></legend>
				<div class="row-fluid">
					<div class="span6 control-group">
						<?php echo $this->Form->input('denominacion', array('class' => 'span12', 'label' => __('Denominacion'))); ?>
						<span class="help-block"><?php echo __("helpbox dependenciacategorias edit denominacion"); ?></span>
					</div>
				</div>
			</fieldset>
			<div class="form-actions">
				<button type="submit" class="btn btn-primary"><?php echo __("Guardar"); ?></button>
				<button type="button" class="btn btn-small cancel"><?php echo __("Cancelar"); ?></button>
			</div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>
