<?php $this->Html->addCrumb(__('Dependenciacategorias'), '/dependenciacategorias/index'); ?>
<?php $this->Html->addCrumb(__('Visualizar'), '/dependenciacategorias/view/' . $dependenciacategoria['Dependenciacategoria']['id']); ?>
<div class="row-fluid dependenciacategorias view body-top">
	<div class="title span7">
		<h2><?php echo __('Visualizar Dependenciacategoria'); ?></h2>
	</div>
	<div class="actions span5">
		<ul class="inline-menu pull-right">
			<li class="active"><?php echo $this->Html->link(__('Visualizar'), array('action' => 'view', $dependenciacategoria['Dependenciacategoria']['id'])); ?></li>
			<li><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $dependenciacategoria['Dependenciacategoria']['id'])); ?></li>
			<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
			<li><?php echo $this->Html->link(__('Agregar'), array('action' => 'add')); ?></li>
		</ul>
	</div>
</div>

<div class="row-fluid body-middle">
	<div class="span12">
		<dl>
			<dt><?php echo __('Denominacion'); ?></dt>
			<dd>
				<?php echo h($dependenciacategoria['Dependenciacategoria']['denominacion']); ?>
				&nbsp;
			</dd>
		</dl>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related %s', 'Dependencias'); ?></h3>
	<?php if (!empty($dependenciacategoria['Dependencia'])): ?>
	<table cellpadding="0" cellspacing="0" class="table table-striped table-condensed">
	<tr>
		<th><?php echo __('Denominacion'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($dependenciacategoria['Dependencia'] as $dependencia): ?>
		<tr>
			<td><?php echo $dependencia['descripcion']; ?></td>
			<td class="actions btn-group span1">
				<?php echo $this->Html->link(__('<i class="icon-zoom-in icon-white"></i>'), array('controller' => 'dependencias', 'action' => 'view', $dependencia['id']), array("class" => "btn btn-small btn-primary", "escape" => false, "title" => __('Visualizar registro'))); ?>
				<?php echo $this->Html->link(__('<i class="icon-edit icon-white"></i>'), array('controller' => 'dependencias', 'action' => 'edit', $dependencia['id']), array("class" => "btn btn-small btn-primary", "escape" => false, "title" => __('Editar registro'))); ?>
				<?php echo $this->Form->postLink(__('<i class="icon-trash icon-white"></i>'), array('controller' => 'dependencias', 'action' => 'delete', $dependencia['id']), array("class" => "btn btn-small btn-primary", "escape" => false, "title" => __('Eliminar registro')), __('¿Está de acuerdo con eliminar este registro?')); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

</div>
