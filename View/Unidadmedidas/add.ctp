<?php $this->Html->addCrumb(__('Planificacion'), '/planificacion/'); ?>
<?php $this->Html->addCrumb(__('Metas'), '/planificacion/metas/' . $medioverificacione['Indicadore']['Meta']['poa_id']); ?>
<?php $this->Html->addCrumb(__('Indicadores'), '/planificacion/indicadores/' . $medioverificacione['Indicadore']['Meta']['poa_id'] . '/' . $medioverificacione['Indicadore']['Meta']['id']); ?>
<?php $this->Html->addCrumb(__('Medioverificaciones'), '/planificacion/medioverificaciones/' . $medioverificacione['Indicadore']['Meta']['poa_id'] . '/' . $medioverificacione['Indicadore']['id']); ?>
<?php $this->Html->addCrumb(__('UnidadmSupuestos'), '/planificacion/unidadm_supuestos/' . $medioverificacione['Indicadore']['Meta']['poa_id'] . '/' . $medioverificacione['Medioverificacione']['id']); ?>
<?php $this->Html->addCrumb(__('Agregar'), '/unidadmedidas/add/' . $medioverificacione['Medioverificacione']['id']); ?>
<div class="row-fluid unidadmedidas form body-top">
	<div class="title span7">
		<h2><?php echo __('Agregar Unidadmedida'); ?></h2>
	</div>
	<div class="actions span5">
		<ul class="inline-menu pull-right">
			<li class="active"><?php echo $this->Html->link(__('Agregar'), array('action' => 'add', $medioverificacione['Medioverificacione']['id'])); ?></li>
			<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		</ul>
	</div>
</div>
<div class="row-fluid body-middle">
	<div class="span12">
		<?php echo $this->Form->create('Unidadmedida'); ?>
			<?php echo $this->Form->input('medioverificacione_id', array('type' => 'hidden', 'value' => $medioverificacione['Medioverificacione']['id'])); ?>
			<fieldset>
				<legend><?php echo __('Datos del Objetivo'); ?></legend>
				<div class="row-fluid">
					<div class="span12">
						<?php echo $medioverificacione['Indicadore']['Meta']['Objetivo']['descripcion']; ?>
					</div>
				</div>
				<legend><?php echo __('Datos de la Meta/Producto'); ?></legend>
				<div class="row-fluid">
					<div class="span12">
						<strong><?php echo $medioverificacione['Indicadore']['Meta']['cantidad']; ?></strong> <?php echo $medioverificacione['Indicadore']['Meta']['descripcion']; ?>
					</div>
				</div>
				<legend><?php echo __('Datos del Indicador'); ?></legend>
				<div class="row-fluid">
					<div class="span12">
						<?php echo $medioverificacione['Indicadore']['resultado']; ?> = <?php echo $medioverificacione['Indicadore']['numerador']; ?> / <?php echo $medioverificacione['Indicadore']['denominador']; ?> * 100
					</div>
				</div>
				<legend><?php echo __('Datos del Medio de Verificacion'); ?></legend>
				<div class="row-fluid">
					<div class="span12">
						<?php echo $medioverificacione['Medioverificacione']['descripcion']; ?>
					</div>
				</div>
				<legend><?php echo __('Datos del Unidadmedida'); ?></legend>
				<div class="row-fluid">
					<div class="span6 control-group">
						<?php echo $this->Form->input('denominacion', array('class' => 'span12', 'label' => __('Denominacion'), 'autofocus' => true)); ?>
						<span class="help-block"><?php echo __("helpbox unidadmedidas add denominacion"); ?></span>
					</div>
				</div>
			</fieldset>
			<div class="form-actions">
				<button type="submit" class="btn btn-primary"><?php echo __("Guardar"); ?></button>
				<button type="button" class="btn btn-small cancel"><?php echo __("Cancelar"); ?></button>
			</div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>
