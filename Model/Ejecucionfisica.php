<?php
App::uses('AppModel', 'Model');
/**
 * Ejecucionfisica Model
 *
 * @property Unidadmedida $Unidadmedida
 * @property Trimestre $Trimestre
 * @property Ejecucionevaluacione $Ejecucionevaluacione
 */
class Ejecucionfisica extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'unidadmedida_id';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'unidadmedida_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'trimestre_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'cantidad_planificada' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Unidadmedida' => array(
			'className' => 'Unidadmedida',
			'foreignKey' => 'unidadmedida_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Trimestre' => array(
			'className' => 'Trimestre',
			'foreignKey' => 'trimestre_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	
	public function getEjecucionFisicaByPoaId($poa_id) {
		$query = "
			SELECT
				Objetivo.*,
				Meta.*,
				Indicadore.*,
				Medioverificacione.*,
				Unidadmedida.*,
				Ejecucionfisica1.*,
				Ejecucionfisica2.*,
				Ejecucionfisica3.*,
				Ejecucionfisica4.*,
				Ejecucionevaluacione1.*,
				Ejecucionevaluacione2.*,
				Ejecucionevaluacione3.*,
				Ejecucionevaluacione4.*
				FROM 
				`unidadmedidas` as Unidadmedida
				LEFT JOIN ejecucionfisicas as Ejecucionfisica1 on Ejecucionfisica1.trimestre_id = 1 and Ejecucionfisica1.unidadmedida_id = Unidadmedida.id
				LEFT JOIN ejecucionfisicas as Ejecucionfisica2 on Ejecucionfisica2.trimestre_id = 2 and Ejecucionfisica1.unidadmedida_id = Ejecucionfisica2.unidadmedida_id
				LEFT JOIN ejecucionfisicas as Ejecucionfisica3 on Ejecucionfisica3.trimestre_id = 3 and Ejecucionfisica1.unidadmedida_id = Ejecucionfisica3.unidadmedida_id
				LEFT JOIN ejecucionfisicas as Ejecucionfisica4 on Ejecucionfisica4.trimestre_id = 4 and Ejecucionfisica1.unidadmedida_id = Ejecucionfisica4.unidadmedida_id
				
				LEFT JOIN medioverificaciones as Medioverificacione on Medioverificacione.id = Unidadmedida.medioverificacione_id
				LEFT JOIN indicadores as Indicadore on Indicadore.id = Medioverificacione.indicadore_id
				LEFT JOIN metas as Meta on Meta.id = Indicadore.meta_id
				LEFT JOIN objetivos as Objetivo on Objetivo.id = Meta.objetivo_id
				LEFT JOIN poas as Poa on Poa.id = Meta.poa_id
				LEFT JOIN ejecucionevaluaciones as Ejecucionevaluacione1 on Ejecucionevaluacione1.unidadmedida_id = Unidadmedida.id and Ejecucionevaluacione1.trimestre_id = 1 AND Ejecucionfisica1.trimestre_id = Ejecucionevaluacione1.trimestre_pertenece_id
				LEFT JOIN ejecucionevaluaciones as Ejecucionevaluacione2 on Ejecucionevaluacione2.unidadmedida_id = Unidadmedida.id and Ejecucionevaluacione2.trimestre_id = 2 AND Ejecucionfisica2.trimestre_id = Ejecucionevaluacione2.trimestre_pertenece_id
				LEFT JOIN ejecucionevaluaciones as Ejecucionevaluacione3 on Ejecucionevaluacione3.unidadmedida_id = Unidadmedida.id and Ejecucionevaluacione3.trimestre_id = 3 AND Ejecucionfisica3.trimestre_id = Ejecucionevaluacione3.trimestre_pertenece_id
				LEFT JOIN ejecucionevaluaciones as Ejecucionevaluacione4 on Ejecucionevaluacione4.unidadmedida_id = Unidadmedida.id and Ejecucionevaluacione4.trimestre_id = 4 AND Ejecucionfisica4.trimestre_id = Ejecucionevaluacione4.trimestre_pertenece_id
				WHERE
				Poa.id = %s
				ORDER BY Objetivo.id, Meta.id, Indicadore.id, Medioverificacione.id, Unidadmedida.id
		";
		$query = sprintf($query, $poa_id);
		return $this->query($query);
	}
    
    public function getNivelEjecucionByTrimestre($poa_id, $params) {
        $trimestre_from = 1;
        if (isset($params['trimestredesde_id']) && !empty($params['trimestredesde_id'])) {
            $trimestre_from = $params['trimestredesde_id'];
        }
        $trimestre_to = 4;
        if (isset($params['trimestrehasta_id']) && !empty($params['trimestrehasta_id'])) {
            $trimestre_to = $params['trimestrehasta_id'];
        }
        $this->virtualFields['total_planificado'] = 0;
        $this->virtualFields['total_ejecutado'] = 0;
        $this->virtualFields['porcentaje_ejecucion'] = 0;
        $this->virtualFields['meta_productos'] = 0;
        $query = "
            SELECT
                (SELECT COUNT(m.id) as count FROM metas as m WHERE m.poa_id = %s) as Ejecucionfisica__meta_productos,
                Ejecucionfisica.trimestre_id,
                SUM(Ejecucionfisica.cantidad_planificada) as Ejecucionfisica__total_planificado,
                Ejecucionevaluacione.trimestre_id,
                Ejecucionevaluacione.trimestre_pertenece_id,
                Ejecucionevaluacione.cantidad_ejecutada,
                SUM(Ejecucionevaluacione.cantidad_ejecutada) as Ejecucionfisica__total_ejecutado,
                (SUM(Ejecucionevaluacione.cantidad_ejecutada)/SUM(Ejecucionfisica.cantidad_planificada) * 100) as Ejecucionfisica__porcentaje_ejecucion
            FROM 
                `ejecucionfisicas` as Ejecucionfisica
                LEFT JOIN unidadmedidas as Unidadmedida on Unidadmedida.id = Ejecucionfisica.unidadmedida_id
                LEFT JOIN medioverificaciones as Medioverificacione on Medioverificacione.id = Unidadmedida.medioverificacione_id
                LEFT JOIN indicadores as Indicadore on Indicadore.id = Medioverificacione.indicadore_id
                LEFT JOIN metas as Meta on Meta.id = Indicadore.meta_id
                LEFT JOIN objetivos as Objetivo on Objetivo.id = Meta.objetivo_id
                LEFT JOIN poas as Poa on Poa.id = Meta.poa_id
                LEFT JOIN ejecucionevaluaciones as Ejecucionevaluacione on Ejecucionevaluacione.unidadmedida_id = Unidadmedida.id AND Ejecucionfisica.trimestre_id = Ejecucionevaluacione.trimestre_id AND Ejecucionfisica.trimestre_id = Ejecucionevaluacione.trimestre_pertenece_id
            WHERE
                Poa.id = %s AND Ejecucionfisica.trimestre_id BETWEEN %s AND %s
            ORDER BY
                Ejecucionfisica.trimestre_id";
        $query = sprintf($query, $poa_id, $poa_id, $trimestre_from, $trimestre_to);
		$query = $this->query($query);
        return $query[0];
    }
}
