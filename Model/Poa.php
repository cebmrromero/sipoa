<?php
App::uses('AppModel', 'Model');
/**
 * Poa Model
 *
 * @property Dependencia $Dependencia
 * @property Poacabecera $Poacabecera
 * @property Organo $Organo
 * @property Meta $Meta
 * @property Objetivo $Objetivo
 */
class Poa extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';

/**
 * actsAs
 *
 * @var array
 */
    public $actsAs = array('Search.Searchable');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'dependencia_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'ano' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'es_reprogramado' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				'allowEmpty' => true,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'poacabecera_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'organo_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Dependencia' => array(
			'className' => 'Dependencia',
			'foreignKey' => 'dependencia_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Poacabecera' => array(
			'className' => 'Poacabecera',
			'foreignKey' => 'poacabecera_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Organo' => array(
			'className' => 'Organo',
			'foreignKey' => 'organo_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Meta' => array(
			'className' => 'Meta',
			'foreignKey' => 'poa_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);


/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Objetivo' => array(
			'className' => 'Objetivo',
			'joinTable' => 'objetivos_poas',
			'foreignKey' => 'poa_id',
			'associationForeignKey' => 'objetivo_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
	);
    
    public $filterArgs = array(
        'dependencia_id' => array('type' => 'value'),
        'dependenciacategoria_id' => array('type' => 'subquery', 'method' => 'findByDependenciacategoria', 'field' => 'Dependencia.dependenciacategoria_id'),
        'ano' => array('type' => 'value'),
        'es_reprogramado' => array('type' => 'value'),
        'organo_id' => array('type' => 'value'),
    );
    
    public function findByDependenciacategoria($data = array()) {
		$this->Dependencia->Behaviors->attach('Containable', array('autoFields' => false));
        $this->Dependencia->Behaviors->attach('Search.Searchable');
        $this->Dependencia->recursive = -1;
        $query = $this->Dependencia->getQuery('all', array(
            'conditions' => array('Dependencia.dependenciacategoria_id' => $data['dependenciacategoria_id']),
            'fields' => array('dependenciacategoria_id'),
            'contain' => array('Dependenciacategoria')
        ));
        return $query;
    }

    public function hasMetaAnEjecucionfisica($id, $meta_id) {
    	echo $sql = "SELECT ef.* FROM ejecucionfisicas as ef
			LEFT JOIN unidadmedidas as um on um.id = ef.unidadmedida_id
			LEFT JOIN medioverificaciones as mv on mv.id = um.medioverificacione_id
			LEFT JOIN indicadores as i on i.id = mv.indicadore_id
			LEFT JOIN metas as m on m.id = i.meta_id
			WHERE m.poa_id = $id and m.id = $meta_id";
		return $this->query($sql);
    }
}
