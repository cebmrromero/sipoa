<?php
App::uses('AppModel', 'Model');
/**
 * Ejecucionevaluacione Model
 *
 * @property Ejecucionfisica $Ejecucionfisica
 * @property Ejecucionindicadore $Ejecucionindicadore
 * @property Metapendiente $Metapendiente
 * @property Productometa $Productometa
 */
class Ejecucionevaluacione extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'unidadmedida_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'trimestre_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'trimestre_pertenece_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'cantidad_ejecutada' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Unidadmedida' => array(
			'className' => 'Unidadmedida',
			'foreignKey' => 'unidadmedida_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Trimestre' => array(
			'className' => 'Trimestre',
			'foreignKey' => 'trimestre_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Trimestrepertenece' => array(
			'className' => 'Trimestre',
			'foreignKey' => 'trimestre_pertenece_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Ejecucionindicadore' => array(
			'className' => 'Ejecucionindicadore',
			'foreignKey' => 'ejecucionevaluacione_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Productometa' => array(
			'className' => 'Productometa',
			'foreignKey' => 'ejecucionevaluacione_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
