<?php
App::uses('AppModel', 'Model');
/**
 * Metapendiente Model
 *
 * @property Ejecucionevaluacione $Ejecucionevaluacione
 * @property Trimestre $Trimestre
 */
class Metapendiente extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'explicacion';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'unidadmedida_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'trimestre_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'trimestre_ejecucion_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'cantidad_pendiente' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'explicacion' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Unidadmedida' => array(
			'className' => 'Unidadmedida',
			'foreignKey' => 'unidadmedida_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Trimestre' => array(
			'className' => 'Trimestre',
			'foreignKey' => 'trimestre_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Trimestreejecucion' => array(
			'className' => 'Trimestre',
			'foreignKey' => 'trimestre_ejecucion_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Trimestrereporte' => array(
			'className' => 'Trimestre',
			'foreignKey' => 'trimestre_reporte_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
