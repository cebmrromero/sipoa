<?php
App::uses('AppModel', 'Model');
App::uses('CakeNumber', 'Utility');
/**
 * Meta Model
 *
 * @property Poa $Poa
 * @property Objetivo $Objetivo
 * @property Indicadore $Indicadore
 */
class Meta extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'descripcion';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'poa_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => true,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'objetivo_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => true,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'cantidad' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => true,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'descripcion' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => true,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'costo' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => true,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Poa' => array(
			'className' => 'Poa',
			'foreignKey' => 'poa_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Objetivo' => array(
			'className' => 'Objetivo',
			'foreignKey' => 'objetivo_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Indicadore' => array(
			'className' => 'Indicadore',
			'foreignKey' => 'meta_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
    
    public function afterFind($results, $primary = false) {
        foreach ($results as $key => $val) {
            if (isset($val['Meta']['costo'])) {
                $results[$key]['Meta']['costo'] = $this->currencyFormatAfterFind($val['Meta']['costo']);
            }
        }
        return $results;
    }
    
    public function currencyFormatAfterFind($number) {
        $currency = 'Bs.';
        return CakeNumber::currency($number, $currency, array('wholePosition' => 'after', 'thousands' => '.', 'decimals' => ','));
    }
    
    public function beforeSave($options = array()) {
        if (!empty($this->data['Meta']['costo'])) {
            $this->data['Meta']['costo'] = $this->currencyFormatBeforeSave($this->data['Meta']['costo']);
        }
        return true;
    }
    
    public function currencyFormatBeforeSave($number) {
        $number = str_replace('.', '', $number);
        $number = str_replace(',', '.', $number);
        return $number;
    }

}
