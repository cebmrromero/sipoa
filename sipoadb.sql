-- MySQL dump 10.13  Distrib 5.5.35, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: sipoadb
-- ------------------------------------------------------
-- Server version	5.5.35-0ubuntu0.12.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `acos`
--

DROP TABLE IF EXISTS `acos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT '',
  `foreign_key` int(10) unsigned DEFAULT NULL,
  `alias` varchar(255) DEFAULT '',
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=124 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acos`
--

LOCK TABLES `acos` WRITE;
/*!40000 ALTER TABLE `acos` DISABLE KEYS */;
INSERT INTO `acos` VALUES (1,NULL,NULL,NULL,'controllers',1,244),(2,1,NULL,NULL,'Administracion',2,5),(3,2,NULL,NULL,'index',3,4),(4,1,NULL,NULL,'Dependenciacategorias',6,17),(5,4,NULL,NULL,'index',7,8),(6,4,NULL,NULL,'view',9,10),(7,4,NULL,NULL,'add',11,12),(8,4,NULL,NULL,'edit',13,14),(9,4,NULL,NULL,'delete',15,16),(10,1,NULL,NULL,'Dependencias',18,29),(11,10,NULL,NULL,'index',19,20),(12,10,NULL,NULL,'view',21,22),(13,10,NULL,NULL,'add',23,24),(14,10,NULL,NULL,'edit',25,26),(15,10,NULL,NULL,'delete',27,28),(16,1,NULL,NULL,'Ejecucionevaluaciones',30,33),(17,16,NULL,NULL,'add',31,32),(18,1,NULL,NULL,'Ejecucionfisicas',34,39),(19,18,NULL,NULL,'add',35,36),(20,18,NULL,NULL,'delete',37,38),(21,1,NULL,NULL,'Ejecucionindicadores',40,43),(22,21,NULL,NULL,'add',41,42),(23,1,NULL,NULL,'Evaluacion',44,57),(24,23,NULL,NULL,'index',45,46),(25,23,NULL,NULL,'principal',47,48),(26,23,NULL,NULL,'ejecucion_fisica',49,50),(27,23,NULL,NULL,'metas_pendientes',51,52),(28,23,NULL,NULL,'producto_metas',53,54),(29,23,NULL,NULL,'indicadores',55,56),(30,1,NULL,NULL,'Groups',58,69),(31,30,NULL,NULL,'index',59,60),(32,30,NULL,NULL,'view',61,62),(33,30,NULL,NULL,'add',63,64),(34,30,NULL,NULL,'edit',65,66),(35,30,NULL,NULL,'delete',67,68),(36,1,NULL,NULL,'Indicadores',70,77),(37,36,NULL,NULL,'add',71,72),(38,36,NULL,NULL,'edit',73,74),(39,36,NULL,NULL,'delete',75,76),(40,1,NULL,NULL,'Medioverificaciones',78,85),(41,40,NULL,NULL,'add',79,80),(42,40,NULL,NULL,'edit',81,82),(43,40,NULL,NULL,'delete',83,84),(44,1,NULL,NULL,'Metapendientes',86,89),(45,44,NULL,NULL,'add',87,88),(46,1,NULL,NULL,'Metas',90,97),(47,46,NULL,NULL,'add',91,92),(48,46,NULL,NULL,'edit',93,94),(49,46,NULL,NULL,'delete',95,96),(50,1,NULL,NULL,'Objetivos',98,109),(51,50,NULL,NULL,'index',99,100),(52,50,NULL,NULL,'view',101,102),(53,50,NULL,NULL,'add',103,104),(54,50,NULL,NULL,'edit',105,106),(55,50,NULL,NULL,'delete',107,108),(56,1,NULL,NULL,'Organos',110,121),(57,56,NULL,NULL,'index',111,112),(58,56,NULL,NULL,'view',113,114),(59,56,NULL,NULL,'add',115,116),(60,56,NULL,NULL,'edit',117,118),(61,56,NULL,NULL,'delete',119,120),(62,1,NULL,NULL,'Pages',122,125),(63,62,NULL,NULL,'display',123,124),(64,1,NULL,NULL,'Planificacion',126,141),(65,64,NULL,NULL,'index',127,128),(66,64,NULL,NULL,'principal',129,130),(67,64,NULL,NULL,'metas',131,132),(68,64,NULL,NULL,'indicadores',133,134),(69,64,NULL,NULL,'medioverificaciones',135,136),(70,64,NULL,NULL,'unidadm_supuestos',137,138),(71,64,NULL,NULL,'ejecucion_fisica',139,140),(72,1,NULL,NULL,'Poacabeceras',142,153),(73,72,NULL,NULL,'index',143,144),(74,72,NULL,NULL,'view',145,146),(75,72,NULL,NULL,'add',147,148),(76,72,NULL,NULL,'edit',149,150),(77,72,NULL,NULL,'delete',151,152),(78,1,NULL,NULL,'Poas',154,165),(79,78,NULL,NULL,'index',155,156),(80,78,NULL,NULL,'view',157,158),(81,78,NULL,NULL,'add',159,160),(82,78,NULL,NULL,'edit',161,162),(83,78,NULL,NULL,'delete',163,164),(84,1,NULL,NULL,'Productometas',166,169),(85,84,NULL,NULL,'add',167,168),(86,1,NULL,NULL,'Reportes',170,181),(87,86,NULL,NULL,'index',171,172),(88,86,NULL,NULL,'reporte1',173,174),(89,86,NULL,NULL,'reporte1_pdf',175,176),(90,86,NULL,NULL,'reporte1_excel',177,178),(91,86,NULL,NULL,'reporte1_json',179,180),(92,1,NULL,NULL,'Supuestos',182,189),(93,92,NULL,NULL,'add',183,184),(94,92,NULL,NULL,'edit',185,186),(95,92,NULL,NULL,'delete',187,188),(96,1,NULL,NULL,'Trimestres',190,201),(97,96,NULL,NULL,'index',191,192),(98,96,NULL,NULL,'view',0,0),(99,96,NULL,NULL,'view',193,194),(100,96,NULL,NULL,'add',195,196),(101,96,NULL,NULL,'edit',197,198),(102,96,NULL,NULL,'delete',199,200),(103,1,NULL,NULL,'Unidadmedidas',202,209),(104,103,NULL,NULL,'add',203,204),(105,103,NULL,NULL,'edit',205,206),(106,103,NULL,NULL,'delete',207,208),(107,1,NULL,NULL,'Users',210,231),(108,107,NULL,NULL,'index',211,212),(109,107,NULL,NULL,'view',213,214),(110,107,NULL,NULL,'edit',215,216),(111,107,NULL,NULL,'password',217,218),(112,107,NULL,NULL,'initDB',219,220),(113,107,NULL,NULL,'checkAro',221,222),(114,107,NULL,NULL,'rebuildARO',223,224),(115,107,NULL,NULL,'login',225,226),(116,107,NULL,NULL,'logout',227,228),(117,107,NULL,NULL,'profile',229,230),(118,1,NULL,NULL,'AclExtras',232,233),(119,1,NULL,NULL,'DebugKit',234,241),(120,119,NULL,NULL,'ToolbarAccess',235,240),(121,120,NULL,NULL,'history_state',236,237),(122,120,NULL,NULL,'sql_explain',238,239),(123,1,NULL,NULL,'Search',242,243);
/*!40000 ALTER TABLE `acos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aros`
--

DROP TABLE IF EXISTS `aros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aros` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT '',
  `foreign_key` int(10) unsigned DEFAULT NULL,
  `alias` varchar(255) DEFAULT '',
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=155 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aros`
--

LOCK TABLES `aros` WRITE;
/*!40000 ALTER TABLE `aros` DISABLE KEYS */;
INSERT INTO `aros` VALUES (1,NULL,'Group',1,'',1,6),(2,NULL,'Group',2,'',7,8),(3,NULL,'Group',3,'',9,10),(4,NULL,'Group',4,'',11,14),(5,NULL,'Group',5,'',15,16),(6,NULL,'Group',6,'',17,18),(7,NULL,'Group',7,'',19,20),(8,NULL,'Group',8,'',21,22),(9,NULL,'Group',9,'',23,306),(10,NULL,'Group',99,'',307,308),(11,1,'User',1,'',2,3),(12,9,'User',2,'',24,25),(13,9,'User',6,'',26,27),(14,9,'User',7,'',28,29),(15,9,'User',8,'',30,31),(16,9,'User',9,'',32,33),(17,9,'User',11,'',34,35),(18,9,'User',12,'',36,37),(19,9,'User',13,'',38,39),(20,9,'User',14,'',40,41),(21,9,'User',15,'',42,43),(22,9,'User',16,'',44,45),(23,9,'User',17,'',46,47),(24,9,'User',18,'',48,49),(25,9,'User',19,'',50,51),(26,9,'User',20,'',52,53),(27,9,'User',21,'',54,55),(28,9,'User',22,'',56,57),(29,9,'User',23,'',58,59),(30,9,'User',24,'',60,61),(31,9,'User',25,'',62,63),(32,9,'User',26,'',64,65),(33,9,'User',27,'',66,67),(34,9,'User',28,'',68,69),(35,9,'User',29,'',70,71),(36,9,'User',30,'',72,73),(37,9,'User',31,'',74,75),(38,9,'User',32,'',76,77),(39,9,'User',33,'',78,79),(40,9,'User',34,'',80,81),(41,9,'User',35,'',82,83),(42,9,'User',36,'',84,85),(43,9,'User',37,'',86,87),(44,9,'User',38,'',88,89),(45,9,'User',39,'',90,91),(46,9,'User',40,'',92,93),(47,9,'User',41,'',94,95),(48,9,'User',42,'',96,97),(49,9,'User',43,'',98,99),(50,9,'User',44,'',100,101),(51,9,'User',45,'',102,103),(52,9,'User',46,'',104,105),(53,9,'User',47,'',106,107),(54,9,'User',48,'',108,109),(55,9,'User',49,'',110,111),(56,9,'User',50,'',112,113),(57,9,'User',51,'',114,115),(58,9,'User',52,'',116,117),(59,9,'User',53,'',118,119),(60,9,'User',54,'',120,121),(61,9,'User',55,'',122,123),(62,9,'User',56,'',124,125),(63,9,'User',57,'',126,127),(64,9,'User',58,'',128,129),(65,9,'User',59,'',130,131),(66,9,'User',60,'',132,133),(67,9,'User',61,'',134,135),(68,9,'User',62,'',136,137),(69,9,'User',63,'',138,139),(70,9,'User',64,'',140,141),(71,9,'User',65,'',142,143),(72,9,'User',66,'',144,145),(73,9,'User',67,'',146,147),(74,9,'User',68,'',148,149),(75,9,'User',69,'',150,151),(76,9,'User',70,'',152,153),(77,9,'User',71,'',154,155),(78,9,'User',72,'',156,157),(79,9,'User',73,'',158,159),(80,9,'User',74,'',160,161),(81,9,'User',75,'',162,163),(82,9,'User',76,'',164,165),(83,1,'User',77,'',4,5),(84,9,'User',78,'',166,167),(85,9,'User',79,'',168,169),(86,9,'User',80,'',170,171),(87,9,'User',81,'',172,173),(88,9,'User',82,'',174,175),(89,9,'User',83,'',176,177),(90,9,'User',84,'',178,179),(91,9,'User',85,'',180,181),(92,9,'User',86,'',182,183),(93,9,'User',87,'',184,185),(94,9,'User',88,'',186,187),(95,9,'User',89,'',188,189),(96,9,'User',90,'',190,191),(97,9,'User',91,'',192,193),(98,9,'User',92,'',194,195),(99,9,'User',93,'',196,265),(100,9,'User',94,'',266,267),(101,9,'User',95,'',268,269),(102,9,'User',96,'',270,271),(103,9,'User',97,'',272,273),(104,9,'User',98,'',274,275),(105,9,'User',99,'',276,277),(106,9,'User',100,'',278,279),(107,9,'User',101,'',280,281),(108,9,'User',102,'',282,283),(109,9,'User',103,'',284,285),(110,9,'User',104,'',286,287),(111,9,'User',105,'',288,289),(112,9,'User',106,'',290,291),(113,9,'User',107,'',292,293),(114,9,'User',108,'',294,295),(115,9,'User',109,'',296,297),(116,9,'User',110,'',298,299),(117,9,'User',111,'',300,301),(118,9,'User',112,'',302,303),(119,99,'User',113,'',197,198),(120,99,'User',114,'',199,200),(121,99,'User',115,'',201,202),(122,99,'User',116,'',203,204),(123,99,'User',117,'',205,206),(124,99,'User',118,'',207,208),(125,99,'User',119,'',209,210),(126,99,'User',120,'',211,212),(127,99,'User',121,'',213,214),(128,99,'User',122,'',215,216),(129,99,'User',123,'',217,218),(130,99,'User',124,'',219,220),(131,99,'User',125,'',221,222),(132,99,'User',126,'',223,224),(133,99,'User',127,'',225,226),(134,99,'User',128,'',227,228),(135,99,'User',129,'',229,230),(136,99,'User',130,'',231,232),(137,99,'User',131,'',233,234),(138,99,'User',132,'',235,236),(139,99,'User',133,'',237,238),(140,99,'User',134,'',239,240),(141,99,'User',135,'',241,242),(142,99,'User',136,'',243,244),(143,99,'User',137,'',245,246),(144,99,'User',138,'',247,248),(145,99,'User',139,'',249,250),(146,99,'User',140,'',251,252),(147,99,'User',141,'',253,254),(148,99,'User',142,'',255,256),(149,99,'User',143,'',257,258),(150,99,'User',144,'',259,260),(151,99,'User',145,'',261,262),(152,99,'User',146,'',263,264),(153,4,'User',147,'',12,13),(154,9,'User',148,'',304,305);
/*!40000 ALTER TABLE `aros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aros_acos`
--

DROP TABLE IF EXISTS `aros_acos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aros_acos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `aro_id` int(10) unsigned NOT NULL,
  `aco_id` int(10) unsigned NOT NULL,
  `_create` char(2) NOT NULL DEFAULT '0',
  `_read` char(2) NOT NULL DEFAULT '0',
  `_update` char(2) NOT NULL DEFAULT '0',
  `_delete` char(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aros_acos`
--

LOCK TABLES `aros_acos` WRITE;
/*!40000 ALTER TABLE `aros_acos` DISABLE KEYS */;
INSERT INTO `aros_acos` VALUES (1,1,1,'1','1','1','1'),(2,2,1,'1','1','1','1'),(3,8,1,'-1','-1','-1','-1'),(4,7,1,'-1','-1','-1','-1'),(5,4,1,'1','1','1','1');
/*!40000 ALTER TABLE `aros_acos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ejecucionevaluaciones`
--

DROP TABLE IF EXISTS `ejecucionevaluaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ejecucionevaluaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unidadmedida_id` int(11) NOT NULL,
  `trimestre_id` int(11) NOT NULL,
  `trimestre_pertenece_id` int(11) NOT NULL,
  `cantidad_ejecutada` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_ejecucionevaluaciones_trimestres1_idx` (`trimestre_id`),
  KEY `fk_ejecucionevaluaciones_unidadmedidas1_idx` (`unidadmedida_id`),
  KEY `fk_ejecucionevaluaciones_trimestres2_idx` (`trimestre_pertenece_id`),
  CONSTRAINT `fk_ejecucionevaluaciones_trimestres1` FOREIGN KEY (`trimestre_id`) REFERENCES `trimestres` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ejecucionevaluaciones_trimestres2` FOREIGN KEY (`trimestre_pertenece_id`) REFERENCES `trimestres` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ejecucionevaluaciones_unidadmedidas1` FOREIGN KEY (`unidadmedida_id`) REFERENCES `unidadmedidas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ejecucionevaluaciones`
--

LOCK TABLES `ejecucionevaluaciones` WRITE;
/*!40000 ALTER TABLE `ejecucionevaluaciones` DISABLE KEYS */;
/*!40000 ALTER TABLE `ejecucionevaluaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ejecucionfisicas`
--

DROP TABLE IF EXISTS `ejecucionfisicas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ejecucionfisicas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unidadmedida_id` int(11) NOT NULL,
  `trimestre_id` int(11) NOT NULL DEFAULT '0',
  `cantidad_planificada` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `indicadore_id_UNIQUE` (`trimestre_id`,`unidadmedida_id`),
  KEY `fk_ejecucionfisicas_trimestres1_idx` (`trimestre_id`),
  KEY `fk_ejecucionfisicas_unidadmedidas1_idx` (`unidadmedida_id`),
  CONSTRAINT `fk_ejecucionfisicas_trimestres1` FOREIGN KEY (`trimestre_id`) REFERENCES `trimestres` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ejecucionfisicas_unidadmedidas1` FOREIGN KEY (`unidadmedida_id`) REFERENCES `unidadmedidas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=682 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ejecucionfisicas`
--

LOCK TABLES `ejecucionfisicas` WRITE;
/*!40000 ALTER TABLE `ejecucionfisicas` DISABLE KEYS */;
/*!40000 ALTER TABLE `ejecucionfisicas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ejecucionindicadores`
--

DROP TABLE IF EXISTS `ejecucionindicadores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ejecucionindicadores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ejecucionevaluacione_id` int(11) NOT NULL,
  `interpretacion` text NOT NULL,
  `acciones_correctivas` text,
  PRIMARY KEY (`id`),
  KEY `fk_ejecucionindicadores_ejecucionevaluaciones1_idx` (`ejecucionevaluacione_id`),
  CONSTRAINT `fk_ejecucionindicadores_ejecucionevaluaciones1` FOREIGN KEY (`ejecucionevaluacione_id`) REFERENCES `ejecucionevaluaciones` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ejecucionindicadores`
--

LOCK TABLES `ejecucionindicadores` WRITE;
/*!40000 ALTER TABLE `ejecucionindicadores` DISABLE KEYS */;
/*!40000 ALTER TABLE `ejecucionindicadores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fases`
--

DROP TABLE IF EXISTS `fases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `denominacion` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fases`
--

LOCK TABLES `fases` WRITE;
/*!40000 ALTER TABLE `fases` DISABLE KEYS */;
INSERT INTO `fases` VALUES (1,'Planificacion'),(2,'Evaluacion');
/*!40000 ALTER TABLE `fases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `indicadores`
--

DROP TABLE IF EXISTS `indicadores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `indicadores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `meta_id` int(11) NOT NULL,
  `resultado` text NOT NULL,
  `numerador` text NOT NULL,
  `denominador` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_indicadores_metas1_idx` (`meta_id`),
  CONSTRAINT `fk_indicadores_metas1` FOREIGN KEY (`meta_id`) REFERENCES `metas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=168 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `indicadores`
--

LOCK TABLES `indicadores` WRITE;
/*!40000 ALTER TABLE `indicadores` DISABLE KEYS */;
/*!40000 ALTER TABLE `indicadores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medioverificaciones`
--

DROP TABLE IF EXISTS `medioverificaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `medioverificaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `indicadore_id` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_medioverificaciones_indicadores1_idx` (`indicadore_id`),
  CONSTRAINT `fk_medioverificaciones_indicadores1` FOREIGN KEY (`indicadore_id`) REFERENCES `indicadores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=189 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medioverificaciones`
--

LOCK TABLES `medioverificaciones` WRITE;
/*!40000 ALTER TABLE `medioverificaciones` DISABLE KEYS */;
/*!40000 ALTER TABLE `medioverificaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `metapendientes`
--

DROP TABLE IF EXISTS `metapendientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `metapendientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unidadmedida_id` int(11) NOT NULL,
  `trimestre_reporte_id` int(11) NOT NULL,
  `trimestre_id` int(11) NOT NULL,
  `trimestre_ejecucion_id` int(11) NOT NULL,
  `cantidad_pendiente` int(11) NOT NULL DEFAULT '0',
  `explicacion` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ejecucionpendientes_trimestres1_idx` (`trimestre_id`),
  KEY `fk_metapendientes_unidadmedidas1_idx` (`unidadmedida_id`),
  KEY `fk_metapendientes_trimestres1_idx` (`trimestre_ejecucion_id`),
  KEY `fk_metapendientes_trimestres2_idx` (`trimestre_reporte_id`),
  CONSTRAINT `fk_ejecucionpendientes_trimestres1` FOREIGN KEY (`trimestre_id`) REFERENCES `trimestres` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_metapendientes_trimestres1` FOREIGN KEY (`trimestre_ejecucion_id`) REFERENCES `trimestres` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_metapendientes_trimestres2` FOREIGN KEY (`trimestre_reporte_id`) REFERENCES `trimestres` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_metapendientes_unidadmedidas1` FOREIGN KEY (`unidadmedida_id`) REFERENCES `unidadmedidas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `metapendientes`
--

LOCK TABLES `metapendientes` WRITE;
/*!40000 ALTER TABLE `metapendientes` DISABLE KEYS */;
/*!40000 ALTER TABLE `metapendientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `metas`
--

DROP TABLE IF EXISTS `metas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `metas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poa_id` int(11) NOT NULL,
  `objetivo_id` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  `costo` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_metas_objetivos1_idx` (`objetivo_id`),
  KEY `fk_metas_poas1_idx` (`poa_id`),
  CONSTRAINT `fk_metas_objetivos1` FOREIGN KEY (`objetivo_id`) REFERENCES `objetivos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_metas_poas1` FOREIGN KEY (`poa_id`) REFERENCES `poas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=156 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `metas`
--

LOCK TABLES `metas` WRITE;
/*!40000 ALTER TABLE `metas` DISABLE KEYS */;
/*!40000 ALTER TABLE `metas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `objetivos`
--

DROP TABLE IF EXISTS `objetivos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `objetivos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `objetivos`
--

LOCK TABLES `objetivos` WRITE;
/*!40000 ALTER TABLE `objetivos` DISABLE KEYS */;
/*!40000 ALTER TABLE `objetivos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `objetivos_poas`
--

DROP TABLE IF EXISTS `objetivos_poas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `objetivos_poas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poa_id` int(11) NOT NULL,
  `objetivo_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_poas_has_objetivos_objetivos1_idx` (`objetivo_id`),
  KEY `fk_poas_has_objetivos_poas1_idx` (`poa_id`),
  CONSTRAINT `fk_poas_has_objetivos_objetivos1` FOREIGN KEY (`objetivo_id`) REFERENCES `objetivos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_poas_has_objetivos_poas1` FOREIGN KEY (`poa_id`) REFERENCES `poas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `objetivos_poas`
--

LOCK TABLES `objetivos_poas` WRITE;
/*!40000 ALTER TABLE `objetivos_poas` DISABLE KEYS */;
/*!40000 ALTER TABLE `objetivos_poas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `organos`
--

DROP TABLE IF EXISTS `organos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organos`
--

LOCK TABLES `organos` WRITE;
/*!40000 ALTER TABLE `organos` DISABLE KEYS */;
INSERT INTO `organos` VALUES (1,'CONTRALORIA DEL ESTADO MERIDA');
/*!40000 ALTER TABLE `organos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `poacabeceras`
--

DROP TABLE IF EXISTS `poacabeceras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `poacabeceras` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `denominacion` varchar(200) NOT NULL,
  `descripcion` text NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rght` int(11) DEFAULT NULL,
  `seleccionable` tinyint(1) NOT NULL DEFAULT '0',
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_poacabeceras_poacabeceras1_idx` (`parent_id`),
  CONSTRAINT `fk_poacabeceras_poacabeceras1` FOREIGN KEY (`parent_id`) REFERENCES `poacabeceras` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `poacabeceras`
--

LOCK TABLES `poacabeceras` WRITE;
/*!40000 ALTER TABLE `poacabeceras` DISABLE KEYS */;
INSERT INTO `poacabeceras` VALUES (6,'DIRECTRIZ PNSB','5.1.3. DEMOCRACIA PROTAGONICA Y REVOLUCIONARIA',NULL,6,15,0,1),(7,'AREA ESTRATEGICA DE DESARROLLO','5. DERECHOS SOCIALES',6,7,14,0,1),(8,'OBJETIVO ESTADAL','5.4.14 FOMENTAR UNA GESTION PUBLICA AL SERVICIO DEL PUEBLO BASADA EN LA NUEVA ETICA SOCIALISTA DEL SERVIDOR PUBLICO, LA CUAL SE SUSTENTA EN UN CONJUNTO DE VALORES Y PRINCIPIOS HUMANISTAS',7,8,13,0,1),(9,'ESTRATEGIA ESTADAL','5.5.2 CONSTRUIR UNA ESTRUCTURA SOCIAL INCLUYENTE Y UN NUEVO MODELO SOCIAL, PRODUCTIVO, HUMANISTA Y ENDOGENO',8,9,12,0,1),(10,'POLITICA ESTADAL','5.5.2.1 CREAR UNA ESTRUCTURA GUBERNAMENTAL BASADA EN LOS PRINCIPIOS DE EQUIDAD, EFICIENCIA Y EFICACIA AL SERVICIO DEL PUEBLO',9,10,11,1,1);
/*!40000 ALTER TABLE `poacabeceras` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `poas`
--

DROP TABLE IF EXISTS `poas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `poas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dependencia_id` int(11) NOT NULL,
  `ano` year(4) NOT NULL,
  `es_reprogramado` tinyint(1) NOT NULL DEFAULT '0',
  `numero_reprogramacion` smallint(1) DEFAULT NULL,
  `poacabecera_id` int(11) NOT NULL,
  `organo_id` int(11) NOT NULL,
  `status` smallint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_poas_dependencias1_idx` (`dependencia_id`),
  KEY `fk_poas_poacabeceras1_idx` (`poacabecera_id`),
  KEY `fk_poas_organos1_idx` (`organo_id`),
  CONSTRAINT `fk_poas_organos1` FOREIGN KEY (`organo_id`) REFERENCES `organos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_poas_pasos1` FOREIGN KEY (`organo_id`) REFERENCES `organos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_poas_poacabeceras1` FOREIGN KEY (`poacabecera_id`) REFERENCES `poacabeceras` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `poas`
--

LOCK TABLES `poas` WRITE;
/*!40000 ALTER TABLE `poas` DISABLE KEYS */;
/*!40000 ALTER TABLE `poas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productometas`
--

DROP TABLE IF EXISTS `productometas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productometas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ejecucionevaluacione_id` int(11) NOT NULL,
  `trimestre_id` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  `medio_verificacion` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_productometas_trimestres1_idx` (`trimestre_id`),
  KEY `fk_productometas_ejecucionevaluaciones1_idx` (`ejecucionevaluacione_id`),
  CONSTRAINT `fk_productometas_ejecucionevaluaciones1` FOREIGN KEY (`ejecucionevaluacione_id`) REFERENCES `ejecucionevaluaciones` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_productometas_trimestres1` FOREIGN KEY (`trimestre_id`) REFERENCES `trimestres` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productometas`
--

LOCK TABLES `productometas` WRITE;
/*!40000 ALTER TABLE `productometas` DISABLE KEYS */;
/*!40000 ALTER TABLE `productometas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supuestos`
--

DROP TABLE IF EXISTS `supuestos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supuestos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `medioverificacione_id` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_supuestos_medioverificaciones1_idx` (`medioverificacione_id`),
  CONSTRAINT `fk_supuestos_medioverificaciones1` FOREIGN KEY (`medioverificacione_id`) REFERENCES `medioverificaciones` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=227 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supuestos`
--

LOCK TABLES `supuestos` WRITE;
/*!40000 ALTER TABLE `supuestos` DISABLE KEYS */;
/*!40000 ALTER TABLE `supuestos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trimestres`
--

DROP TABLE IF EXISTS `trimestres`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trimestres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `denominacion` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trimestres`
--

LOCK TABLES `trimestres` WRITE;
/*!40000 ALTER TABLE `trimestres` DISABLE KEYS */;
INSERT INTO `trimestres` VALUES (1,'Trimestre I'),(2,'Trimestre II'),(3,'Trimestre III'),(4,'Trimestre IV');
/*!40000 ALTER TABLE `trimestres` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unidadmedidas`
--

DROP TABLE IF EXISTS `unidadmedidas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unidadmedidas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `medioverificacione_id` int(11) NOT NULL,
  `denominacion` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_unidadmedidas_medioverificaciones1_idx` (`medioverificacione_id`),
  CONSTRAINT `fk_unidadmedidas_medioverificaciones1` FOREIGN KEY (`medioverificacione_id`) REFERENCES `medioverificaciones` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=175 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unidadmedidas`
--

LOCK TABLES `unidadmedidas` WRITE;
/*!40000 ALTER TABLE `unidadmedidas` DISABLE KEYS */;
/*!40000 ALTER TABLE `unidadmedidas` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-09-30 11:37:34
