<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');
	
	public function beforeFilter() {
		parent::beforeFilter();
		// For CakePHP 2.1 and up
		$this->Auth->allow('initDB', "checkAro", "rebuildARO", "login", "logout");
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->User->recursive = 0;
		$users = $this->paginate();
		$this->set(compact('users'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$user = $this->User->find('first', $options);
		$this->set(compact('user'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$user = $this->User->findById($id);
			$user['User']['group_id'] = $this->request->data['User']['group_id'];
			$user['User']['cargo'] = $this->request->data['User']['cargo'];
			if ($this->User->save($user)) {
				$this->Session->setFlash(__('The user has been saved'), 'success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'error');
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
		$groups = $this->User->Group->find('list');
		$sistemas = $this->User->Sistema->find('list');
		$dependencias = $this->User->Dependencia->find('list');
		$auth_user = $this->Session->read('Auth.User');
		$this->set(compact('groups', 'sistemas', 'dependencias', 'auth_user'));
	}

/**
 * password method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function password($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->User->save($this->request->data, true, array('id', 'username', 'group_id', 'first_name', 'last_name', 'group_id', 'created', 'modified'))) {
				$this->Session->setFlash(__('The user has been saved'), 'success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'error');
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
		$groups = $this->User->Group->find('list');
		$this->set(compact('groups'));
	}
	
	public function initDB() {
		$group = $this->User->Group;
		// Administradores
		$group->id = 1;
		$this->Acl->allow($group, 'controllers');
		
		// Contralor
		$group->id = 2;
		$this->Acl->allow($group, 'controllers');
		
		// Director General
		$group->id = 8;
		$this->Acl->deny($group, 'controllers');
		
		// Directores
		$group->id = 7;
		$this->Acl->deny($group, 'controllers');
		
		// Coordinadores
		$group->id = 4;
		$this->Acl->allow($group, 'controllers');
		echo "all done";
		
		// Asistentes de Auditor Fiscal
		$group->id = 9;
		$this->Acl->allow($group, 'controllers');
		echo "all done";
		exit;
	}
	
	public function checkAro($user) {
		$aro = new Aro();
		$conditions = array(
			'conditions' => array(
				'parent_id' => $user['User']['group_id'],
				'foreign_key' => $user['User']['id']
			)
		);
		$myaro = $aro->find('first', $conditions);
		if (!$myaro) {
			$data = array(
				'foreign_key' => $user['User']['id'],
				'model' => 'User',
				'parent_id' => $user['User']['group_id']
			);
			$myaro = $aro->create();
			$aro->save($data);
		}
	}
	
	public function rebuildARO() {
		// Build the groups.
		$groups = $this->User->Group->find('all');
		$aro = new Aro();
		foreach($groups as $group) {
			$aro->create();
			$aro->save(array(
			//	'alias'=>$group['Group']['name'],
				'foreign_key' => $group['Group']['id'],
				'model'=>'Group',
				'parent_id' => null
			));
		}
	 
		// Build the users.
		$users = $this->User->find('all');
		$i=0;
		foreach($users as $user) {
			$aroList[$i++]= array(
			//	'alias' => $user['User']['email'],
				'foreign_key' => $user['User']['id'],
				'model' => 'User',
				'parent_id' => $user['User']['group_id']
			);	
		}
		foreach($aroList as $data) {
			$aro->create();
			$aro->save($data);
		}
	 
		echo "AROs rebuilt!";
		return true;
	}
	

	

/**
 * login method
 *
 * @return void
 */
	public function login() {
		if ($this->request->is('post')) {
			if ($this->Auth->login()) {
				$this->User->setDataSource("auth");
				$auth_user = $this->Session->read("Auth.User");
				$this->User->recursive = -1;
				$user = $this->User->findByUsername($this->request->data['User']['username']);
				if ($user) {
					$this->checkAro($user);
					$user['User']['password'] = $this->request->data['User']['password'];
					$user = $this->User->save($user);
					$is_new = false;
				} else {
					$auth_user['User']['group_id'] = 9; // 99 = Bloqueado // Por ahora todos son asistentes fiscals
					$user = $this->User->save($auth_user);
					$user = $this->User->findByUsername($this->request->data['User']['username']);
					$is_new = true;
				}
				if ($this->User->hasAccessTo('sipoa', $user['User']['id'])) {
					$this->Auth->login($user['User']);
					if ($is_new || ($user['User']['group_id'] == 99)) {
						return $this->redirect("/users/edit/" . $user['User']['id']);
					} else {
						return $this->redirect($this->Auth->redirectUrl());
					}
				} else {
					$this->Session->setFlash(__('Usted no tiene acceso al sistema.'), 'error');
					$this->redirect($this->Auth->logout());
				}
			}
			$this->Session->setFlash(__('Usuario o contraseña incorrectos.'), 'error');
		}
	}
	
	public function logout() {
		$this->Session->setFlash(__('Ha dejado el sistema exitosamente'), 'success');
		$this->redirect($this->Auth->logout());
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function profile() {
		$auth_user = $this->Session->read("Auth.User");
		if (!$auth_user) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->User->save($this->request->data, array('cargo'))) {
				$this->Session->setFlash(__('The user has been saved.'), 'success');
				return $this->redirect($this->referer());
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'error');
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $auth_user['id']));
			$this->request->data = $this->User->find('first', $options);
		}
		$groups = $this->User->Group->find('list');
		$sistemas = $this->User->Sistema->find('list');
		$this->set(compact('groups', 'sistemas', 'auth_user'));
	}
}
