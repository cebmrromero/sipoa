<?php
App::uses('AppController', 'Controller');
/**
 * Metapendientes Controller
 *
 * @property Metapendiente $Metapendiente
 */
class MetapendientesController extends AppController {

/**
 * add method
 *
 * @return void
 */
	public function add($poa_id = null, $trimestre_id = null) {
		if ($this->request->is('post')) {
			$this->Metapendiente->create();
			
			$error = false;
			foreach ($this->request->data as $data) {
				if (!$this->Metapendiente->saveAll($data)) {
					$error = true;
				}
			}
			
			if ($error) {
				$this->Session->setFlash(__('The metapendiente could not be saved. Please, try again.'), 'error');
				$this->redirect($this->referer());
			} else {
				$this->Session->setFlash(__('The metapendiente has been saved'), 'success');
				$this->redirect(array('controller' => 'evaluacion', 'action' => 'producto_metas', $poa_id, $trimestre_id));;
			}
		}
	}
}
