<?php
App::uses('AppController', 'Controller');
/**
 * Organos Controller
 *
 * @property Organo $Organo
 * @property PaginatorComponent $Paginator
 */
class OrganosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Organo->recursive = 0;
		$organos = $this->paginate();
		$this->set(compact('organos'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Organo->recursive = 2;
		if (!$this->Organo->exists($id)) {
			throw new NotFoundException(__('Invalid organo'));
		}
		$options = array('conditions' => array('Organo.' . $this->Organo->primaryKey => $id));
		$organo = $this->Organo->find('first', $options);
		$this->set(compact('organo'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Organo->create();
			if ($this->Organo->save($this->request->data)) {
				$this->Session->setFlash(__('The organo has been saved'), 'success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The organo could not be saved. Please, try again.'), 'error');
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Organo->exists($id)) {
			throw new NotFoundException(__('Invalid organo'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Organo->save($this->request->data)) {
				$this->Session->setFlash(__('The organo has been saved'), 'success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The organo could not be saved. Please, try again.'), 'error');
			}
		} else {
			$options = array('conditions' => array('Organo.' . $this->Organo->primaryKey => $id));
			$this->request->data = $this->Organo->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Organo->id = $id;
		if (!$this->Organo->exists()) {
			throw new NotFoundException(__('Invalid organo'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Organo->delete()) {
			$this->Session->setFlash(__('Organo deleted'), 'success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Organo was not deleted'), 'error');
		$this->redirect(array('action' => 'index'));
	}}
