<?php
App::uses('AppController', 'Controller');

App::build(array('Vendor' => array(APP . 'Vendor' . DS . 'Html2pdf' . DS))); 
App::uses('HTML2PDF', 'Vendor');
/**
 * Reportes Controller
 *
 * @property Objetivo $Objetivo
 */
class ReportesController extends AppController {
	
	public function beforeFilter() {
		parent::beforeFilter();
		// For CakePHP 2.1 and up
		$this->Auth->allow('reporte1_json');
	}
/**
 * Operaciones no tiene modelo
 */
	var $uses = array('Poa');
	var $components = array('Search.Prg', 'RequestHandler');
	var $presetVars = true; // using the model configuration

/**
 * index method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function index() {
		$this->Session->write('operacion', 'reportes');
		$reportes = array(array('nombre' => "Reporte de Poas, según filtros dados", 'src' => 'reporte1'));
		$this->set(compact('reportes'));
	}
	
	
	public function reporte1() {
        $this->Prg->commonProcess();
		
		$parsedParams = $this->Prg->parsedParams();
		if ($parsedParams) {
			$this->Poa->recursive = 0;
			$this->paginate = array(
				'conditions' => $this->Poa->parseCriteria($parsedParams),
			);
			$poas = $this->paginate();
			$this->loadModel("Ejecucionfisica");
			foreach ($poas as &$poa) {
				$poa['Ejecucionfisica'] = $this->Ejecucionfisica->getEjecucionFisicaByPoaId($poa['Poa']['id']);
				if (isset($this->request->params['named']['trimestredesde_id']) or isset($this->request->params['named']['trimestrehasta_id'])) {
					$poa['Nivelejecucion'] = $this->Ejecucionfisica->getNivelEjecucionByTrimestre($poa['Poa']['id'], $this->request->params['named']);
				}
			}
			//print_r($poas[0]['Ejecucionfisica']);
		}
		$organos = $this->Poa->Organo->find('list');
		$dependencias = $this->Poa->Dependencia->find('list');
		$dependenciacategorias = $this->Poa->Dependencia->Dependenciacategoria->find('list');
		$aux = range(date('Y'), date('Y') - 5);
		$anos = array();
		foreach ($aux as $year) {
			$anos[$year] = $year;
		}
		$this->loadModel("Trimestre");
		$trimestredesdes = $trimestrehastas = $this->Trimestre->find('list');
		$this->set(compact('poas', 'organos', 'dependencias', 'anos', 'trimestredesdes', 'trimestrehastas', 'dependenciacategorias'));
	}
	
	public function reporte1_pdf() {
        $this->Prg->commonProcess();
		
		$parsedParams = $this->Prg->parsedParams();
		if ($parsedParams) {
			$this->Poa->recursive = 0;
			$this->paginate = array(
				'conditions' => $this->Poa->parseCriteria($parsedParams),
			);
			$poas = $this->paginate();
			$this->loadModel("Ejecucionfisica");
			foreach ($poas as &$poa) {
				$poa['Ejecucionfisica'] = $this->Ejecucionfisica->getEjecucionFisicaByPoaId($poa['Poa']['id']);
				if (isset($this->request->params['named']['trimestredesde_id']) or isset($this->request->params['named']['trimestrehasta_id'])) {
					$poa['Nivelejecucion'] = $this->Ejecucionfisica->getNivelEjecucionByTrimestre($poa['Poa']['id'], $this->request->params['named']);
				}
			}
			//print_r($poas[0]['Ejecucionfisica']);
		}
		$organos = $this->Poa->Organo->find('list');
		$dependencias = $this->Poa->Dependencia->find('list');
		$aux = range(date('Y'), date('Y') - 5);
		$anos = array();
		foreach ($aux as $year) {
			$anos[$year] = $year;
		}
		$this->loadModel("Trimestre");
		$trimestredesdes = $trimestrehastas = $this->Trimestre->find('list');
		
		
		$this->autoRender = false;
		/* Set up new view that won't enter the ClassRegistry */
		$pdf_view = new View($this, false);
		 
		/* Grab output into variable without the view actually outputting! */
		$pdf_view->layout = false;
		
		$pdf_view->set(compact('poas', 'organos', 'dependencias', 'anos', 'trimestredesdes', 'trimestrehastas'));
		
		ob_start();
		echo $pdf_view->render();
		$content = ob_get_clean();
        try
		{
			$html2pdf = new HTML2PDF('P', 'Letter', 'es', true, 'UTF-8', 0);
			$html2pdf->writeHTML($content, false);
			$html2pdf->Output('reporte1.pdf');
		}
		catch(HTML2PDF_exception $e) {
			echo "Error"; 
			echo $e;
			exit;
		}
		
		exit(1);
	}
	
	public function reporte1_excel() {
		$parsedParams = $this->request->params['named'];
		echo $command = "cd pl; perl json.pl " . $this->request->url;
		//$command = "touch pl/test";
		exec($command, $output);
		$this->redirect(array('controller' => 'pl', 'action' => 'reporte1.xls'));
	}
	
	public function reporte1_json() {
        $this->Prg->commonProcess();
		
		$parsedParams = $this->Prg->parsedParams();
		if ($parsedParams) {
			$this->Poa->recursive = 0;
			$this->paginate = array(
				'conditions' => $this->Poa->parseCriteria($parsedParams),
			);
			$poas = $this->paginate();
			$this->loadModel("Ejecucionfisica");
			foreach ($poas as &$poa) {
				$poa['Ejecucionfisica'] = $this->Ejecucionfisica->getEjecucionFisicaByPoaId($poa['Poa']['id']);
				if (isset($this->request->params['named']['trimestredesde_id']) or isset($this->request->params['named']['trimestrehasta_id'])) {
					$poa['Nivelejecucion'] = $this->Ejecucionfisica->getNivelEjecucionByTrimestre($poa['Poa']['id'], $this->request->params['named']);
				}
				foreach ($poa['Ejecucionfisica'] as &$ejecucionfisica) {
					//unset($ejecucionfisica['Indicadore']);
					//print_r($ejecucionfisica);
					$this->Ejecucionfisica->Unidadmedida->Medioverificacione->Supuesto->recursive = -1;
					$supuestos = $this->Ejecucionfisica->Unidadmedida->Medioverificacione->Supuesto->findAllByMedioverificacioneId($ejecucionfisica['Medioverificacione']['id']);
					$ejecucionfisica['Supuesto'] = Hash::extract($supuestos, "{n}.Supuesto");
					
					$this->Ejecucionfisica->Unidadmedida->Medioverificacione->Indicadore->recursive = 1;
					$indicadores = $this->Ejecucionfisica->Unidadmedida->Medioverificacione->Indicadore->findAllById($ejecucionfisica['Indicadore']['id']);
					//$indicadores = $this->Ejecucionfisica->Unidadmedida->Medioverificacione->Indicadore->findAllByMetaId($ejecucionfisica['Meta']['id']);
					//print_r($indicadores);
					//$ejecucionfisica['Indicadore'] = Hash::extract($indicadores, "{n}.Indicadore");
					$ejecucionfisica['Indicadore'] = $indicadores;
				}
				//die;
			}
		}
		$organos = $this->Poa->Organo->find('list');
		$dependencias = $this->Poa->Dependencia->find('list');
		$aux = range(date('Y'), date('Y') - 5);
		$anos = array();
		foreach ($aux as $year) {
			$anos[$year] = $year;
		}
		$this->loadModel("Trimestre");
		$trimestredesdes = $trimestrehastas = $this->Trimestre->find('list');
		
		$this->layout = false;
		
		// Obteniendo la ruta completa que es necesaria para escirbir la cabecera
		foreach ($poas as &$poa) {
			$poa['Poacabecera'] = $this->Poa->Poacabecera->getPath($poa['Poacabecera']['id']);
		}
		//return new CakeResponse(array('body' => json_encode($poas)));
		echo json_encode($poas);
		exit(1);
	}
}