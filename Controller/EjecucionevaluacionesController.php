<?php
App::uses('AppController', 'Controller');
/**
 * Ejecucionevaluaciones Controller
 *
 * @property Ejecucionevaluacione $Ejecucionevaluacione
 */
class EjecucionevaluacionesController extends AppController {

/**
 * add method
 *
 * @return void
 */
	public function add($poa_id = null, $trimestre_id = null) {
		if ($this->request->is('post')) {
			$this->Ejecucionevaluacione->create();
			
			$error = false;
			foreach ($this->request->data as $data) {
				if (!$this->Ejecucionevaluacione->saveAll($data)) {
					$error = true;
				}
			}
			
			if ($error) {
				$this->Session->setFlash(__('The ejecucionevaluacione could not be saved. Please, try again.'), 'error');
				$this->redirect($this->referer());
			} else {
				$this->Session->setFlash(__('The ejecucionevaluacione has been saved'), 'success');
				$this->redirect(array('controller' => 'evaluacion', 'action' => 'metas_pendientes', $poa_id, $trimestre_id));;
			}
		}
	}
}
