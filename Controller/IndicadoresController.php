<?php
App::uses('AppController', 'Controller');
/**
 * Indicadores Controller
 *
 * @property Indicadore $Indicadore
 */
class IndicadoresController extends AppController {

/**
 * add method
 *
 * @return void
 */
	public function add($meta_id) {
		$meta = $this->Indicadore->Meta->findById($meta_id);
		if ($this->request->is('post')) {
			$this->Indicadore->create();
			if ($this->Indicadore->save($this->request->data)) {
				$this->Session->setFlash(__('The indicadore has been saved'), 'success');
				// Si el boton continuar fue presionado
				//if ($this->request->data['Indicadore']['continuar']) {
				//	$this->redirect(array('action' => 'add', $meta_id));
				//} else {
					//$this->redirect(array('controller' => 'planificacion', 'action' => 'medioverificaciones', $meta['Meta']['poa_id'], $this->Indicadore->getLastInsertID()));
					$this->redirect(array('controller' => 'planificacion', 'action' => 'indicadores', $meta['Meta']['poa_id'], $meta_id));
				//}
			} else {
				$this->Session->setFlash(__('The indicadore could not be saved. Please, try again.'), 'error');
			}
		}
		$poa = $this->Indicadore->Meta->Poa->findById($meta['Meta']['poa_id']);
		$this->set(compact('meta', 'poa'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null, $poa_id = null) {
		if (!$this->Indicadore->exists($id)) {
			throw new NotFoundException(__('Invalid indicadore'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Indicadore->save($this->request->data)) {
				$this->Session->setFlash(__('The indicadore has been saved'), 'success');
				$this->redirect(array('controller' => 'planificacion', 'action' => 'indicadores', $poa_id, $this->request->data['Indicadore']['meta_id']));
			} else {
				$this->Session->setFlash(__('The indicadore could not be saved. Please, try again.'), 'error');
			}
		} else {
			$options = array('conditions' => array('Indicadore.' . $this->Indicadore->primaryKey => $id));
			$this->Indicadore->recursive = 2;
			$this->request->data = $this->Indicadore->find('first', $options);
		}
		$metas = $this->Indicadore->Meta->find('list');
		$this->set(compact('metas'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Indicadore->id = $id;
		if (!$this->Indicadore->exists()) {
			throw new NotFoundException(__('Invalid indicadore'));
		}
		$this->request->onlyAllow('get', 'delete');
		if ($this->Indicadore->delete()) {
			$this->Session->setFlash(__('Indicadore deleted'), 'success');
			$this->redirect($this->referer());
		}
		$this->Session->setFlash(__('Indicadore was not deleted'), 'error');
		$this->redirect($this->referer());
	}
}
