<?php
App::uses('AppController', 'Controller');
/**
 * Dependencias Controller
 *
 * @property Dependencia $Dependencia
 * @property PaginatorComponent $Paginator
 */
class DependenciasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Dependencia->recursive = 0;
		$dependencias = $this->paginate();
		$this->set(compact('dependencias'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Dependencia->exists($id)) {
			throw new NotFoundException(__('Invalid dependencia'));
		}
		$options = array('conditions' => array('Dependencia.' . $this->Dependencia->primaryKey => $id));
		$dependencia = $this->Dependencia->find('first', $options);
		$this->set(compact('dependencia'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Dependencia->create();
			if ($this->Dependencia->save($this->request->data)) {
				$this->Session->setFlash(__('The dependencia has been saved'), 'success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The dependencia could not be saved. Please, try again.'), 'error');
			}
		}	
		$dependenciacategorias = $this->Dependencia->Dependenciacategoria->find('list');
		$this->set(compact('dependenciacategorias'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Dependencia->exists($id)) {
			throw new NotFoundException(__('Invalid dependencia'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Dependencia->save($this->request->data)) {
				$this->Session->setFlash(__('The dependencia has been saved'), 'success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The dependencia could not be saved. Please, try again.'), 'error');
			}
		} else {
			$options = array('conditions' => array('Dependencia.' . $this->Dependencia->primaryKey => $id));
			$this->request->data = $this->Dependencia->find('first', $options);
		}
		$dependenciacategorias = $this->Dependencia->Dependenciacategoria->find('list');
		$this->set(compact('dependenciacategorias'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Dependencia->id = $id;
		if (!$this->Dependencia->exists()) {
			throw new NotFoundException(__('Invalid dependencia'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Dependencia->delete()) {
			$this->Session->setFlash(__('Dependencia deleted'), 'success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Dependencia was not deleted'), 'error');
		$this->redirect(array('action' => 'index'));
	}}
