<?php
App::uses('AppController', 'Controller');
/**
 * Objetivos Controller
 *
 * @property Objetivo $Objetivo
 */
class ObjetivosController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Objetivo->recursive = 0;
		$objetivos = $this->paginate();
		$right_side_element = 'Help/Objetivos/index'; // Helpbox Right
		$this->set(compact('objetivos', 'right_side_element'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Objetivo->exists($id)) {
			throw new NotFoundException(__('Invalid objetivo'));
		}
		$options = array('conditions' => array('Objetivo.' . $this->Objetivo->primaryKey => $id));
		$objetivo = $this->Objetivo->find('first', $options);
		$this->set(compact('objetivo'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add($poa_id = null) {
		if ($this->request->is('post')) {
			$this->Objetivo->create();
			if ($this->Objetivo->save($this->request->data)) {
				$this->Session->setFlash(__('The objetivo has been saved'), 'success');
				if ($poa_id) {
					$this->redirect(array('controller' => 'poas', 'action' => 'edit', $poa_id));
				}
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The objetivo could not be saved. Please, try again.'), 'error');
			}
		}
		$options = ($poa_id) ? array('conditions' => array('Poa.id' => $poa_id)) : null;
		$poas = $this->Objetivo->Poa->find('list', $options);
		$this->set(compact('poas', 'poa_id'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Objetivo->exists($id)) {
			throw new NotFoundException(__('Invalid objetivo'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Objetivo->save($this->request->data)) {
				$this->Session->setFlash(__('The objetivo has been saved'), 'success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The objetivo could not be saved. Please, try again.'), 'error');
			}
		} else {
			$options = array('conditions' => array('Objetivo.' . $this->Objetivo->primaryKey => $id));
			$this->request->data = $this->Objetivo->find('first', $options);
		}
		$poas = $this->Objetivo->Poa->find('list');
		$this->set(compact('poas'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Objetivo->id = $id;
		if (!$this->Objetivo->exists()) {
			throw new NotFoundException(__('Invalid objetivo'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Objetivo->delete()) {
			$this->Session->setFlash(__('Objetivo deleted'), 'success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Objetivo was not deleted'), 'error');
		$this->redirect(array('action' => 'index'));
	}
}
