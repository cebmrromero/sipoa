<?php
App::uses('AppController', 'Controller');

/**
 * Administracion Controller
 *
 * @property Objetivo $Objetivo
 */
class AdministracionController extends AppController {
/**
 * Operaciones no tiene modelo
 */
	var $uses = array();

/**
 * index method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function index() {
		$this->Session->write('operacion', 'administracion');
		$modulos = array(
			array('nombre' => "Categoría de las Dependencias", 'src' => 'dependenciacategorias'),
			array('nombre' => "Dependencias", 'src' => 'dependencias'),
                        array('nombre' => "Objetivos", 'src' => 'objetivos'),
			array('nombre' => "Órganos", 'src' => 'organos'),
			array('nombre' => "Directrices del POA", 'src' => 'poacabeceras'),
			array('nombre' => "Trimestres", 'src' => 'trimestres'),
			// array('nombre' => "Grupos", 'src' => 'groups'),
			// array('nombre' => "Usuarios", 'src' => 'users'),
		);
		$this->set(compact('modulos'));
	}
}
