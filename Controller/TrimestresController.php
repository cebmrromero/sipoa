<?php
App::uses('AppController', 'Controller');
/**
 * Trimestres Controller
 *
 * @property Trimestre $Trimestre
 * @property PaginatorComponent $Paginator
 */
class TrimestresController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Trimestre->recursive = 0;
		$trimestres = $this->paginate();
		$this->set(compact('trimestres'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Trimestre->exists($id)) {
			throw new NotFoundException(__('Invalid trimestre'));
		}
		$options = array('conditions' => array('Trimestre.' . $this->Trimestre->primaryKey => $id));
		$trimestre = $this->Trimestre->find('first', $options);
		$this->set(compact('trimestre'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Trimestre->create();
			if ($this->Trimestre->save($this->request->data)) {
				$this->Session->setFlash(__('The trimestre has been saved'), 'success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The trimestre could not be saved. Please, try again.'), 'error');
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Trimestre->exists($id)) {
			throw new NotFoundException(__('Invalid trimestre'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Trimestre->save($this->request->data)) {
				$this->Session->setFlash(__('The trimestre has been saved'), 'success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The trimestre could not be saved. Please, try again.'), 'error');
			}
		} else {
			$options = array('conditions' => array('Trimestre.' . $this->Trimestre->primaryKey => $id));
			$this->request->data = $this->Trimestre->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Trimestre->id = $id;
		if (!$this->Trimestre->exists()) {
			throw new NotFoundException(__('Invalid trimestre'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Trimestre->delete()) {
			$this->Session->setFlash(__('Trimestre deleted'), 'success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Trimestre was not deleted'), 'error');
		$this->redirect(array('action' => 'index'));
	}}
