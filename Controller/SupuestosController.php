<?php
App::uses('AppController', 'Controller');
/**
 * Supuestos Controller
 *
 * @property Supuesto $Supuesto
 */
class SupuestosController extends AppController {

/**
 * add method
 *
 * @return void
 */
	public function add($medioverificacione_id) {
		$this->Supuesto->Medioverificacione->recursive = 3;
		$medioverificacione = $this->Supuesto->Medioverificacione->findById($medioverificacione_id);
		if ($this->request->is('post')) {
			$this->Supuesto->create();
			if ($this->Supuesto->save($this->request->data)) {
				$this->Session->setFlash(__('The supuesto has been saved'), 'success');
				// Si el boton continuar fue presionado
				if ($this->request->data['Supuesto']['continuar']) {
					$this->redirect(array('action' => 'add', $medioverificacione_id));
				} else {
					$this->redirect(array('controller' => 'planificacion', 'action' => 'unidadm_supuestos', $medioverificacione['Indicadore']['Meta']['poa_id'], $medioverificacione_id));
				}
			} else {
				$this->Session->setFlash(__('The supuesto could not be saved. Please, try again.'), 'error');
			}
		}
		$this->set(compact('medioverificacione'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null, $poa_id = null) {
		if (!$this->Supuesto->exists($id)) {
			throw new NotFoundException(__('Invalid supuesto'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Supuesto->save($this->request->data)) {
				$this->Session->setFlash(__('The supuesto has been saved'), 'success');
				$this->redirect(array('controller' => 'planificacion', 'action' => 'unidadm_supuestos', $poa_id, $this->request->data['Supuesto']['medioverificacione_id']));
			} else {
				$this->Session->setFlash(__('The supuesto could not be saved. Please, try again.'), 'error');
			}
		} else {
			$options = array('conditions' => array('Supuesto.' . $this->Supuesto->primaryKey => $id));
			$this->Supuesto->recursive = 4;
			$this->request->data = $this->Supuesto->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Supuesto->id = $id;
		if (!$this->Supuesto->exists()) {
			throw new NotFoundException(__('Invalid supuesto'));
		}
		$this->request->onlyAllow('get', 'delete');
		if ($this->Supuesto->delete()) {
			$this->Session->setFlash(__('Supuesto deleted'), 'success');
			$this->redirect($this->referer());
		}
		$this->Session->setFlash(__('Supuesto was not deleted'), 'error');
		$this->redirect($this->referer());
	}
}
