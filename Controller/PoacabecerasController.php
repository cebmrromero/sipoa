<?php
App::uses('AppController', 'Controller');
/**
 * Poacabeceras Controller
 *
 * @property Poacabecera $Poacabecera
 */
class PoacabecerasController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Poacabecera->recursive = 0;
		$poacabeceras = $this->paginate();
		$this->set(compact('poacabeceras'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Poacabecera->exists($id)) {
			throw new NotFoundException(__('Invalid poacabecera'));
		}
		$options = array('conditions' => array('Poacabecera.' . $this->Poacabecera->primaryKey => $id));
		$poacabecera = $this->Poacabecera->find('first', $options);
		$this->set(compact('poacabecera'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Poacabecera->create();
			if ($this->Poacabecera->save($this->request->data)) {
				$this->Session->setFlash(__('The poacabecera has been saved'), 'success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The poacabecera could not be saved. Please, try again.'), 'error');
			}
		}
		$parents = $this->Poacabecera->ParentPoacabecera->find('list');
		$this->set(compact('parents'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Poacabecera->exists($id)) {
			throw new NotFoundException(__('Invalid poacabecera'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Poacabecera->save($this->request->data)) {
				$this->Session->setFlash(__('The poacabecera has been saved'), 'success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The poacabecera could not be saved. Please, try again.'), 'error');
			}
		} else {
			$options = array('conditions' => array('Poacabecera.' . $this->Poacabecera->primaryKey => $id));
			$this->request->data = $this->Poacabecera->find('first', $options);
		}
		$parents = $this->Poacabecera->ParentPoacabecera->find('list');
		$this->set(compact('parents'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Poacabecera->id = $id;
		if (!$this->Poacabecera->exists()) {
			throw new NotFoundException(__('Invalid poacabecera'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Poacabecera->delete()) {
			$this->Session->setFlash(__('Poacabecera deleted'), 'success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Poacabecera was not deleted'), 'error');
		$this->redirect(array('action' => 'index'));
	}
}
