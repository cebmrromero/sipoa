<?php
App::uses('AppController', 'Controller');
/**
 * Ejecucionfisicas Controller
 *
 * @property Ejecucionfisica $Ejecucionfisica
 */
class EjecucionfisicasController extends AppController {

/**
 * add method
 *
 * @return void
 */
	public function add($unidadmedida_id) {
		$this->Ejecucionfisica->Unidadmedida->recursive = 4;
		$unidadmedida = $this->Ejecucionfisica->Unidadmedida->findById($unidadmedida_id);
		if ($this->request->is('post')) {
			$this->Ejecucionfisica->create();
			$error = false;
			foreach($this->request->data as $data) {
				if (!$this->Ejecucionfisica->saveAll($data)) {
					$error = true;
				}
				if (!$error) {
					$this->Session->setFlash(__('The ejecucionfisica has been saved'), 'success');
					$this->redirect(array('controller' => 'planificacion', 'action' => 'ejecucion_fisica', $unidadmedida['Medioverificacione']['Indicadore']['Meta']['poa_id'], $unidadmedida['Unidadmedida']['id']));
				} else {
					$this->Session->setFlash(__('The ejecucionfisica could not be saved. Please, try again.'), 'error');
					echo "error;" ; print_r($this->Ejecucionfisica->validationErrors);
				}
			}
		}
		$trimestres = $this->Ejecucionfisica->Trimestre->find('list');
		$this->set(compact('unidadmedida', 'trimestres'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Ejecucionfisica->id = $id;
		if (!$this->Ejecucionfisica->exists()) {
			throw new NotFoundException(__('Invalid ejecucionfisica'));
		}
		$ejecucionfisica = $this->Ejecucionfisica->findById($id);
		$ejecucionfisicas = $this->Ejecucionfisica->findAllByUnidadmedidaId($ejecucionfisica['Ejecucionfisica']['unidadmedida_id']);
		$this->request->onlyAllow('get', 'delete');
		$error = false;
		foreach($ejecucionfisicas as $ejecucionfisica) {
			if (!$this->Ejecucionfisica->delete($ejecucionfisica['Ejecucionfisica']['id'])) {
				$error = true;
			}
		}
		if (!$error) {
			$this->Session->setFlash(__('Ejecucionfisica deleted'), 'success');
			$this->redirect($this->referer());
		}
		$this->Session->setFlash(__('Ejecucionfisica was not deleted'), 'error');
		$this->redirect($this->referer());
	}
}
