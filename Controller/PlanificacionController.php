<?php
App::uses('AppController', 'Controller');
/**
 * Planificacion Controller
 *
 * @property Objetivo $Objetivo
 */
class PlanificacionController extends AppController {
/**
 * Operaciones no tiene modelo
 */
	var $uses = array('Poa');
	public $paginate = array(
		'order' => array(
			'Poa.status' => 'DESC',
			'Poa.dependencia_id' => 'ASC',
			'Poa.id' => 'DESC',
		),
		'limit' => 15
	);

/**
 * index method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function index() {
		$this->Session->write('operacion', 'planificacion');

		$dependencias = $this->Poa->Dependencia->find('all');
		// print_r($dependencias);die;
		
		//$this->Poa->recursive = 1;
		//$poas = $this->paginate('Poa', array(array('OR' => array('Poa.status = 1', 'Poa.status = 0'))));
		$right_side_element = 'Help/Planificacion/index'; // Helpbox Right
		$this->set(compact('poas', 'right_side_element', 'dependencias'));
	}

/**
 * principal method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function principal($id = null) {
		if (!$this->Poa->exists($id)) {
			throw new NotFoundException(__('Invalid poa'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Poa->save($this->request->data)) {
				$this->redirect(array('action' => 'metas', $id));
			}
		}
		$options = array('conditions' => array('Poa.' . $this->Poa->primaryKey => $id));
		$this->request->data = $this->Poa->find('first', $options);
		$poacabeceras = $this->Poa->Poacabecera->getPath($this->request->data['Poacabecera']['id']);
		$this->loadModel('Ejecucionfisica');
		$query = "
			SELECT
				Objetivo.*,
				Meta.*,
				Indicadore.*,
				Medioverificacione.*,
				Unidadmedida.*,
				Ejecucionfisica1.*,
				Ejecucionfisica2.*,
				Ejecucionfisica3.*,
				Ejecucionfisica4.*,
				Ejecucionevaluacione1.*,
				Ejecucionevaluacione2.*,
				Ejecucionevaluacione3.*,
				Ejecucionevaluacione4.*
				FROM 
				`ejecucionfisicas` as Ejecucionfisica1
				LEFT JOIN ejecucionfisicas as Ejecucionfisica2 on Ejecucionfisica2.trimestre_id = 2 and Ejecucionfisica1.unidadmedida_id = Ejecucionfisica2.unidadmedida_id
				LEFT JOIN ejecucionfisicas as Ejecucionfisica3 on Ejecucionfisica3.trimestre_id = 3 and Ejecucionfisica1.unidadmedida_id = Ejecucionfisica3.unidadmedida_id
				LEFT JOIN ejecucionfisicas as Ejecucionfisica4 on Ejecucionfisica4.trimestre_id = 4 and Ejecucionfisica1.unidadmedida_id = Ejecucionfisica4.unidadmedida_id
				LEFT JOIN unidadmedidas as Unidadmedida on Unidadmedida.id = Ejecucionfisica1.unidadmedida_id
				LEFT JOIN medioverificaciones as Medioverificacione on Medioverificacione.id = Unidadmedida.medioverificacione_id
				LEFT JOIN indicadores as Indicadore on Indicadore.id = Medioverificacione.indicadore_id
				LEFT JOIN metas as Meta on Meta.id = Indicadore.meta_id
				LEFT JOIN objetivos as Objetivo on Objetivo.id = Meta.objetivo_id
				LEFT JOIN poas as Poa on Poa.id = Meta.poa_id
				LEFT JOIN ejecucionevaluaciones as Ejecucionevaluacione1 on Ejecucionevaluacione1.unidadmedida_id = Unidadmedida.id and Ejecucionevaluacione1.trimestre_id = 1 AND Ejecucionfisica1.trimestre_id = Ejecucionevaluacione1.trimestre_pertenece_id
				LEFT JOIN ejecucionevaluaciones as Ejecucionevaluacione2 on Ejecucionevaluacione2.unidadmedida_id = Unidadmedida.id and Ejecucionevaluacione2.trimestre_id = 2 AND Ejecucionfisica2.trimestre_id = Ejecucionevaluacione2.trimestre_pertenece_id
				LEFT JOIN ejecucionevaluaciones as Ejecucionevaluacione3 on Ejecucionevaluacione3.unidadmedida_id = Unidadmedida.id and Ejecucionevaluacione3.trimestre_id = 3 AND Ejecucionfisica3.trimestre_id = Ejecucionevaluacione3.trimestre_pertenece_id
				LEFT JOIN ejecucionevaluaciones as Ejecucionevaluacione4 on Ejecucionevaluacione4.unidadmedida_id = Unidadmedida.id and Ejecucionevaluacione4.trimestre_id = 4 AND Ejecucionfisica4.trimestre_id = Ejecucionevaluacione4.trimestre_pertenece_id
				WHERE
				Ejecucionfisica1.trimestre_id = 1 AND Poa.id = %s
		";
		$query = sprintf($query, $id);
		$planificacion_trimestral = $this->Ejecucionfisica->query($query);
		$this->set(compact('poacabeceras', 'planificacion_trimestral'));
	}

/**
 * metas method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function metas($id = null) {
		if (!$this->Poa->exists($id)) {
			throw new NotFoundException(__('Invalid poa'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Poa->saveAll($this->request->data)) {
				$this->redirect(array('action' => 'indicadores', $id));
			} else {
				$this->Session->setFlash(__('The meta could not be saved. Please, try again.'), 'error');
			}
		}
		$options = array('conditions' => array('Poa.' . $this->Poa->primaryKey => $id));
		$this->Poa->recursive = 2;
		$this->request->data = $this->Poa->find('first', $options);
		$poacabeceras = $this->Poa->Poacabecera->getPath($this->request->data['Poacabecera']['id']);
		$this->set(compact('poacabeceras'));
	}

/**
 * indicadores method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function indicadores($id = null, $meta_id = null) {
		if (!$this->Poa->exists($id)) {
			throw new NotFoundException(__('Invalid poa'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Poa->saveAll($this->request->data)) {
				$this->redirect(array('action' => 'medioverificaciones', $id));
			} else {
				$this->Session->setFlash(__('The indicadore could not be saved. Please, try again.'), 'error');
			}
		}
		$options = array('conditions' => array('Poa.' . $this->Poa->primaryKey => $id));
		$this->request->data = $this->Poa->find('first', $options);
		if (!$meta_id) {
			$options = array('conditions' => array('Meta.poa_' . $this->Poa->primaryKey => $id));
			$metas = $this->Poa->Meta->find("all", $options);
		} else {
			$this->Poa->Meta->recursive = 1;
			$meta = $this->Poa->Meta->findById($meta_id);
			
		}
		$this->set(compact('metas', 'meta'));
	}

/**
 * medioverificaciones method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function medioverificaciones($id = null, $indicadore_id = null) {
		if (!$this->Poa->exists($id)) {
			throw new NotFoundException(__('Invalid poa'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Poa->saveAll($this->request->data)) {
				$this->redirect(array('action' => 'unidadm_supuestos', $id));
			} else {
				$this->Session->setFlash(__('The medioverificacione could not be saved. Please, try again.'), 'error');
			}
		}
		$options = array('conditions' => array('Poa.' . $this->Poa->primaryKey => $id));
		$this->request->data = $this->Poa->find('first', $options);
		if (!$indicadore_id) {
			$options = array('conditions' => array('Meta.poa_' . $this->Poa->primaryKey => $id));
			$this->Poa->Meta->recursive = 2;
			$metas = $this->Poa->Meta->find("all", $options);
		} else {
			$this->Poa->Meta->Indicadore->recursive = 2;
			$indicadore = $this->Poa->Meta->Indicadore->findById($indicadore_id);
		}
		$this->set(compact('metas', 'indicadore'));
	}

/**
 * unidadm_supuestos method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function unidadm_supuestos($id = null, $medioverificacione_id = null) {
		if (!$this->Poa->exists($id)) {
			throw new NotFoundException(__('Invalid poa'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Poa->saveAll($this->request->data)) {
				$this->redirect(array('action' => 'ejecucion_fisica', $id));
			} else {
				$this->Session->setFlash(__('The unidadm_supuestos could not be saved. Please, try again.'), 'error');
			}
		}
		$options = array('conditions' => array('Poa.' . $this->Poa->primaryKey => $id));
		$this->request->data = $this->Poa->find('first', $options);
		if (!$medioverificacione_id) {
			$options = array('conditions' => array('Meta.poa_' . $this->Poa->primaryKey => $id));
			$this->Poa->Meta->recursive = 3;
			$metas = $this->Poa->Meta->find("all", $options);
		} else {
			$this->Poa->Meta->Indicadore->Medioverificacione->recursive = 3;
			$medioverificacione = $this->Poa->Meta->Indicadore->Medioverificacione->findById($medioverificacione_id);
		}
		$this->set(compact('metas', 'medioverificacione'));
	}

/**
 * ejecucion_fisica method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function ejecucion_fisica($id = null, $unidadmedida_id = null) {
		if (!$this->Poa->exists($id)) {
			throw new NotFoundException(__('Invalid poa'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Poa->saveAll($this->request->data)) {
				$this->Session->setFlash(__('The planificacion has been completed'), 'success');
				$this->redirect(array('action' => 'metas', $id));
			} else {
				$this->Session->setFlash(__('The ejecucion_fisica could not be saved. Please, try again.'), 'error');
			}
		}
		$options = array('conditions' => array('Poa.' . $this->Poa->primaryKey => $id));
		$this->request->data = $this->Poa->find('first', $options);
		if (!$unidadmedida_id) {
			$options = array('conditions' => array('Meta.poa_' . $this->Poa->primaryKey => $id));
			$this->Poa->Meta->recursive = 4;
			$metas = $this->Poa->Meta->find("all", $options);
		} else {
			$this->Poa->Meta->Indicadore->Medioverificacione->Unidadmedida->recursive = 4;
			$unidadmedida = $this->Poa->Meta->Indicadore->Medioverificacione->Unidadmedida->findById($unidadmedida_id);
		}
		if (!$unidadmedida['Ejecucionfisica']) {
			if ($this->Poa->hasMetaAnEjecucionfisica($id, $unidadmedida['Medioverificacione']['Indicadore']['meta_id'])) {
				$this->Session->setFlash(__('Ya la meta posee una ejecucion fisica, no pede ingresar otra.'), 'error');
				$this->redirect(array('action' => 'metas', $id));
			}
		}
		$this->set(compact('metas', 'unidadmedida'));
	}
}