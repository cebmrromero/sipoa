<?php
App::uses('AppController', 'Controller');
/**
 * Unidadmedidas Controller
 *
 * @property Unidadmedida $Unidadmedida
 */
class UnidadmedidasController extends AppController {

/**
 * add method
 *
 * @return void
 */
	public function add($medioverificacione_id) {
		$this->Unidadmedida->Medioverificacione->recursive = 3;
		$medioverificacione = $this->Unidadmedida->Medioverificacione->findById($medioverificacione_id);
		if ($this->request->is('post')) {
			$this->Unidadmedida->create();
			if ($this->Unidadmedida->save($this->request->data)) {
				$this->Session->setFlash(__('The unidadmedida has been saved'), 'success');
				$this->redirect(array('controller' => 'planificacion', 'action' => 'unidadm_supuestos', $medioverificacione['Indicadore']['Meta']['poa_id'], $medioverificacione_id));
				
			} else {
				$this->Session->setFlash(__('The unidadmedida could not be saved. Please, try again.'), 'error');
			}
		}
		$this->set(compact('medioverificacione'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null, $poa_id = null) {
		if (!$this->Unidadmedida->exists($id)) {
			throw new NotFoundException(__('Invalid unidadmedida'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Unidadmedida->save($this->request->data)) {
				$this->Session->setFlash(__('The unidadmedida has been saved'), 'success');
				$this->redirect(array('controller' => 'planificacion', 'action' => 'unidadm_supuestos', $poa_id, $this->request->data['Unidadmedida']['medioverificacione_id']));
			} else {
				$this->Session->setFlash(__('The unidadmedida could not be saved. Please, try again.'), 'error');
			}
		} else {
			$options = array('conditions' => array('Unidadmedida.' . $this->Unidadmedida->primaryKey => $id));
			$this->Unidadmedida->recursive = 4;
			$this->request->data = $this->Unidadmedida->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Unidadmedida->id = $id;
		if (!$this->Unidadmedida->exists()) {
			throw new NotFoundException(__('Invalid unidadmedida'));
		}
		$this->request->onlyAllow('get', 'delete');
		if ($this->Unidadmedida->delete()) {
			$this->Session->setFlash(__('Unidadmedida deleted'), 'success');
			$this->redirect($this->referer());
		}
		$this->Session->setFlash(__('Unidadmedida was not deleted'), 'error');
		$this->redirect($this->referer());
	}
}
