<?php
App::uses('AppController', 'Controller');

/**
 * Poas Controller
 *
 * @property Poa $Poa
 */
class PoasController extends AppController {
	//public $helpers = array('Cakepdf.Pdf');
	//public $components = array('Search.Prg', 'Cakepdf.Pdf', 'RequestHandler');
	public $components = array('Search.Prg', 'RequestHandler');
    public $presetVars = true; // using the model configuration
	public $paginate = array(
		'order' => array(
			'Poa.status' => 'DESC',
			'Poa.dependencia_id' => 'ASC',
			'Poa.id' => 'DESC',
		),
		'limit' => 15
	);
	
	public function beforeFilter() {
		parent::beforeFilter();
		// For CakePHP 2.1 and up
		$this->Auth->allow('reporte1_json');
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Session->write('operacion', 'configuracion');
		$dependencias = $this->Poa->Dependencia->find('all');
		$right_side_element = 'Help/Poa/index'; // Helpbox Right
		$this->set(compact( 'right_side_element', 'dependencias'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Poa->exists($id)) {
			throw new NotFoundException(__('Invalid poa'));
		}
		$options = array('conditions' => array('Poa.' . $this->Poa->primaryKey => $id));
		$poa = $this->Poa->find('first', $options);
		$right_side_element = 'Help/Poa/view'; // Helpbox Right
		$this->set(compact( 'right_side_element', 'poa'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Poa->create();
			$sql = 'SELECT id FROM poas as Poa WHERE Poa.ano = %s AND Poa.dependencia_id = %s AND Poa.status IN (0,1)';
			$sql = sprintf($sql, date('Y'), $this->request->data['Poa']['dependencia_id']);
			$poas = $this->Poa->query($sql);
			$count = sizeof($poas);
			if ($count) {
				$this->request->data['Poa']['es_reprogramado'] = 1;
				$this->request->data['Poa']['numero_reprogramacion'] = $count;
			}
			if ($this->Poa->save($this->request->data)) {
				$sql = 'UPDATE poas SET status = 0 WHERE ano = %s AND dependencia_id = %s AND status IN (0,1) AND NOT id = %s';
				$sql = sprintf($sql, date('Y'), $this->request->data['Poa']['dependencia_id'], $this->Poa->getLastInsertID());
				$this->Poa->query($sql);
				$this->Session->setFlash(__('The poa has been saved'), 'success');
				$this->redirect(array('action' => 'edit', $this->Poa->getLastInsertID()));
			} else {
				$this->Session->setFlash(__('The poa could not be saved. Please, try again.'), 'error');
			}
		}
		$dependencias = $this->Poa->Dependencia->find('list');
		$options = array('conditions' => array('Poacabecera.seleccionable' => '1', 'Poacabecera.activo' => '1'));
		$poacabecera = $this->Poa->Poacabecera->find('first', $options);
		$poacabeceras = $this->Poa->Poacabecera->getPath($poacabecera['Poacabecera']['id']);
		$organos = $this->Poa->Organo->find('first');
		$objetivos = $this->Poa->Objetivo->find('list');
		
		$right_side_element = 'Help/Poa/add'; // Helpbox Right
		$this->set(compact( 'right_side_element', 'dependencias', 'poacabeceras', 'organos', 'objetivos'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Poa->exists($id)) {
			throw new NotFoundException(__('Invalid poa'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if (!is_array($this->request->data['Objetivo']['ObjetivoAnteriore'])) {
				$this->request->data['Objetivo']['ObjetivoAnteriore'] = array();
			}
			if (!is_array($this->request->data['Objetivo']['Objetivo'])) {
				$this->request->data['Objetivo']['Objetivo'] = array();
			}
			$this->request->data['Objetivo']['Objetivo'] = array_merge($this->request->data['Objetivo']['Objetivo'], $this->request->data['Objetivo']['ObjetivoAnteriore']);
			if ($this->Poa->save($this->request->data)) {
				$this->Session->setFlash(__('The poa has been saved'), 'success');
				$this->redirect(array('controller' => 'planificacion', 'action' => 'principal', $id));
			} else {
				$this->Session->setFlash(__('The poa could not be saved. Please, try again.'), 'error');
			}
		} else {
			$options = array('conditions' => array('Poa.' . $this->Poa->primaryKey => $id));
			$this->request->data = $this->Poa->find('first', $options);
		}
		$dependencias = $this->Poa->Dependencia->find('list');
		$options = array('conditions' => array('Poacabecera.id' => $this->request->data['Poa']['poacabecera_id']));
		$poacabecera = $this->Poa->Poacabecera->find('first', $options);
		$poacabeceras = $this->Poa->Poacabecera->getPath($poacabecera['Poacabecera']['id']);
		$organos = $this->Poa->Organo->find('first');
		$objs_ids = $this->Poa->query(sprintf("
			SELECT DISTINCT ObjetivosPoa.objetivo_id FROM objetivos_poas AS ObjetivosPoa
			LEFT JOIN poas as Poa on Poa.id = ObjetivosPoa.poa_id
			WHERE Poa.dependencia_id = %s AND YEAR(ObjetivosPoa.created) = %s", $this->request->data['Dependencia']['id'], date('Y') - 1));
		$objs_ids = Hash::extract($objs_ids, '{n}.{s}.objetivo_id');

		$objetivos = $this->Poa->Objetivo->find('list', array('conditions' => array('NOT' => array('Objetivo.id' => $objs_ids))));
		$objetivos_anteriores = $this->Poa->Objetivo->find('list', array('conditions' => array('Objetivo.id' => $objs_ids)));
		$right_side_element = 'Help/Poa/edit'; // Helpbox Right
		$this->set(compact( 'right_side_element', 'dependencias', 'poacabeceras', 'organos', 'objetivos', 'objetivos_anteriores'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Poa->id = $id;
		if (!$this->Poa->exists()) {
			throw new NotFoundException(__('Invalid poa'));
		}
		$poa = $this->Poa->findById($id);
		$poa['Poa']['status'] = 3;
		$this->request->onlyAllow('post', 'delete');
		if ($this->Poa->save($poa)) {
			$this->Session->setFlash(__('Poa deleted'), 'success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Poa was not deleted'), 'error');
		$this->redirect(array('action' => 'index'));
	}
}
