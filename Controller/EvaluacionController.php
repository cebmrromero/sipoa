<?php
App::uses('AppController', 'Controller');
/**
 * Evaluacion Controller
 *
 * @property Objetivo $Objetivo
 */
class EvaluacionController extends AppController {
/**
 * Operaciones no tiene modelo
 */
	var $uses = array('Poa');
	public $paginate = array(
		'order' => array(
			'Poa.status' => 'DESC',
			'Poa.dependencia_id' => 'ASC',
			'Poa.id' => 'DESC',
		),
		'limit' => 15
	);

/**
 * index method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function index() {
		$this->Session->write('operacion', 'evaluacion');

		$dependencias = $this->Poa->Dependencia->find('all');
		
		$this->Poa->recursive = 1;
		$poas = $this->paginate('Poa', array(array('OR' => array('Poa.status = 1', 'Poa.status = 0'))));
		$this->paginate['order'] = 'Poa.dependencia_id';
		$this->set(compact('poas', 'dependencias'));
	}

/**
 * principal method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function principal($id = null) {
		if (!$this->Poa->exists($id)) {
			throw new NotFoundException(__('Invalid poa'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Poa->save($this->request->data)) {
				$this->redirect(array('action' => 'metas', $id));
			}
		}
		$this->loadModel("Ejecucionfisica");
		$this->loadModel('Trimestre');
		$options = array('conditions' => array('Poa.' . $this->Poa->primaryKey => $id));
		$this->request->data = $this->Poa->find('first', $options);
		$poacabeceras = $this->Poa->Poacabecera->getPath($this->request->data['Poacabecera']['id']);
		$trimestres = $this->Trimestre->find("all");
		$ejecucion_trimestral = $this->Ejecucionfisica->getEjecucionFisicaByPoaId($id);
		$this->set(compact('poacabeceras', 'trimestres', 'ejecucion_trimestral'));
	}

/**
 * ejecucion_fisica method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function ejecucion_fisica($id = null, $trimestre_id = null) {
		// TODO: Si la ejecucion fisica (en realidad la ejecucionevaluacione) tiene productometas registrados, no se debe poder cambiar la cantidad
		if (!$this->Poa->exists($id)) {
			throw new NotFoundException(__('Invalid poa'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Poa->saveAll($this->request->data)) {
				$this->Session->setFlash(__('The evaluacion has been completed'), 'success');
				$this->redirect(array('action' => 'metas', $id));
			} else {
				$this->Session->setFlash(__('The evaluacion could not be saved. Please, try again.'), 'error');
			}
		}
		$options = array('conditions' => array('Poa.' . $this->Poa->primaryKey => $id));
		$this->request->data = $this->Poa->find('first', $options);
		
		$this->loadModel('Trimestre');
		
		$this->loadModel('Unidadmedida');
		$conditions = array(
			'conditions' => array(
				"Ejecucionfisica.trimestre_id = $trimestre_id",
				'Ejecucionfisica.cantidad_planificada > 0',
				'Meta.poa_id' => $id
			),
			'joins' => array(
				array(
					'alias' => 'Medioverificacione',
					'table' => 'medioverificaciones',
					'type' => 'LEFT',
					'conditions' => 'Medioverificacione.id = Unidadmedida.medioverificacione_id'
				),
				array(
					'alias' => 'Indicadore',
					'table' => 'indicadores',
					'type' => 'LEFT',
					'conditions' => 'Indicadore.id = Medioverificacione.indicadore_id'
				),
				array(
					'alias' => 'Meta',
					'table' => 'metas',
					'type' => 'LEFT',
					'conditions' => 'Meta.id = Indicadore.meta_id'
				),
				array(
					'alias' => 'Ejecucionfisica',
					'table' => 'ejecucionfisicas',
					'type' => 'LEFT',
					'conditions' => 'Ejecucionfisica.unidadmedida_id = Unidadmedida.id'
				),
				array(
					'alias' => 'Ejecucionevaluacione',
					'table' => 'ejecucionevaluaciones',
					'type' => 'LEFT',
					'conditions' => 'Ejecucionevaluacione.unidadmedida_id = Unidadmedida.id AND Ejecucionevaluacione.trimestre_id = Ejecucionfisica.trimestre_id AND Ejecucionfisica.trimestre_id = Ejecucionevaluacione.trimestre_pertenece_id'
				),
				array(
					'alias' => 'Metapendiente',
					'table' => 'metapendientes',
					'type' => 'LEFT',
					'conditions' => '(Metapendiente.unidadmedida_id = Unidadmedida.id) AND (Metapendiente.trimestre_id = Ejecucionfisica.trimestre_id) AND (Metapendiente.trimestre_reporte_id = Ejecucionfisica.trimestre_id)'
				),
				array(
					'alias' => 'Objetivo',
					'table' => 'objetivos',
					'type' => 'LEFT',
					'conditions' => 'Objetivo.id = Meta.objetivo_id'
				),
				array(
					'alias' => 'Trimestre',
					'table' => 'trimestres',
					'type' => 'LEFT',
					'conditions' => 'Trimestre.id = Ejecucionfisica.trimestre_id'
				),
			),
			'fields' => array(
				"Ejecucionfisica.*",
				'Unidadmedida.*',
				'Medioverificacione.*',
				'Indicadore.*',
				'Meta.*',
				'Objetivo.*',
				'Ejecucionevaluacione.*',
				'Trimestre.*',
			),
			'order' => 'Objetivo.id, Ejecucionfisica.trimestre_id DESC, Ejecucionevaluacione.trimestre_pertenece_id, Meta.id, Indicadore.id, Medioverificacione.id, Unidadmedida.id',
		);
		$this->Unidadmedida->recursive = -1;
		$ejecucionfisicas = $this->Unidadmedida->find("all", $conditions);
		//print_r($ejecucionfisicas);die;
		//
		$conditions = array(
			'conditions' => array(
				"Metapendiente.trimestre_ejecucion_id = $trimestre_id",
				'Meta.poa_id' => $id
			),
			'joins' => array(
				array(
					'alias' => 'Medioverificacione',
					'table' => 'medioverificaciones',
					'type' => 'LEFT',
					'conditions' => 'Medioverificacione.id = Unidadmedida.medioverificacione_id'
				),
				array(
					'alias' => 'Indicadore',
					'table' => 'indicadores',
					'type' => 'LEFT',
					'conditions' => 'Indicadore.id = Medioverificacione.indicadore_id'
				),
				array(
					'alias' => 'Meta',
					'table' => 'metas',
					'type' => 'LEFT',
					'conditions' => 'Meta.id = Indicadore.meta_id'
				),
				array(
					'alias' => 'Objetivo',
					'table' => 'objetivos',
					'type' => 'LEFT',
					'conditions' => 'Objetivo.id = Meta.objetivo_id'
				),
				array(
					'alias' => 'Ejecucionfisica',
					'table' => 'ejecucionfisicas',
					'type' => 'LEFT',
					'conditions' => 'Ejecucionfisica.unidadmedida_id = Unidadmedida.id'
				),
				array(
					'alias' => 'Metapendiente',
					'table' => 'metapendientes',
					'type' => 'LEFT',
					'conditions' => 'Metapendiente.unidadmedida_id = Unidadmedida.id AND Metapendiente.trimestre_id = Ejecucionfisica.trimestre_id'
				),
				array(
					'alias' => 'Ejecucionfisica2',
					'table' => 'ejecucionfisicas',
					'type' => 'LEFT',
					'conditions' => 'Ejecucionfisica2.unidadmedida_id = Unidadmedida.id AND Ejecucionfisica2.trimestre_id = Metapendiente.trimestre_ejecucion_id'
				),
				array(
					'alias' => 'Ejecucionevaluacione2',
					'table' => 'ejecucionevaluaciones',
					'type' => 'LEFT',
					'conditions' => 'Ejecucionevaluacione2.unidadmedida_id = Unidadmedida.id AND Ejecucionevaluacione2.trimestre_id = Metapendiente.trimestre_id'
				),
				array(
					'alias' => 'Ejecucionevaluacione',
					'table' => 'ejecucionevaluaciones',
					'type' => 'LEFT',
					'conditions' => 'Ejecucionevaluacione.unidadmedida_id = Unidadmedida.id AND Ejecucionevaluacione.trimestre_pertenece_id = Metapendiente.trimestre_id AND Ejecucionevaluacione.trimestre_id = Metapendiente.trimestre_ejecucion_id'
					//'conditions' => 'Ejecucionevaluacione.unidadmedida_id = Unidadmedida.id AND Ejecucionevaluacione.trimestre_pertenece_id = Metapendiente.trimestre_id'
				),
				array(
					'alias' => 'Trimestre',
					'table' => 'trimestres',
					'type' => 'LEFT',
					'conditions' => 'Trimestre.id = Ejecucionfisica.trimestre_id'
				),
			),
			'fields' => array(
				'Ejecucionfisica.cantidad_planificada - SUM(Ejecucionevaluacione.cantidad_ejecutada) AS Ejecucionfisica__pendiente',
				'SUM(Ejecucionevaluacione.cantidad_ejecutada) - Ejecucionevaluacione.cantidad_ejecutada AS Ejecucionfisica__ejecutado',
				"Ejecucionfisica.*",
				"Ejecucionfisica2.*",
				'Unidadmedida.*',
				'Medioverificacione.*',
				'Indicadore.*',
				'Meta.*',
				'Objetivo.*',
				'Ejecucionevaluacione.*',
				'Ejecucionevaluacione2.*',
				'Trimestre.*',
				'Metapendiente.*'
			),
			//'group' => 'Unidadmedida.id HAVING (Ejecucionfisica.cantidad_planificada - SUM(Ejecucionevaluacione.cantidad_ejecutada)) > 0',
			'group' => 'Unidadmedida.id, Ejecucionfisica.trimestre_id',
			'order' => 'Objetivo.id, Ejecucionfisica.trimestre_id ASC, Meta.id, Indicadore.id, Medioverificacione.id, Unidadmedida.id',
		);
		$this->Unidadmedida->Ejecucionfisica->virtualFields['pendiente'] = 0;
		$this->Unidadmedida->Ejecucionfisica->virtualFields['ejecutado'] = 0;
		$this->Unidadmedida->recursive = -1;
		$metapendientes = $this->Unidadmedida->find("all", $conditions);
		//print_r($metapendientes);//die;
		
		$this->Trimestre->recursive = -1;
		$trimestre = $this->Trimestre->findById($trimestre_id);
		$this->set(compact('ejecucionfisicas', 'trimestre', 'metapendientes'));
	}
	
	public function metas_pendientes($id = null, $trimestre_id = null) {
		if (!$this->Poa->exists($id)) {
			throw new NotFoundException(__('Invalid poa'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Poa->saveAll($this->request->data)) {
				$this->Session->setFlash(__('The metas_pendientes has been completed'), 'success');
				$this->redirect(array('action' => 'metas', $id));
			} else {
				$this->Session->setFlash(__('The metas_pendientes could not be saved. Please, try again.'), 'error');
			}
		}
		$options = array('conditions' => array('Poa.' . $this->Poa->primaryKey => $id));
		$this->request->data = $this->Poa->find('first', $options);
		
		$this->loadModel('Unidadmedida');
		$this->loadModel('Trimestre');
		$this->loadModel('Unidadmedida');
		$conditions = array(
			'conditions' => array(
				"Ejecucionfisica.trimestre_id = $trimestre_id",
				'(Ejecucionfisica.cantidad_planificada - Ejecucionevaluacione.cantidad_ejecutada) > 0',
				'Meta.poa_id' => $id
			),
			'joins' => array(
				array(
					'alias' => 'Medioverificacione',
					'table' => 'medioverificaciones',
					'type' => 'LEFT',
					'conditions' => 'Medioverificacione.id = Unidadmedida.medioverificacione_id'
				),
				array(
					'alias' => 'Indicadore',
					'table' => 'indicadores',
					'type' => 'LEFT',
					'conditions' => 'Indicadore.id = Medioverificacione.indicadore_id'
				),
				array(
					'alias' => 'Meta',
					'table' => 'metas',
					'type' => 'LEFT',
					'conditions' => 'Meta.id = Indicadore.meta_id'
				),
				array(
					'alias' => 'Ejecucionfisica',
					'table' => 'ejecucionfisicas',
					'type' => 'LEFT',
					'conditions' => 'Ejecucionfisica.unidadmedida_id = Unidadmedida.id'
				),
				array(
					'alias' => 'Ejecucionevaluacione',
					'table' => 'ejecucionevaluaciones',
					'type' => 'LEFT',
					'conditions' => 'Ejecucionevaluacione.unidadmedida_id = Unidadmedida.id AND Ejecucionevaluacione.trimestre_id = Ejecucionfisica.trimestre_id AND Ejecucionfisica.trimestre_id = Ejecucionevaluacione.trimestre_pertenece_id'
				),
				array(
					'alias' => 'Metapendiente',
					'table' => 'metapendientes',
					'type' => 'LEFT',
					'conditions' => '(Metapendiente.unidadmedida_id = Unidadmedida.id) AND (Metapendiente.trimestre_id = Ejecucionfisica.trimestre_id) AND (Metapendiente.trimestre_reporte_id = Ejecucionfisica.trimestre_id)'
				),
				array(
					'alias' => 'Objetivo',
					'table' => 'objetivos',
					'type' => 'LEFT',
					'conditions' => 'Objetivo.id = Meta.objetivo_id'
				),
				array(
					'alias' => 'Trimestre',
					'table' => 'trimestres',
					'type' => 'LEFT',
					'conditions' => 'Trimestre.id = Ejecucionfisica.trimestre_id'
				),
			),
			'fields' => array(
				'(Ejecucionfisica.cantidad_planificada - Ejecucionevaluacione.cantidad_ejecutada) as Ejecucionfisica__cantidad_por_alcanzar',
				"Ejecucionfisica.*",
				'Unidadmedida.*',
				'Medioverificacione.*',
				'Indicadore.*',
				'Meta.*',
				'Objetivo.*',
				'Ejecucionevaluacione.*',
				'Trimestre.*',
				'Metapendiente.*'
			),
			'order' => 'Objetivo.id, Ejecucionfisica.trimestre_id ASC, Ejecucionevaluacione.trimestre_pertenece_id, Meta.id, Indicadore.id, Medioverificacione.id, Unidadmedida.id',
		);
		$this->Unidadmedida->Ejecucionfisica->virtualFields['cantidad_por_alcanzar'] = 0;
		$this->Unidadmedida->recursive = -1;
		$ejecucionfisicas = $this->Unidadmedida->find("all", $conditions);
		
		$conditions = array(
			'conditions' => array(
				"Metapendiente.trimestre_ejecucion_id = $trimestre_id",
				'((Ejecucionfisica.cantidad_planificada - Ejecucionevaluacione2.cantidad_ejecutada) - Ejecucionevaluacione.cantidad_ejecutada) > 0',
				'(Metapendiente.cantidad_pendiente - Ejecucionevaluacione.cantidad_ejecutada) > 0',
				'Meta.poa_id' => $id
			),
			'joins' => array(
				array(
					'alias' => 'Metapendiente',
					'table' => 'metapendientes',
					'type' => 'LEFT',
					'conditions' => 'Metapendiente.unidadmedida_id = Unidadmedida.id'
				),
				array(
					'alias' => 'Metapendiente2',
					'table' => 'metapendientes',
					'type' => 'LEFT',
					'conditions' => 'Metapendiente2.unidadmedida_id = Unidadmedida.id AND Metapendiente.trimestre_ejecucion_id = Metapendiente2.trimestre_reporte_id AND Metapendiente2.trimestre_id = Metapendiente.trimestre_id'
				),
				array(
					'alias' => 'Ejecucionevaluacione',
					'table' => 'ejecucionevaluaciones',
					'type' => 'LEFT',
					'conditions' => 'Ejecucionevaluacione.trimestre_id = Metapendiente.trimestre_ejecucion_id AND Metapendiente.trimestre_id = Ejecucionevaluacione.trimestre_pertenece_id AND Unidadmedida.id = Ejecucionevaluacione.unidadmedida_id'
				),
				array(
					'alias' => 'Ejecucionfisica',
					'table' => 'ejecucionfisicas',
					'type' => 'LEFT',
					'conditions' => 'Ejecucionfisica.unidadmedida_id = Unidadmedida.id  AND Ejecucionfisica.trimestre_id = Metapendiente.trimestre_id'
				),
				array(
					'alias' => 'Ejecucionevaluacione2',
					'table' => 'ejecucionevaluaciones',
					'type' => 'LEFT',
					'conditions' => 'Ejecucionevaluacione2.trimestre_id = Metapendiente.trimestre_id AND Metapendiente.trimestre_id = Ejecucionevaluacione2.trimestre_pertenece_id AND Unidadmedida.id = Ejecucionevaluacione2.unidadmedida_id'
				),
				array(
					'alias' => 'Medioverificacione',
					'table' => 'medioverificaciones',
					'type' => 'LEFT',
					'conditions' => 'Medioverificacione.id = Unidadmedida.medioverificacione_id'
				),
				array(
					'alias' => 'Indicadore',
					'table' => 'indicadores',
					'type' => 'LEFT',
					'conditions' => 'Indicadore.id = Medioverificacione.indicadore_id'
				),
				array(
					'alias' => 'Meta',
					'table' => 'metas',
					'type' => 'LEFT',
					'conditions' => 'Meta.id = Indicadore.meta_id'
				),
				array(
					'alias' => 'Objetivo',
					'table' => 'objetivos',
					'type' => 'LEFT',
					'conditions' => 'Objetivo.id = Meta.objetivo_id'
				),
				array(
					'alias' => 'Trimestre',
					'table' => 'trimestres',
					'type' => 'LEFT',
					'conditions' => 'Trimestre.id = Metapendiente.trimestre_id'
				),
			),
			'fields' => array(
				'(Ejecucionfisica.cantidad_planificada - Ejecucionevaluacione2.cantidad_ejecutada) - Ejecucionevaluacione.cantidad_ejecutada as Ejecucionfisica__cantidad_por_alcanzar',
				"Ejecucionfisica.*",
				'Unidadmedida.*',
				//'Medioverificacione.*',
				//'Indicadore.*',
				'Meta.*',
				'Objetivo.*',
				'Ejecucionevaluacione.*',
				'Trimestre.*',
				'Metapendiente.*',
				'Metapendiente2.*'
			),
			'order' => 'Objetivo.id, Ejecucionfisica.trimestre_id ASC, Ejecucionevaluacione.trimestre_pertenece_id, Meta.id, Indicadore.id, Medioverificacione.id, Unidadmedida.id',
		);
		$this->Unidadmedida->Ejecucionfisica->virtualFields['cantidad_por_alcanzar'] = 0;
		$this->Unidadmedida->recursive = -1;
		$metapendientes = $this->Unidadmedida->find("all", $conditions);
		//print_r($metapendientes);
		
		$this->Trimestre->recursive = -1;
		$trimestre = $this->Trimestre->findById($trimestre_id);
		$trimestres = $this->Trimestre->find("list", array('conditions' => array("Trimestre.id > {$trimestre['Trimestre']['id']}")));
		$this->set(compact('ejecucionfisicas', 'trimestre', 'trimestres', 'metapendientes'));
	}
	
	public function producto_metas($id = null, $trimestre_id = null) {
		if (!$this->Poa->exists($id)) {
			throw new NotFoundException(__('Invalid poa'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Poa->saveAll($this->request->data)) {
				$this->Session->setFlash(__('The evaluacion has been completed'), 'success');
				$this->redirect(array('action' => 'metas', $id));
			} else {
				$this->Session->setFlash(__('The evaluacion could not be saved. Please, try again.'), 'error');
			}
		}
		$options = array('conditions' => array('Poa.' . $this->Poa->primaryKey => $id));
		$this->request->data = $this->Poa->find('first', $options);
		
		$this->loadModel('Trimestre');
		
		$this->loadModel('Unidadmedida');
		$conditions = array(
			'conditions' => array(
				'Ejecucionfisica.trimestre_id' => $trimestre_id,
				'Meta.poa_id' => $id,
				'Ejecucionfisica.cantidad_planificada >= 0',
				'Ejecucionevaluacione.cantidad_ejecutada > 0',
			),
			'joins' => array(
				array(
					'alias' => 'Medioverificacione',
					'table' => 'medioverificaciones',
					'type' => 'LEFT',
					'conditions' => 'Medioverificacione.id = Unidadmedida.medioverificacione_id'
				),
				array(
					'alias' => 'Indicadore',
					'table' => 'indicadores',
					'type' => 'LEFT',
					'conditions' => 'Indicadore.id = Medioverificacione.indicadore_id'
				),
				array(
					'alias' => 'Meta',
					'table' => 'metas',
					'type' => 'LEFT',
					'conditions' => 'Meta.id = Indicadore.meta_id'
				),
				array(
					'alias' => 'Objetivo',
					'table' => 'objetivos',
					'type' => 'LEFT',
					'conditions' => 'Objetivo.id = Meta.objetivo_id'
				),
				array(
					'alias' => 'Ejecucionfisica',
					'table' => 'ejecucionfisicas',
					'type' => 'LEFT',
					'conditions' => 'Ejecucionfisica.unidadmedida_id = Unidadmedida.id'
				),
				array(
					'alias' => 'Ejecucionevaluacione',
					'table' => 'ejecucionevaluaciones',
					'type' => 'LEFT',
					'conditions' => 'Ejecucionevaluacione.unidadmedida_id = Unidadmedida.id AND Ejecucionevaluacione.trimestre_id = Ejecucionfisica.trimestre_id AND Ejecucionfisica.trimestre_id = Ejecucionevaluacione.trimestre_id'
				),
				array(
					'alias' => 'Trimestre',
					'table' => 'trimestres',
					'type' => 'LEFT',
					'conditions' => 'Ejecucionevaluacione.trimestre_pertenece_id = Trimestre.id'
				),
			),
			'fields' => array(
				'Ejecucionfisica.*',
				'Unidadmedida.*',
				'Medioverificacione.*',
				'Indicadore.*',
				'Meta.*',
				'Objetivo.*',
				'Ejecucionevaluacione.*',
				'Trimestre.*'
			),
			'order' => 'Ejecucionevaluacione.trimestre_pertenece_id ASC, Objetivo.id, Meta.id, Indicadore.id, Medioverificacione.id, Unidadmedida.id, Ejecucionfisica.id'
		);
		$this->Unidadmedida->recursive = -1;
		$ejecucionfisicas = $this->Unidadmedida->find("all", $conditions);
		$i = 0;
		foreach ($ejecucionfisicas as $ejecucionfisica) {
			$this->Unidadmedida->Ejecucionevaluacione->Productometa->recursive = -1;
			$productometas = $this->Unidadmedida->Ejecucionevaluacione->Productometa->findAllByEjecucionevaluacioneId($ejecucionfisica['Ejecucionevaluacione']['id']);
			$ejecucionfisicas[$i]['Productometa'] = $productometas;
			$i++;
		}
		$trimestre = $this->Trimestre->findById($trimestre_id);
		$trimestres = $this->Trimestre->find("list", array('conditions' => array("Trimestre.id <= {$trimestre['Trimestre']['id']}")));
		$this->set(compact('ejecucionfisicas', 'trimestre', 'trimestres'));
	}
	
	public function indicadores($id = null, $trimestre_id = null) {
		if (!$this->Poa->exists($id)) {
			throw new NotFoundException(__('Invalid poa'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Poa->saveAll($this->request->data)) {
				$this->Session->setFlash(__('The evaluacion has been completed'), 'success');
				$this->redirect(array('action' => 'metas', $id));
			} else {
				$this->Session->setFlash(__('The evaluacion could not be saved. Please, try again.'), 'error');
			}
		}
		$options = array('conditions' => array('Poa.' . $this->Poa->primaryKey => $id));
		$this->request->data = $this->Poa->find('first', $options);
		
		$this->loadModel('Trimestre');
		
		$this->loadModel('Unidadmedida');
		$conditions = array(
			'conditions' => array(
				'Ejecucionfisica.trimestre_id' => $trimestre_id,
				'Meta.poa_id' => $id,
				'Ejecucionfisica.cantidad_planificada > 0'
			),
			'joins' => array(
				array(
					'alias' => 'Ejecucionfisica',
					'table' => 'ejecucionfisicas',
					'type' => 'LEFT',
					'conditions' => 'Ejecucionfisica.unidadmedida_id = Unidadmedida.id'
				),
				array(
					'alias' => 'Medioverificacione',
					'table' => 'medioverificaciones',
					'type' => 'LEFT',
					'conditions' => 'Medioverificacione.id = Unidadmedida.medioverificacione_id'
				),
				array(
					'alias' => 'Indicadore',
					'table' => 'indicadores',
					'type' => 'LEFT',
					'conditions' => 'Indicadore.id = Medioverificacione.indicadore_id'
				),
				array(
					'alias' => 'Meta',
					'table' => 'metas',
					'type' => 'LEFT',
					'conditions' => 'Meta.id = Indicadore.meta_id'
				),
				array(
					'alias' => 'Objetivo',
					'table' => 'objetivos',
					'type' => 'LEFT',
					'conditions' => 'Objetivo.id = Meta.objetivo_id'
				),
				array(
					'alias' => 'Ejecucionevaluacione',
					'table' => 'ejecucionevaluaciones',
					'type' => 'LEFT',
					'conditions' => 'Ejecucionevaluacione.unidadmedida_id = Unidadmedida.id AND Ejecucionevaluacione.trimestre_id = Ejecucionfisica.trimestre_id AND Ejecucionevaluacione.trimestre_id = Ejecucionevaluacione.trimestre_pertenece_id'
				),
				array(
					'alias' => 'Ejecucionindicadore',
					'table' => 'ejecucionindicadores',
					'type' => 'LEFT',
					'conditions' => 'Ejecucionevaluacione.id = Ejecucionindicadore.ejecucionevaluacione_id'
				),
			),
			'fields' => array(
				'Ejecucionfisica.*',
				'Unidadmedida.*',
				'Medioverificacione.*',
				'Indicadore.*',
				'Meta.*',
				'Objetivo.*',
				'Ejecucionevaluacione.*',
				'Ejecucionindicadore.*',
			),
			'order' => 'Objetivo.id, Meta.id, Indicadore.id, Medioverificacione.id, Unidadmedida.id, Ejecucionfisica.id'
		);
		$this->Unidadmedida->recursive = -1;
		$ejecucionfisicas = $this->Unidadmedida->find("all", $conditions);
		$trimestre = $this->Trimestre->findById($trimestre_id);
		$this->set(compact('ejecucionfisicas', 'trimestre'));
	}
}