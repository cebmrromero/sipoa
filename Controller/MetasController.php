<?php
App::uses('AppController', 'Controller');
/**
 * Metas Controller
 *
 * @property Meta $Meta
 */
class MetasController extends AppController {

/**
 * add method
 *
 * @return void
 */
	public function add($poa_id, $objetivo_id) {
		if ($this->request->is('post')) {
			$this->Meta->create();
			if ($this->Meta->save($this->request->data)) {
				$this->Session->setFlash(__('The meta has been saved'), 'success');
				// Si el boton continuar fue presionado
				if ($this->request->data['Meta']['continuar']) {
					$this->redirect(array('action' => 'add', $poa_id, $objetivo_id));
				} else {
					$this->redirect(array('controller' => 'planificacion', 'action' => 'indicadores', $this->request->data['Meta']['poa_id'], $this->Meta->getLastInsertID()));
				}
			} else {
				$this->Session->setFlash(__('The meta could not be saved. Please, try again.'), 'error');
			}
		}
		$objetivo = $this->Meta->Objetivo->findById($objetivo_id);
		$poa = $this->Meta->Poa->findById($poa_id);
		$this->set(compact('poa', 'objetivo'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null, $poa_id = null) {
		if (!$this->Meta->exists($id)) {
			throw new NotFoundException(__('Invalid meta'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Meta->save($this->request->data)) {
				$this->Session->setFlash(__('The meta has been saved'), 'success');
				$this->redirect(array('controller' => 'planificacion', 'action' => 'metas', $poa_id));
			} else {
				$this->Session->setFlash(__('The meta could not be saved. Please, try again.'), 'error');
			}
		} else {
			$options = array('conditions' => array('Meta.' . $this->Meta->primaryKey => $id));
			$this->request->data = $this->Meta->find('first', $options);
		}
		$objetivos = $this->Meta->Objetivo->find('list');
		
		$this->request->data['Meta']['costo'] = str_replace('Bs.', '', $this->request->data['Meta']['costo']);
		$this->set(compact('objetivos'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Meta->id = $id;
		if (!$this->Meta->exists()) {
			throw new NotFoundException(__('Invalid meta'));
		}
		$this->request->onlyAllow('get', 'delete');
		if ($this->Meta->delete()) {
			$this->Session->setFlash(__('Meta deleted'), 'success');
			$this->redirect($this->referer());
		}
		$this->Session->setFlash(__('Meta was not deleted'), 'error');
		$this->redirect($this->referer());
	}
}
