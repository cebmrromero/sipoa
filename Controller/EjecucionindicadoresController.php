<?php
App::uses('AppController', 'Controller');
/**
 * Ejecucionindicadores Controller
 *
 * @property Ejecucionindicadore $Ejecucionindicadore
 */
class EjecucionindicadoresController extends AppController {

/**
 * add method
 *
 * @return void
 */
	public function add($poa_id = null, $trimestre_id = null) {
		if ($this->request->is('post')) {
			$this->Ejecucionindicadore->create();
			
			$error = false;
			foreach ($this->request->data as $data) {
				if (!$this->Ejecucionindicadore->saveAll($data)) {
					$error = true;
				}
			}
			
			if ($error) {
				$this->Session->setFlash(__('The ejecucionindicadore could not be saved. Please, try again.'), 'error');
				$this->redirect($this->referer());
			} else {
				$this->Session->setFlash(__('The ejecucionindicadore has been saved'), 'success');
				$this->redirect(array('controller' => 'evaluacion', 'action' => 'principal', $poa_id));;
			}
		}
	}
}
