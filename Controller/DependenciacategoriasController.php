<?php
App::uses('AppController', 'Controller');
/**
 * Dependenciacategorias Controller
 *
 * @property Dependenciacategoria $Dependenciacategoria
 * @property PaginatorComponent $Paginator
 */
class DependenciacategoriasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Dependenciacategoria->recursive = 0;
		$dependenciacategorias = $this->paginate();
		$this->set(compact('dependenciacategorias'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Dependenciacategoria->exists($id)) {
			throw new NotFoundException(__('Invalid dependenciacategoria'));
		}
		$options = array('conditions' => array('Dependenciacategoria.' . $this->Dependenciacategoria->primaryKey => $id));
		$dependenciacategoria = $this->Dependenciacategoria->find('first', $options);
		$this->set(compact('dependenciacategoria'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Dependenciacategoria->create();
			if ($this->Dependenciacategoria->save($this->request->data)) {
				$this->Session->setFlash(__('The dependenciacategoria has been saved'), 'success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The dependenciacategoria could not be saved. Please, try again.'), 'error');
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Dependenciacategoria->exists($id)) {
			throw new NotFoundException(__('Invalid dependenciacategoria'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Dependenciacategoria->save($this->request->data)) {
				$this->Session->setFlash(__('The dependenciacategoria has been saved'), 'success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The dependenciacategoria could not be saved. Please, try again.'), 'error');
			}
		} else {
			$options = array('conditions' => array('Dependenciacategoria.' . $this->Dependenciacategoria->primaryKey => $id));
			$this->request->data = $this->Dependenciacategoria->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Dependenciacategoria->id = $id;
		if (!$this->Dependenciacategoria->exists()) {
			throw new NotFoundException(__('Invalid dependenciacategoria'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Dependenciacategoria->delete()) {
			$this->Session->setFlash(__('Dependenciacategoria deleted'), 'success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Dependenciacategoria was not deleted'), 'error');
		$this->redirect(array('action' => 'index'));
	}}
