<?php
App::uses('AppController', 'Controller');
/**
 * Medioverificaciones Controller
 *
 * @property Medioverificacione $Medioverificacione
 */
class MedioverificacionesController extends AppController {

/**
 * add method
 *
 * @return void
 */
	public function add($indicadore_id) {
		$this->Medioverificacione->Indicadore->recursive = 2;
		$indicadore = $this->Medioverificacione->Indicadore->findById($indicadore_id);
		if ($this->request->is('post')) {
			$this->Medioverificacione->create();
			if ($this->Medioverificacione->save($this->request->data)) {
				$this->Session->setFlash(__('The medioverificacione has been saved'), 'success');
				$this->redirect(array('controller' => 'planificacion', 'action' => 'medioverificaciones', $indicadore['Meta']['poa_id'], $indicadore_id));
			} else {
				$this->Session->setFlash(__('The medioverificacione could not be saved. Please, try again.'), 'error');
			}
		}
		$this->set(compact('indicadore'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null, $poa_id = null) {
		if (!$this->Medioverificacione->exists($id)) {
			throw new NotFoundException(__('Invalid medioverificacione'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Medioverificacione->save($this->request->data)) {
				$this->Session->setFlash(__('The medioverificacione has been saved'), 'success');
				$this->redirect(array('controller' => 'planificacion', 'action' => 'medioverificaciones', $poa_id, $this->request->data['Medioverificacione']['indicadore_id']));
			} else {
				$this->Session->setFlash(__('The medioverificacione could not be saved. Please, try again.'), 'error');
			}
		} else {
			$options = array('conditions' => array('Medioverificacione.' . $this->Medioverificacione->primaryKey => $id));
			$this->Medioverificacione->recursive = 3;
			$this->request->data = $this->Medioverificacione->find('first', $options);
		}
		$indicadores = $this->Medioverificacione->Indicadore->find('list');
		$this->set(compact('indicadores'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Medioverificacione->id = $id;
		if (!$this->Medioverificacione->exists()) {
			throw new NotFoundException(__('Invalid medioverificacione'));
		}
		$this->request->onlyAllow('get', 'delete');
		if ($this->Medioverificacione->delete()) {
			$this->Session->setFlash(__('Medioverificacione deleted'), 'success');
			$this->redirect($this->referer());
		}
		$this->Session->setFlash(__('Medioverificacione was not deleted'), 'error');
		$this->redirect($this->referer());
	}
}
