<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Console.Templates.default.views
 * @since         CakePHP(tm) v 1.2.0.5234
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<?php echo "<?php \$this->Html->addCrumb(__('$pluralHumanName'), '/$pluralVar/index'); ?>\n"; ?>
<div class="row-fluid <?php echo $pluralVar; ?> index body-top">
	<div class="title span7">
		<h2><?php echo "<?php echo __('Listado de {$pluralHumanName}'); ?>"; ?></h2>
	</div>
	<div class="actions span5">
		<ul class="inline-menu pull-right">
			<li><?php echo "<?php echo \$this->Html->link(__('Agregar'), array('action' => 'add')); ?>"; ?></li>
			<li class="active"><?php echo "<?php echo \$this->Html->link(__('Listado'), array('action' => 'index')); ?>"; ?></li>
		</ul>
	</div>
</div>
<div class="row-fluid body-middle">
	<div class="span12">
		<p><?php echo "<?php echo __(\"A continuacion se encuentan los registros correspondientes a %s:\", \"$pluralHumanName\"); ?>"; ?></p>
		<table cellpadding="0" cellspacing="0" class="table table-striped table-condensed">
			<thead>
				<tr>
<?php foreach ($fields as $field): ?>
					<th><?php echo "<?php echo \$this->Paginator->sort('{$field}'); ?>"; ?></th>
<?php endforeach; ?>
					<th class="actions"><?php echo "<?php echo __('Actions'); ?>"; ?></th>
				</tr>
			</thead>
			<tbody>
				<?php
	echo "<?php foreach (\${$pluralVar} as \${$singularVar}): ?>\n";
	echo "\t\t\t\t\t<tr>\n";
		foreach ($fields as $field) {
			$isKey = false;
			if (!empty($associations['belongsTo'])) {
				foreach ($associations['belongsTo'] as $alias => $details) {
					if ($field === $details['foreignKey']) {
						$isKey = true;
						echo "\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t<?php echo \$this->Html->link(\${$singularVar}['{$alias}']['{$details['displayField']}'], array('controller' => '{$details['controller']}', 'action' => 'view', \${$singularVar}['{$alias}']['{$details['primaryKey']}'])); ?>\n\t\t\t\t\t\t</td>\n";
						break;
					}
				}
			}
			if ($isKey !== true) {
				echo "\t\t\t\t\t\t<td><?php echo h(\${$singularVar}['{$modelClass}']['{$field}']); ?>&nbsp;</td>\n";
			}
		}

		echo "\t\t\t\t\t\t<td class=\"actions btn-group span1\">\n";
		echo "\t\t\t\t\t\t\t<?php echo \$this->Html->link(__('<i class=\"icon-zoom-in icon-white\"></i>'), array('action' => 'view', \${$singularVar}['{$modelClass}']['{$primaryKey}']), array(\"class\" => \"btn btn-small btn-primary\", \"escape\" => false, \"title\" => __('Visualizar registro'))); ?>\n";
		echo "\t\t\t\t\t\t\t<?php echo \$this->Html->link(__('<i class=\"icon-edit icon-white\"></i>'), array('action' => 'edit', \${$singularVar}['{$modelClass}']['{$primaryKey}']), array(\"class\" => \"btn btn-small btn-primary\", \"escape\" => false, \"title\" => __('Editar registro'))); ?>\n";
		echo "\t\t\t\t\t\t\t<?php echo \$this->Form->postLink(__('<i class=\"icon-trash icon-white\"></i>'), array('action' => 'delete', \${$singularVar}['{$modelClass}']['{$primaryKey}']), array(\"class\" => \"btn btn-small btn-primary\", \"escape\" => false, \"title\" => __('Eliminar registro')), __('¿Está de acuerdo con eliminar este registro?')); ?>\n";
		echo "\t\t\t\t\t\t</td>\n";
	echo "\t\t\t\t\t</tr>\n";

	echo "\t\t\t\t<?php endforeach; ?>\n";
	?>
			</tbody>
		</table>
	</div>
</div>
<div class="row-fluid body-bottom">
	<div class="span12 text-center">
		<p><?php echo "<?php echo \$this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de un total de {:count}, iniciando en el registro {:start}, y finalizando en {:end}'))); ?>"; ?></p>
		<div class="paging">
		<?php
			echo "\t<?php\n";
			echo "\t\t\techo \$this->Paginator->prev('< ' . __('anterior'), array(), null, array('class' => 'prev disabled'));\n";
			echo "\t\t\techo \$this->Paginator->numbers(array('separator' => '|'));\n";
			echo "\t\t\techo \$this->Paginator->next(__('siguiente') . ' >', array(), null, array('class' => 'next disabled'));\n";
			echo "\t\t\t?>\n";
		?>
		</div>
	</div>
</div>

