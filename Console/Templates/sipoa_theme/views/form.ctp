<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Console.Templates.default.views
 * @since         CakePHP(tm) v 1.2.0.5234
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<?php $add_edit = ($action == 'add') ? 'Agregar' : 'Editar'; ?>
<?php echo "<?php \$this->Html->addCrumb(__('$pluralHumanName'), '/$pluralVar/index'); ?>\n"; ?>
<?php if ($action == 'add'): ?>
	<?php echo "<?php \$this->Html->addCrumb(__('$add_edit'), '/$pluralVar/add'); ?>\n"; ?>
<?php else: ?>
	<?php echo "<?php \$this->Html->addCrumb(__('$add_edit'), '/$pluralVar/edit/' . \$this->request->data['{$modelClass}']['{$primaryKey}']); ?>\n"; ?>
<?php endif; ?>
<div class="row-fluid <?php echo $pluralVar; ?> form body-top">
	<div class="title span7">
		<h2><?php echo "<?php echo __('$add_edit {$singularHumanName}'); ?>"; ?></h2>
	</div>
	<div class="actions span5">
		<ul class="inline-menu pull-right">
<?php if ($action == 'edit'): ?>
			<li class="active"><?php echo "<?php echo \$this->Html->link(__('Editar'), array('action' => '$action', \$this->request->data['{$modelClass}']['{$primaryKey}'])); ?>"; ?></li>
			<li><?php echo "<?php echo \$this->Html->link(__('Agregar'), array('action' => 'add')); ?>"; ?></li>
<?php else: ?>
			<li class="active"><?php echo "<?php echo \$this->Html->link(__('Agregar'), array('action' => 'add')); ?>"; ?></li>
<?php endif; ?>
			<li><?php echo "<?php echo \$this->Html->link(__('Listado'), array('action' => 'index')); ?>"; ?></li>
		</ul>
	</div>
</div>
<div class="row-fluid body-middle">
	<div class="span12">
		<?php echo "<?php echo \$this->Form->create('{$modelClass}'); ?>\n"; ?>
<?php if ($action == 'edit') { echo "\t\t\t<?php echo \$this->Form->input('id'); ?>\n"; } ?>
			<fieldset>
				<legend><?php printf("<?php echo __('Datos del %s'); ?>", $singularHumanName); ?></legend>
				<div class="row-fluid">
<?php
		foreach ($fields as $field) {
			if (strpos($action, 'add') !== false && $field == $primaryKey) {
				continue;
			} elseif (!in_array($field, array('created', 'modified', 'updated', 'id'))) {
?>
					<div class="span6 control-group">
						<?php printf("<?php echo \$this->Form->input('{$field}', array('class' => 'span12', 'label' => __('%s'))); ?>\n", Inflector::humanize($field)); ?>
						<span class="help-block"><?php echo "<?php echo __(\"helpbox $pluralVar $action $field\"); ?>"; ?></span>
					</div>
<?php
			}
		}
		if (!empty($associations['hasAndBelongsToMany'])) {
			foreach ($associations['hasAndBelongsToMany'] as $assocName => $assocData) {
?>
					<div class="span6 control-group">
						<?php printf("<?php echo \$this->Form->input('{$assocName}', array('class' => 'span12', 'label' => __('%s'))); ?>\n", Inflector::humanize($assocName)); ?>
						<span class="help-block"><?php echo "<?php echo __(\"helpbox $pluralVar $action $assocName\"); ?>"; ?></span>
					</div>
<?php
			}
		}
?>
				</div>
			</fieldset>
			<div class="form-actions">
				<?php echo "<button type=\"submit\" class=\"btn btn-primary\"><?php echo __(\"Guardar\"); ?></button>\n"; ?>
				<?php echo "<button type=\"button\" class=\"btn btn-small cancel\"><?php echo __(\"Cancelar\"); ?></button>\n"; ?>
			</div>
		<?php echo "<?php echo \$this->Form->end(); ?>\n"; ?>
	</div>
</div>
