#!/usr/bin/perl -w

use DBI;
use Data::Dumper;
use feature 'say';

my $where = ' ';
my $and = ' AND ';
my $dependenciacategoria = '';
foreach my $arg (@ARGV) {
    my @params = split('=', $arg);
    if ($params[0] eq 'dependencia_id' && $params[1] ne '') {
        $where .= $and . " Poa.dependencia_id = " . $params[1];
        $and = ' AND ';
    }
    if ($params[0] eq 'dependenciacategoria_id' && $params[1] ne '') {
        $dependenciacategoria = " WHERE Dependencia.dependenciacategoria_id = " . $params[1];
    }
    if ($params[0] eq 'ano' && $params[1] ne '') {
        $where .= $and . " Poa.ano = " . $params[1];
        $and = ' AND ';
    }
    if ($params[0] eq 'es_reprogramado' && $params[1] ne '') {
        $where .= $and . " Poa.es_reprogramado = " . $params[1];
        $and = ' AND ';
    }
}
#print $where;

$dbh = DBI->connect('dbi:mysql:sipoadb', 'root', 'root');

$sql = "SELECT
    `Poa`.`id`, `Poa`.`dependencia_id`, `Poa`.`ano`, `Poa`.`es_reprogramado`, `Poa`.`poacabecera_id`, `Poa`.`organo_id`, `Poa`.`paso_id`, `Dependencia`.`dependenciacategoria_id`, `Dependencia`.`descripcion`, `Poacabecera`.`denominacion`, `Poacabecera`.`descripcion`, `Poacabecera`.`parent_id`, `Poacabecera`.`lft`, `Poacabecera`.`rght`, `Poacabecera`.`seleccionable`, `Poacabecera`.`activo`, `Organo`.`descripcion`
FROM
    `poas` AS `Poa`
LEFT JOIN `dependencias` AS `Dependencia` ON (`Poa`.`dependencia_id` = `Dependencia`.`id`)
LEFT JOIN `poacabeceras` AS `Poacabecera` ON (`Poa`.`poacabecera_id` = `Poacabecera`.`id`)
LEFT JOIN `organos` AS `Organo` ON (`Poa`.`organo_id` = `Organo`.`id`)
WHERE
    `Dependencia`.`dependenciacategoria_id` in (
        SELECT `Dependencia`.`dependenciacategoria_id` FROM `sipoadb`.`dependencias` AS Dependencia LEFT JOIN `sipoadb`.`dependenciacategorias` AS Dependenciacategoria ON (`Dependencia`.`dependenciacategoria_id` = `Dependenciacategoria`.`id`) $dependenciacategoria
    ) $where
";
#print $sql;
$sth = $dbh->prepare($sql);
$sth->execute;

my @poas;	
while ($row = $sth->fetchrow_hashref()) {
    push(@poas, $row);
#print $$row{'id'} . "\n";
#     say Dumper($row);
}

#say Dumper(@poas);
my @nivel;
$poas[0]{'nivel_ejecucion'} = @nivel;
print $poas[0]{'nivel_ejecucion'};
