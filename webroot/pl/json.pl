#!/usr/bin/perl -w

#
# Incorporando las librerias
#

# Liberrias base
use strict;
use warnings;

# Esto es para poder tener acceso al GET, y con ello obtener el JSON
use LWP::Simple;

# Para el manejo de jsons
use JSON qw( decode_json );

# Libreria para escribir en excel
use Spreadsheet::WriteExcel;

# Asuntos de archvios y escritura en sistema de archivos
use POSIX;

# Esto es para permitir escritura utf8, acentos y caracteres especiales
use utf8;
use open qw(:std :utf8);

# Debug
use Data::Dumper;
use feature 'say';


#############################
#                           #
# Configuracion del Reporte #
#                           #
#############################

# Nombre del archivo destino
my $filename = "reporte1.xls";

# URL Base del sistema
my $url = 'http://localhost/sipoa/';

# Capturando la ruta que viene por argumento, es una ruta para CakePHP Search Plugin
foreach my $arg (@ARGV) {
    $url .= $arg;
}

# Originalmente la ruta es la generada para
# excel, y se le cambia a json, para luego manipularlo
$url =~ s/excel/json/ig;
print $url . "\n";

# Obteniendo el json de la ruta
my $json = get($url);

# Decodificando el JSON
my $decoded = decode_json($json);


#
# Creando la hoja de Excel
#
my $workbook  = Spreadsheet::WriteExcel->new($filename);

# Dandole nombre a la hoja
my $worksheet = $workbook->add_worksheet("Reporte POA");
$worksheet->set_landscape();                # Horizontal
$worksheet->set_paper(1);                   # Carta
$worksheet->center_horizontally();          # Centado Horizontalmente
$worksheet->set_margins(0.2);               # Margenes # TODO: Poner los correctos
$worksheet->set_print_scale(80);            # Escala de impresion para que se vea en la hoja completa
$worksheet->hide_gridlines();               # Eliminando las grid para que no se vean en la impresion
my $n = 5;                                  # Hay 5 espacios para el logo, por eso se inicia en 5 la escritura
my $line_h = 13;                            # Altura predefinida de una celda

# Definiendo las dimensiones del documento
$worksheet->set_column("A:A", 1.50 * 12.50);
$worksheet->set_column("B:B", 0.32 * 12.50);
$worksheet->set_column("C:C", 1.35 * 12.50);
$worksheet->set_column("D:D", 0.08 * 12.50);
$worksheet->set_column("E:E", 1.62 * 12.50);
$worksheet->set_column("F:F", 0.13 * 12.50);
$worksheet->set_column("G:G", 0.26 * 12.50);
$worksheet->set_column("H:H", 0.07 * 12.50);
$worksheet->set_column("I:I", 1.21 * 12.50);
$worksheet->set_column("J:J", 0.07 * 12.50);
$worksheet->set_column("K:K", 1.24 * 12.50);
$worksheet->set_column("L:L", 1.31 * 12.50);
$worksheet->set_column("M:M", 0.68 * 12.50);
$worksheet->set_column("N:N", 0.68 * 12.50);
$worksheet->set_column("O:O", 0.68 * 12.50);
$worksheet->set_column("P:P", 0.68 * 12.50);
$worksheet->set_column("Q:Q", 0.91 * 12.50);



#############################
#                           #
#          FORMATOS         #
#                           #
#############################

my %global_options = (
    valign  => 'vcenter', font => 'Arial', size  => 10,
);

# Formato del Titulo
my %options = (
    %global_options, (
        center_across => 1, valign  => 'vcenter', bold => 1, right => 6, top => 6
    )
);
my $title_plan_format = $workbook->add_format(%options);

%options = (
    %global_options, (
        center_across => 1, bold => 1, left => 6, bottom => 1, top => 6
    )
);
my $title_plan_text_format = $workbook->add_format(%options);


# Formatos de la Cabecera
%options = (
    %global_options, (
        align => 'left', top => 1, left => 6, bottom => 1
    )
);
my $cabeceras_poa_left_format = $workbook->add_format(%options);

%options = (
    %global_options, (
        top => 1, right => 6, bottom => 1, text_wrap => 1
    )
);
my $cabeceras_poa_right_format = $workbook->add_format(%options);

%options = (
    %global_options, (
        align => 'left', top => 1, left => 6, bottom => 6, text_wrap => 1
    )
);
my $cabeceras_poa_left_ultima_format = $workbook->add_format(%options);

my $cabeceras_poa_right_ultima_format = $workbook->add_format(align => 'left', valign  => 'vcenter', 'font' => 'Arial', size  => 10, top => 1, right => 6, bottom => 6);
   $cabeceras_poa_right_ultima_format->set_text_wrap();

my $topheading_principio = $workbook->add_format(center_across => 1, valign  => 'vcenter', 'font' => 'Arial', size  => 10, top => 6, left=> 6, bottom => 2, right => 2);
$topheading_principio->set_text_wrap();
my $topheading = $workbook->add_format(center_across => 1, valign  => 'vcenter', 'font' => 'Arial', size  => 10, top => 6, left=> 2, bottom => 2, right => 2);
$topheading->set_text_wrap();
my $topheading_inner = $workbook->add_format(center_across => 1, valign  => 'vcenter', 'font' => 'Arial', size  => 10, top => 2, left=> 2, bottom => 2, right => 2);
$topheading_inner->set_text_wrap();
my $topheading_fin = $workbook->add_format(center_across => 1, valign  => 'vcenter', 'font' => 'Arial', size  => 10, top => 6, left=> 2, bottom => 2, right => 6);
$topheading_fin->set_text_wrap();

my $topheading_bold = $workbook->add_format(center_across => 1, bold => 1, valign  => 'vcenter', 'font' => 'Arial', size  => 10, top => 6, left=> 2, bottom => 2, right => 2);
$topheading_bold->set_text_wrap();
my $topheading_cost = $workbook->add_format(center_across => 1, valign  => 'vcenter', 'font' => 'Arial', size  => 10);
$topheading_cost->set_text_wrap();
my $objetivoformat = $workbook->add_format(align => 'justify', valign  => 'vcenter', 'font' => 'Arial', size  => 10, left => 2, right => 2);
$objetivoformat->set_text_wrap();
my $objetivoformat_down = $workbook->add_format(align => 'justify', valign  => 'top', 'font' => 'Arial', size  => 10, bottom => 1);
my $objetivoformat_down_right = $workbook->add_format(align => 'justify', valign  => 'top', 'font' => 'Arial', size  => 10, bottom => 1, right => 2);
my $objetivoformat_down_left = $workbook->add_format(align => 'justify', valign  => 'vcenter', 'font' => 'Arial', size  => 10, bottom => 1, left => 2);
my $indformat = $workbook->add_format(align => 'justify', valign  => 'bottom', 'font' => 'Arial', size  => 10);
$indformat->set_text_wrap();
my $indnumerador = $workbook->add_format(align => 'justify', valign  => 'bottom', 'font' => 'Arial', size  => 10, bottom => 1);
$indnumerador->set_text_wrap();
my $indnumerador_left = $workbook->add_format(align => 'justify', valign  => 'bottom', 'font' => 'Arial', size  => 10, left => 2);
my $inddenominador = $workbook->add_format(align => 'justify', valign  => 'top', 'font' => 'Arial', size  => 10);
$inddenominador->set_text_wrap();
my $indformattop2 = $workbook->add_format(align => 'justify', valign  => 'vcenter', 'font' => 'Arial', size  => 10);
$indformattop2->set_text_wrap();
my $indformattop = $workbook->add_format(align => 'justify', valign  => 'top', 'font' => 'Arial', size  => 10);
$indformattop->set_text_wrap();
my $indformat_rigth = $workbook->add_format(align => 'justify', valign  => 'vcenter', 'font' => 'Arial', size  => 10, right => 2);
$indformat_rigth->set_text_wrap();
my $indformat_res = $workbook->add_format(align => 'justify', valign  => 'top', 'font' => 'Arial', size  => 10, right => 2, left => 2);
$indformat_res->set_text_wrap();
my $metaformat = $workbook->add_format(align => 'justify', valign  => 'top', 'font' => 'Arial', size  => 10);
$metaformat->set_text_wrap();
my $metaformat_count = $workbook->add_format(align => 'justify', valign  => 'top', 'font' => 'Arial', size  => 10);
$metaformat->set_text_wrap();
my $mvformat = $workbook->add_format(align => 'justify', valign  => 'top', 'font' => 'Arial', size  => 10);
$mvformat->set_text_wrap();
my $mvformat_right = $workbook->add_format(align => 'justify', valign  => 'top', 'font' => 'Arial', size  => 10, right => 2);
$mvformat_right->set_text_wrap();
my $supformat= $workbook->add_format(align => 'justify', valign  => 'top', 'font' => 'Arial', size  => 10, right => 2);
$supformat->set_text_wrap();
my $umformat = $workbook->add_format(align => 'center', valign  => 'top', 'font' => 'Arial', size  => 10, right => 2);
$umformat->set_text_wrap();
my $umformat_right = $workbook->add_format(align => 'center', valign  => 'top', 'font' => 'Arial', size  => 10, right => 2);
$umformat_right->set_text_wrap();
my $umformat_right_double = $workbook->add_format(align => 'center', valign  => 'top', 'font' => 'Arial', size  => 10, right => 6);
$umformat_right_double->set_text_wrap();

my $triformat = $workbook->add_format(align => 'center', valign  => 'top', 'font' => 'Arial', size  => 10, right => 2);
$triformat->set_text_wrap();
my $triformat_right = $workbook->add_format(align => 'center', valign  => 'top', 'font' => 'Arial', size  => 10, right => 6);
$triformat_right->set_text_wrap();
my $ultima_linea_format = $workbook->add_format(center_across => 1, valign  => 'vcenter', bold => 1, 'font' => 'Arial', size  => 10, top => 6);

$worksheet->insert_image('A1', 'left.png', 103, 5);
$worksheet->insert_image('K1', 'right.png', 34, 22);
my $rango = '';
my $nlines = 3;
my $oldobjetivo = 0;
my $oldmeta = 0;
my $oldind = 0;
my $oldmv = 0;
my $oldsup = 0;
my $naux_i = 0;
my $mh = 0;

# Empezando a escribir las lines del poa
foreach my $f (@{$decoded}) {
    if ( $n == 5 ) {
        my $cabecera = 'PLAN OPERATIVO ANUAL ' . $f->{"Poa"}->{"ano"};
        $worksheet->write      ($n, 0, $cabecera, $title_plan_text_format);
        $worksheet->write_blank($n, 1,            $title_plan_format);
        $worksheet->write_blank($n, 2,            $title_plan_format);
        $worksheet->write_blank($n, 3,            $title_plan_format);
        $worksheet->write_blank($n, 4,            $title_plan_format);
        $worksheet->write_blank($n, 5,            $title_plan_format);
        $worksheet->write_blank($n, 6,            $title_plan_format);
        $worksheet->write_blank($n, 7,            $title_plan_format);
        $worksheet->write_blank($n, 8,            $title_plan_format);
        $worksheet->write_blank($n, 9,            $title_plan_format);
        $worksheet->write_blank($n, 10,           $title_plan_format);
        $worksheet->write_blank($n, 11,           $title_plan_format);
        $worksheet->write_blank($n, 12,           $title_plan_format);
        $worksheet->write_blank($n, 13,           $title_plan_format);
        $worksheet->write_blank($n, 14,           $title_plan_format);
        $worksheet->write_blank($n, 15,           $title_plan_format);
        $worksheet->write_blank($n, 16,           $title_plan_format);
        $n++;
        
        foreach my $poacabecera (@{$f->{"Poacabecera"}}) {
            my $mh = length($poacabecera->{"Poacabecera"}->{"descripcion"}) / 110;
            $worksheet->set_row($n, $line_h * ceil($mh));
            $n++;
            $worksheet->merge_range("A" . $n . ':D' . $n, $poacabecera->{"Poacabecera"}->{"denominacion"} . ":", $cabeceras_poa_left_format);
            $worksheet->merge_range("E" . $n . ':Q' . $n, $poacabecera->{"Poacabecera"}->{"descripcion"}, $cabeceras_poa_right_format);
        }
        $n++;
        $worksheet->merge_range("A" . $n . ':D' . $n, "ÓRGANO / ENTE DESCENTRALIZADO:", $cabeceras_poa_left_ultima_format);
        $worksheet->merge_range("E" . $n . ':Q' . $n, $f->{"Organo"}->{"descripcion"}, $cabeceras_poa_right_ultima_format);
        
        $n++;
        $worksheet->write_blank($n, 0);
        
        $n++;
        $rango = "A" . $n . ':A' . ($n + 2);
        $worksheet->merge_range($rango, "OBJETIVO", $topheading_principio);
        
        $rango = "B" . $n . ':C' . ($n + 2);
        $worksheet->merge_range($rango, "META/PRODUCTO", $topheading);
        
        $rango = "D" . $n . ':G' . ($n + 2);
        $worksheet->merge_range($rango, "INDICADORES DE GESTIÓN", $topheading);
        
        $rango = "H" . $n . ':I' . ($n + 2);
        $worksheet->merge_range($rango, "MEDIOS DE VERIFICACIÓN", $topheading);
        
        $rango = "J" . $n . ':K' . ($n + 2);
        $worksheet->merge_range($rango, "SUPUESTOS", $topheading);
        
        $rango = "L" . $n . ':P' . $n;
        $worksheet->merge_range($rango, "PROGRAMACION DE LA EJECUCIÓN FÍSICA ANUAL", $topheading_bold);
        
        $rango = "L" . ($n + 1) . ':L' . ($n + 2);
        $worksheet->merge_range($rango, "Unidad de\nMedida", $topheading_inner);
        
        $rango = "M" . ($n + 1) . ':M' . ($n + 2);
        $worksheet->merge_range($rango, "I\nTrimestre", $topheading_inner);
        
        $rango = "N" . ($n + 1) . ':N' . ($n + 2);
        $worksheet->merge_range($rango, "II\nTrimestre", $topheading_inner);
        
        $rango = "O" . ($n + 1) . ':O' . ($n + 2);
        $worksheet->merge_range($rango, "III\nTrimestre", $topheading_inner);
        
        $rango = "P" . ($n + 1) . ':P' . ($n + 2);
        $worksheet->merge_range($rango, "IV\nTrimestre", $topheading_inner);
        
        $rango = "Q" . $n . ':Q' . ($n + 2);
        $worksheet->merge_range($rango, "COSTO (Bs.)", $topheading_fin);
        
        $worksheet->repeat_rows(0, $n+1);               # Estableciendo fijas las cabeceras
        $n = $n + 2;
    }
    
    my $naux = $n;
    my $meta_count;
    #say Dumper($f->{"Ejecucionfisica"});
    my @endobj = ();
    my $obji = 1;
    foreach my $ef (@{$f->{"Ejecucionfisica"}}) {
        #print "Ejecucion Fisica\n";
        my $lenmetas = 0;
        foreach my $aux (@{$f->{"Ejecucionfisica"}}) {
            if ($aux->{"Objetivo"}->{"id"} == $ef->{"Objetivo"}->{"id"}) {
                $lenmetas++;
            }
        }
        my $lensupuestos = @{$ef->{"Supuesto"}};
        if ($lensupuestos > $nlines) {
            $nlines = $lensupuestos;
        }
        if ($lenmetas > $nlines) {
            $nlines = $lenmetas;
        }
        my $lenmv = 0;
        foreach my $ind (@{$ef->{"Indicadore"}}) {
            $lenmv = @{$ind->{"Medioverificacione"}};
            if ($lenmv > $nlines) {
                $nlines = $lenmv;
            }
        }
        my $lenindicadores = @{$ef->{"Indicadore"}};
        my $rowspan = 0;
        if ($oldobjetivo != $ef->{"Objetivo"}->{"id"}) {
            $meta_count = 1;
            foreach my $aux (@{$f->{"Ejecucionfisica"}}) {
                if ($aux->{"Objetivo"}->{"id"} == $ef->{"Objetivo"}->{"id"}) {
                    $rowspan++;
                    $lenindicadores = @{$aux->{"Indicadore"}};
                }
            }
            $n++;
            $oldobjetivo = $ef->{"Objetivo"}->{"id"};
            my $rango_b = (($n + ($nlines * ($rowspan + $lenindicadores - 1)) - 1));
            push(@endobj, $rango_b);
            $rango = "A" . $n . ':A' . $rango_b;
            $worksheet->merge_range($rango, $obji . ". " . $ef->{"Objetivo"}->{"descripcion"}, $objetivoformat);
            #print  $ef->{"Objetivo"}->{"descripcion"} . "\n";
            $n = $n + ($nlines * ($rowspan + $lenindicadores - 1)) - 1;
            $obji++;
        }
        
        $lenindicadores = @{$ef->{"Indicadore"}};
        if ($oldmeta != $ef->{"Meta"}->{"id"}) {
            $oldmeta = $ef->{"Meta"}->{"id"};
            $naux++;
            $rango = "B" . $naux . ':B' . ($naux + ($nlines * $lenindicadores) - 1);
            $worksheet->merge_range($rango, " $meta_count.", $metaformat_count);
            $meta_count++;
        
            $rango = "C" . $naux . ':C' . ($naux + ($nlines * $lenindicadores) - 1);
            $naux_i = $naux;
            $naux = $naux + ($nlines * $lenindicadores) - 1;
            #print "Rango = " . $rango . " naux = " . $naux . "\n";
            $worksheet->merge_range($rango, $ef->{"Meta"}->{"cantidad"} . " " . $ef->{"Meta"}->{"descripcion"}, $metaformat);
            $rango = "Q" . $naux_i . ':Q' . ($naux_i + 2);
            $worksheet->merge_range($rango, $ef->{"Meta"}->{"costo"}, $triformat_right);
        } else {
            $rango = "Q" . $naux . ':Q' . ($naux + 2);
            $worksheet->write_blank($rango, $triformat_right);
        }
        my $naux_sup_i = 0;
        #say Dumper($ef->{"Indicadore"});
        my $ix = 0;
        foreach my $ind (@{$ef->{"Indicadore"}}) {
            #print " -" . $ind->{"Indicadore"}->{"resultado"} . "\n";
            if ($oldind != $ind->{"Indicadore"}->{"id"}) {
                $oldind = $ind->{"Indicadore"}->{"id"};
                $mh = length($ind->{"Indicadore"}->{"resultado"}) / 25;
                $worksheet->set_row($naux_i - 1, $line_h * ceil($mh));
                $rango = "D" . $naux_i . ':G' . $naux_i;
                $worksheet->merge_range($rango, $ind->{"Indicadore"}->{"resultado"} . " =", $indformat_res);
                
                my $mhn = length($ind->{"Indicadore"}->{"numerador"}) / 15;
                my $mhd = length($ind->{"Indicadore"}->{"denominador"}) / 15;
                $mh = $mhd;
                if ($mhn > $mhd) {
                    $mh = $mhn;
                }
                $rango = "E" . ($naux_i + 1);
                $worksheet->set_row($naux_i, $line_h * ceil($mh));
                $worksheet->write($rango, $ind->{"Indicadore"}->{"numerador"}, $indnumerador);
                
                $rango = "D" . ($naux_i + 1);
                $worksheet->write_blank($rango, $indnumerador_left);
                
                $rango = "E" . ($naux_i + 2);
                $worksheet->set_row($naux_i + 1, $line_h * ceil($mh));
                $worksheet->write($rango, $ind->{"Indicadore"}->{"denominador"}, $inddenominador);
                
                $rango = "D" . ($naux_i + 2);
                $worksheet->write_blank($rango, $indnumerador_left);
                
                $rango = "F" . ($naux_i + 1) . ':F' . ($naux_i + $nlines - 1);
                $worksheet->merge_range($rango, 'X', $indformattop2);
                
                $rango = "G" . ($naux_i + 1) . ':G' . ($naux_i + $nlines - 1);
                $worksheet->merge_range($rango, '100', $indformat_rigth);
                $ix++;
            }
            my $mvi = 0;
            my $supi = 0;
            my $lenmvs = @{$ind->{"Medioverificacione"}};
            foreach my $mv (@{$ind->{"Medioverificacione"}}) {
                if ($mv->{"indicadore_id"} == $ind->{"Indicadore"}->{"id"}) {
                    #print $ind->{"Indicadore"}->{"resultado"} . "\n";
                    if ($oldmv != $mv->{"id"}) {
                        $oldmv = $mv->{"id"};
                        $mh = length($mv->{"descripcion"}) / 12;
                        if ( (length($ind->{"Indicadore"}->{"numerador"}) / 15) > $mh ) {
                            $mh = length($ind->{"Indicadore"}->{"numerador"}) / 15;
                        }
                        # Fix del tama;o de los indicadores
                        #$worksheet->set_row($naux_i, $line_h * ceil($mh) / 2);
                        #$worksheet->set_row($naux_i + 1, $line_h * ceil($mh) / 2);
                        #$worksheet->set_row($naux_i + 2, $line_h * ceil($mh) / 2);
                        #$worksheet->set_row($naux_i + 3, $line_h * ceil($mh) / 2);
                        
                        $worksheet->set_row($naux_i - 1 + $mvi, $line_h * ceil($mh));
                        $rango = "H" . ($naux_i + $mvi);
                        $worksheet->write($rango, "-", $mvformat);
                        
                        $rango = "I" . ($naux_i + $mvi);
                        $worksheet->write($rango, $mv->{"descripcion"}, $mvformat_right);
                    }
                    my $mhsup = 0;
                    foreach my $sup (@{$ef->{"Supuesto"}}) {
                        if ($sup->{"medioverificacione_id"} == $mv->{"id"}) {
                            #if ($oldsup != $sup->{"id"}) {
                                $oldsup = $sup->{"id"};
                                $mhsup = length($sup->{"descripcion"}) / 16;
                                if ( $mhsup > $mh ) {
                                    $mh = $mhsup;
                                }
                                
                                $worksheet->set_row($naux_i - 1 + $supi, $line_h * ceil($mh));
                                
                                $rango = "J" . ($naux_i + $supi);
                                $worksheet->write($rango, "-", $mvformat);
                                
                                $rango = "K" . ($naux_i + $supi);
                                $worksheet->write($rango, $sup->{"descripcion"}, $supformat);
                                print $rango . " " . $sup->{"descripcion"} . "\n";
                                $supi++;
                            #}
                        }
                    }
                    $mvi++;
                }
            }
            for(my $i = $mvi; $i < $nlines; $i++) {
                $rango = "I" . ($naux_i + $i);
                $worksheet->write_blank($rango, $mvformat_right);
            }
            
            for(my $i = $supi; $i < $nlines; $i++) {
                $rango = "K" . ($naux_i + $i);
                $worksheet->write_blank($rango, $supformat);
            }
            
            #$rango = "L" . $naux_i . ':L' . ($naux_i + 2);
            $rango = "L" . $naux_i;
            $worksheet->write($rango, $ef->{"Unidadmedida"}->{"denominacion"}, $umformat);
            for (my $i = 0; $i < $nlines; $i++) {
                $worksheet->write_blank("L" . ($naux_i + $i), $umformat_right);
                $worksheet->write_blank("M" . ($naux_i + $i), $umformat_right);
                $worksheet->write_blank("N" . ($naux_i + $i), $umformat_right);
                $worksheet->write_blank("O" . ($naux_i + $i), $umformat_right);
                $worksheet->write_blank("P" . ($naux_i + $i), $umformat_right);
                $worksheet->write_blank("Q" . ($naux_i + $i), $umformat_right_double);
            }
            
            $rango = "M" . $naux_i . ':M' . ($naux_i + 2);
            $worksheet->merge_range($rango, $ef->{"Ejecucionfisica1"}->{"cantidad_planificada"}, $triformat);
            
            $rango = "N" . $naux_i . ':N' . ($naux_i + 2);
            $worksheet->merge_range($rango, $ef->{"Ejecucionfisica2"}->{"cantidad_planificada"}, $triformat);
            
            $rango = "O" . $naux_i . ':O' . ($naux_i + 2);
            $worksheet->merge_range($rango, $ef->{"Ejecucionfisica3"}->{"cantidad_planificada"}, $triformat);
            
            $rango = "P" . $naux_i . ':P' . ($naux_i + 2);
            $worksheet->merge_range($rango, $ef->{"Ejecucionfisica4"}->{"cantidad_planificada"}, $triformat);
            
            #$rango = "Q" . $naux_i . ':Q' . ($naux_i + 2);
            #$worksheet->write_blank($rango, $triformat_right);
            
            foreach my $row (@endobj) {
                $worksheet->write_blank('A' . $row, $objetivoformat_down);
                $worksheet->write_blank('B' . $row, $objetivoformat_down);
                $worksheet->write_blank('C' . $row, $objetivoformat_down);
                $worksheet->write_blank('D' . $row, $objetivoformat_down_left);
                $worksheet->write_blank('E' . $row, $objetivoformat_down);
                $worksheet->write_blank('F' . $row, $objetivoformat_down);
                $worksheet->write_blank('G' . $row, $objetivoformat_down);
                $worksheet->write_blank('H' . $row, $objetivoformat_down);
                $worksheet->write_blank('I' . $row, $objetivoformat_down_right);
                $worksheet->write_blank('J' . $row, $objetivoformat_down);
                $worksheet->write_blank('K' . $row, $objetivoformat_down_right);
                $worksheet->write_blank('L' . $row, $objetivoformat_down_right);
                $worksheet->write_blank('M' . $row, $objetivoformat_down);
                $worksheet->write_blank('N' . $row, $objetivoformat_down);
                $worksheet->write_blank('O' . $row, $objetivoformat_down);
                $worksheet->write_blank('P' . $row, $objetivoformat_down);
                #$worksheet->write_blank('Q' . $row, $objetivoformat_down);
            }
                
            $naux_i = $naux_i + $nlines;
        }
    }
}
# Escribiendo la ultima linea con su borde superior
$worksheet->write($n, 0, '', $ultima_linea_format);
$worksheet->write_blank($n, 1, $ultima_linea_format);
$worksheet->write_blank($n, 2, $ultima_linea_format);
$worksheet->write_blank($n, 3, $ultima_linea_format);
$worksheet->write_blank($n, 4, $ultima_linea_format);
$worksheet->write_blank($n, 5, $ultima_linea_format);
$worksheet->write_blank($n, 6, $ultima_linea_format);
$worksheet->write_blank($n, 7, $ultima_linea_format);
$worksheet->write_blank($n, 8, $ultima_linea_format);
$worksheet->write_blank($n, 9, $ultima_linea_format);
$worksheet->write_blank($n, 10, $ultima_linea_format);
$worksheet->write_blank($n, 11, $ultima_linea_format);
$worksheet->write_blank($n, 12, $ultima_linea_format);
$worksheet->write_blank($n, 13, $ultima_linea_format);
$worksheet->write_blank($n, 14, $ultima_linea_format);
$worksheet->write_blank($n, 15, $ultima_linea_format);
$worksheet->write_blank($n, 16, $ultima_linea_format);