$( function() {
/**
 * Url base del sistema
 */
    var BASE_URL = "/sipoa";
   
/**
 * Dropdown Switcher
 */
    $( '#operation-switcher' ).dropdown(); 
/**
 * Metodo para cambiar el switcher al presioanr click sobre el
 */
    onOperationSwitcherClick = function () {
        url = BASE_URL + "/" + $(this).data("value")
        location.href = url;
    }
/**
 * Trigger del metodo onOperationSwitcherClick
 */
    $( '#switcher ul li' ).click(onOperationSwitcherClick);
    
    
/**
 * Metodo para guardar y continuar
 */
    onPressSaveOther = function (e) {
        form = $(this).parents("form")
        $( "#continue" ).val(1)
        form.submit()
    }
/**
 * Guardar y Continuar
 */
    $( "button.save-other" ).click(onPressSaveOther)
});


/**
 * Boton de cancelar
 */
    function onPressCancel() {
        if (confirm("Si cancela perderá los cambios realizados, ¿Está seguro(a) que desea cancelar el proceso?")) {
            history.go(-1);
        }
    }
    $( "button.cancel" ).click(onPressCancel)
/** Evaluacion **/

/**
 * Calculo de porcentajes
 */
    function onEjecucionFisicaInputChange() {
        programado = parseFloat($(this).parent().prev().text())
        ejecutado = this.value
        ejecucion = $(this).parent().next()
        por_completar = $(this).parent().next().next()
        
        p_ejecucion = 0;
        if (programado == 0) {
            p_ejecucion = 0;
            p_porejecutar = 0;
        } else {
            p_ejecucion = (ejecutado / programado) * 100;
            p_porejecutar = 100 - p_ejecucion;   
        }
        
        $(ejecucion).text(p_ejecucion.toFixed(2));
        $(por_completar).text(p_porejecutar.toFixed(2));
        
    }
    $( "form.calculate input[type=text]" ).change(onEjecucionFisicaInputChange);
    $( "form.calculate input[type=text]" ).trigger("change")
    
    
/** Validaciones de la Ejecucion Fisica ADD **/

/**
 * Validando la cantidad planificada que coincida con la distribuida
 */
if ($( "#meta_cantidad" ).length) {
    // Chequear que haya disponibilidad y las funciones de estilo definidas
    checkDisponibilidad();
    $( ".cantidades" ).change(checkDisponibilidad);
    
    // Al enviar el formulario se valida que haya distribuido todo
    $("#EjecucionfisicaAddForm").submit(function (e) {
        cantidad = parseFloat($( "#meta_cantidad" ).text());
        disponibilidad = cantidad - cantidadPlanificada();
        if (disponibilidad > 0) {
            e.preventDefault();
            alert("Debe distrubir todas las Metas indicadas");
        }
    })
}

/**
 * Calculando la cantidad distribuida
 */
function cantidadPlanificada() {
    var cantidades = $( ".cantidades" );
    var suma = 0;
    for (i = 0; i < cantidades.length; i++) {
        suma += (cantidades[i].value) ? parseFloat(cantidades[i].value) : 0;
    }
    return suma;
}

/**
 * Determina si hay disponibilidad y acomoda el diseno de acuerdo a ese valor
 */
function checkDisponibilidad() {
    var cantidad = parseFloat($( "#meta_cantidad" ).text());
    var disponibilidad = cantidad - cantidadPlanificada()
    var flag = Math.ceil(cantidad * 20 / 100);
    
    // Si es menor o igual que el flag se pone en rojo, para avisar que se estan acabando
    if (disponibilidad <= flag) {
        $('#cantidad_feedback').removeClass("green");
        $('#cantidad_feedback').addClass("red");
    } else {
        $('#cantidad_feedback').removeClass("red");
        $('#cantidad_feedback').addClass("green");
    }
    
    // Si la disponibilidad es cero, ocultamos el span que muestra el texto
    if (disponibilidad == 0) {
        $('#cantidad_feedback').hide()
    } else {
        $('#cantidad_feedback').show()
    }
    
    // Si es negativa, se reinicia el valor del input a cero, por seguridad
    if (disponibilidad < 0) {
        this.value = 0
        disponibilidad = cantidad - cantidadPlanificada()
        $(this).trigger("change");
    }
    
    // Se actualiza el valor de la disponibilidad
    $('#cantidad_feedback strong').text(disponibilidad);
}