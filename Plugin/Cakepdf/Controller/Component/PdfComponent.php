<?php
App::uses('Component', 'Controller');

App::build(array('Vendor' => array(APP . 'Plugin' . DS . 'Cakepdf' . DS . 'Vendor' . DS . 'Html2pdf' . DS))); 
App::uses('HTML2PDF', 'Vendor');


class PdfComponent extends Component {
    public function create($controller) {
		$controller->autoRender = false;
		/* Set up new view that won't enter the ClassRegistry */
		$view = new View($controller, false);
		 
		/* Grab output into variable without the view actually outputting! */
		$view->layout = false;
        
        return $view;
    }
    
    public function doGeneratePdf($pdf) {
		ob_start();
		echo $pdf->render();
		$content = ob_get_clean();
        try
		{
			$html2pdf = new HTML2PDF('P', 'Letter', 'es', true, 'UTF-8', 0);
			$html2pdf->writeHTML($content, false);
			//$html2pdf->createIndex('Índice', 25, 12, false, true, 1);
			$html2pdf->Output('bookmark.pdf');
		}
		catch(HTML2PDF_exception $e) {
			echo "Error"; 
			echo $e;
			exit;
		}
    }
}