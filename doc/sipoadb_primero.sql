-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 30-07-2013 a las 11:35:24
-- Versión del servidor: 5.5.32
-- Versión de PHP: 5.3.10-1ubuntu3.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `sipoadb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `depedenciacategorias`
--

CREATE TABLE IF NOT EXISTS `depedenciacategorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `denominacion` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `depedenciacategorias`
--

INSERT INTO `depedenciacategorias` (`id`, `denominacion`) VALUES
(1, 'Sustancial'),
(2, 'Apoyo y Logistica');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dependencias`
--

CREATE TABLE IF NOT EXISTS `dependencias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `depedenciacategoria_id` int(11) NOT NULL,
  `denominacion` varchar(500) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_dependencias_depedenciacategorias_idx` (`depedenciacategoria_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `dependencias`
--

INSERT INTO `dependencias` (`id`, `depedenciacategoria_id`, `denominacion`) VALUES
(1, 1, 'Dirección de Control de la Administración Central y Otro Poder'),
(2, 1, 'Dirección de Control de la Administración Descentralizada'),
(3, 2, 'Dirección Técnica, Planificación y Control de Gestión');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ejecucionfisicas`
--

CREATE TABLE IF NOT EXISTS `ejecucionfisicas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `indicadore_id` int(11) NOT NULL,
  `trimestre1` int(11) NOT NULL DEFAULT '0',
  `trimestre2` int(11) NOT NULL DEFAULT '0',
  `trimestre3` int(11) NOT NULL DEFAULT '0',
  `trimestre4` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_ejecucionfisicas_indicadores1_idx` (`indicadore_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `ejecucionfisicas`
--

INSERT INTO `ejecucionfisicas` (`id`, `indicadore_id`, `trimestre1`, `trimestre2`, `trimestre3`, `trimestre4`) VALUES
(1, 1, 0, 1, 0, 0),
(2, 2, 8, 19, 17, 6),
(3, 3, 10, 15, 10, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `indicadores`
--

CREATE TABLE IF NOT EXISTS `indicadores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `meta_id` int(11) NOT NULL,
  `resultado` varchar(200) NOT NULL,
  `descripcion_numerador` varchar(200) NOT NULL,
  `descripcion_denominador` varchar(200) NOT NULL,
  `variable_numerador` varchar(45) NOT NULL,
  `variable_denominador` varchar(45) NOT NULL,
  `tiene_ejecucionfisica` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_indicadores_metas1_idx` (`meta_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `indicadores`
--

INSERT INTO `indicadores` (`id`, `meta_id`, `resultado`, `descripcion_numerador`, `descripcion_denominador`, `variable_numerador`, `variable_denominador`, `tiene_ejecucionfisica`) VALUES
(1, 1, 'Eficacia en la Presentación del Plan', 'Nº de Plan Elaborado', 'Nº de Plan Programado', 'plan_elaborados', 'plan_programados', 1),
(2, 2, 'Eficacia en la Atención de Apoyos Técnico-Logísticos', 'Nº de Apoyos Técnico-Logísticos efectuados', 'Nº Apoyos Técnico-Logísticos Programados', 'apoyos_efectuados', 'apoyos_programados', 1),
(3, 3, 'Eficacia en la Prestacion de Servicios Bibliotecarios', 'Nº de Servicios Prestados', 'Nº de Servicios programados', 'servicios_prestados', 'servicios_programados', 1),
(4, 3, 'Calidad en la Atención Prestada a los Usuarios', 'Total de Usuarios Satisfechos por el Servicio Bibliotecario Prestado', 'Cantidad Total de usuarios encuestados', 'total_satisfechos', 'total_encuestados', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `medioverificaciones`
--

CREATE TABLE IF NOT EXISTS `medioverificaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `indicadore_id` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_medioverificaciones_indicadores1_idx` (`indicadore_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `medioverificaciones`
--

INSERT INTO `medioverificaciones` (`id`, `indicadore_id`, `descripcion`) VALUES
(1, 1, 'Plan de optimización de la Red Institucional'),
(2, 2, 'Relación de Actividades para el apoyo técnico-logístico por la Dirección Técnica, Planificación y Control de Gestión'),
(3, 3, 'Control de préstamo de material de consulta'),
(4, 4, 'Análisis de los resultados de la encuesta');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `metas`
--

CREATE TABLE IF NOT EXISTS `metas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poa_id` int(11) NOT NULL,
  `objetivo_id` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  `costo` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_metas_objetivos1_idx` (`objetivo_id`),
  KEY `fk_metas_poas1_idx` (`poa_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `metas`
--

INSERT INTO `metas` (`id`, `poa_id`, `objetivo_id`, `cantidad`, `descripcion`, `costo`) VALUES
(1, 1, 1, 1, 'Plan de Optimización de la Red Institucional a nivel de Hardware y Software elaborado', 134176),
(2, 1, 2, 50, 'Apoyos Ténico Logísticos efectuados', 538982),
(3, 1, 2, 40, 'Servicios Bibliotecarios prestados', 186135);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `objetivos`
--

CREATE TABLE IF NOT EXISTS `objetivos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `objetivos`
--

INSERT INTO `objetivos` (`id`, `descripcion`) VALUES
(1, 'Formular herramientas de control interno, para el seguimiento, evaluación y proyección de las planificaciones operativas a las diferentes dependencias'),
(2, 'Lograr la eficacia en la atención de los apoyos técnico-logísticos y la calidad en los servicios bibliotecarios');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `objetivos_poas`
--

CREATE TABLE IF NOT EXISTS `objetivos_poas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poa_id` int(11) NOT NULL,
  `objetivo_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_poas_has_objetivos_objetivos1_idx` (`objetivo_id`),
  KEY `fk_poas_has_objetivos_poas1_idx` (`poa_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `objetivos_poas`
--

INSERT INTO `objetivos_poas` (`id`, `poa_id`, `objetivo_id`) VALUES
(1, 1, 1),
(2, 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pasos`
--

CREATE TABLE IF NOT EXISTS `pasos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` text NOT NULL,
  `descripcion` text NOT NULL,
  `formulario` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `poas`
--

CREATE TABLE IF NOT EXISTS `poas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dependencia_id` int(11) NOT NULL,
  `ano` year(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_poas_dependencias1_idx` (`dependencia_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `poas`
--

INSERT INTO `poas` (`id`, `dependencia_id`, `ano`) VALUES
(1, 3, 2013);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `supuestos`
--

CREATE TABLE IF NOT EXISTS `supuestos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `meta_id` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_supuestos_metas1_idx` (`meta_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `supuestos`
--

INSERT INTO `supuestos` (`id`, `meta_id`, `descripcion`) VALUES
(1, 1, 'Disponibilidad del talento humano y los recursos financieros y tecnológicos requeridos'),
(2, 2, 'Disponibilidad del talento humano'),
(3, 2, 'Disponibilidad operativa de los equipos de audio, video y sonido'),
(4, 2, 'Oportunidades de las solicitudes presentadas'),
(5, 3, 'Disposición por parte de los usuarios en asistir y solicitar en este recinto los servicios prestados');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `unidadmedidas`
--

CREATE TABLE IF NOT EXISTS `unidadmedidas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `indicadore_id` int(11) NOT NULL,
  `denominacion` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_unidadmedidas_indicadores1_idx` (`indicadore_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `unidadmedidas`
--

INSERT INTO `unidadmedidas` (`id`, `indicadore_id`, `denominacion`) VALUES
(1, 1, 'Plan'),
(2, 2, 'Apoyos Técnico-Logísticos'),
(3, 3, 'Servicios Bibliotecarios'),
(4, 4, 'Encuestas');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `dependencias`
--
ALTER TABLE `dependencias`
  ADD CONSTRAINT `fk_dependencias_depedenciacategorias` FOREIGN KEY (`depedenciacategoria_id`) REFERENCES `depedenciacategorias` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `ejecucionfisicas`
--
ALTER TABLE `ejecucionfisicas`
  ADD CONSTRAINT `fk_ejecucionfisicas_indicadores1` FOREIGN KEY (`indicadore_id`) REFERENCES `indicadores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `indicadores`
--
ALTER TABLE `indicadores`
  ADD CONSTRAINT `fk_indicadores_metas1` FOREIGN KEY (`meta_id`) REFERENCES `metas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `medioverificaciones`
--
ALTER TABLE `medioverificaciones`
  ADD CONSTRAINT `fk_medioverificaciones_indicadores1` FOREIGN KEY (`indicadore_id`) REFERENCES `indicadores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `metas`
--
ALTER TABLE `metas`
  ADD CONSTRAINT `fk_metas_objetivos1` FOREIGN KEY (`objetivo_id`) REFERENCES `objetivos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_metas_poas1` FOREIGN KEY (`poa_id`) REFERENCES `poas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `objetivos_poas`
--
ALTER TABLE `objetivos_poas`
  ADD CONSTRAINT `fk_poas_has_objetivos_objetivos1` FOREIGN KEY (`objetivo_id`) REFERENCES `objetivos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_poas_has_objetivos_poas1` FOREIGN KEY (`poa_id`) REFERENCES `poas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `poas`
--
ALTER TABLE `poas`
  ADD CONSTRAINT `fk_poas_dependencias1` FOREIGN KEY (`dependencia_id`) REFERENCES `dependencias` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `supuestos`
--
ALTER TABLE `supuestos`
  ADD CONSTRAINT `fk_supuestos_metas1` FOREIGN KEY (`meta_id`) REFERENCES `metas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `unidadmedidas`
--
ALTER TABLE `unidadmedidas`
  ADD CONSTRAINT `fk_unidadmedidas_indicadores1` FOREIGN KEY (`indicadore_id`) REFERENCES `indicadores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
